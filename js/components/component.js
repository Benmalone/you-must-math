// Component Interface
function Component() {}

// TODO: Observer pattern for general events between components
Component.prototype.receive = function(message) {};