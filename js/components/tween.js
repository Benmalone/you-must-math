function Tween(component, from, to, totalFrames, easing, callBack)
{
    this.component = component;
    this.to = to;
    this.from = from;
    this.totalFrames = totalFrames;
    this.easing = easing;
    
    this.frameCount = 0;
    
    this.onComplete = callBack;
    
    this.manager;
}

Tween.prototype.update = function()
{
    
    if (this.frameCount < this.totalFrames)
    {
        
        var normalizedTime = this.frameCount / this.totalFrames;
        var interpolatedTime = this.Easing[this.easing](normalizedTime);
        
        for (var property in this.from)
        {
            if (this.from.hasOwnProperty(property) && this.component[property] != null)
            {
                this.component[property] = (this.from[property] * (1 - interpolatedTime)) + (this.to[property] * interpolatedTime);
            }
        }
        
        this.frameCount++;
    } 
    else
    {
        if (this.onComplete)
        {
            this.onComplete();
        }
        
        this.manager.removeTween(this);
    }
    
};

Tween.prototype.Easing = 
{
    smoothStep: function(normalizedTime) 
    {
        return normalizedTime * normalizedTime * (3 - 2 * normalizedTime);
    }
};