function BoxRenderer(entity) {
    
    Component.call(this);
    
    this.entity = entity;
    this.transform = entity.transform;

    // Allowing literal values for now
    this.fillStyle = "gray";
    this.strokeStyle = "black";
    this.lineWidth = 4;   

}

BoxRenderer.prototype = Object.create(Component.prototype,
                                {
                                    constructor: 
                                    {
                                        configurable: true,
                                        enumerable: true,
                                        value: BoxRenderer,
                                        writable: true
                                    }
                                });

BoxRenderer.prototype.render = function(context)
{    
    context.save();
    
    context.translate(this.transform.x, this.transform.y);
    context.rotate(this.transform.rotation);
    context.scale(this.transform.scaleX, this.transform.scaleY);
    
    context.strokeStyle = this.strokeStyle;
    context.fillStyle = this.fillStyle;
    context.lineWidth = this.lineWidth;
    
    context.beginPath();
    context.rect(-this.transform.width * this.transform.pivotX,
                 -this.transform.height * this.transform.pivotY,
                 this.transform.width,
                 this.transform.height);
    
    context.stroke();
    context.fill();
    
    context.restore();
};