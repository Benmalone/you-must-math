function TextRenderer(entity) {
    
    Component.call(this);
    
    this.entity = entity;
    
    this.transform = entity.transform;
    this.textComponent = entity.textComponent;
    
}

TextRenderer.prototype = Object.create(Component.prototype,
                                {
                                    constructor: 
                                    {
                                        configurable: true,
                                        enumerable: true,
                                        value: TextRenderer,
                                        writable: true
                                    }
                                });

TextRenderer.prototype.render = function(context)
{
    context.save();
    
    context.translate(this.transform.x, this.transform.y);
    context.rotate(this.transform.rotation);
    context.scale(this.transform.scaleX, this.transform.scaleY);
    
    context.fillStyle = this.textComponent.color;
    context.strokeStyle = this.textComponent.outlineColor;
    context.textBaseline = this.textComponent.textBaseline;
    
    context.font = this.textComponent.fontSize + "px " + this.textComponent.fontFamily;
    
    if (this.textComponent.color != "none")
    {
        context.fillText(this.textComponent.text + this.textComponent._cursorText,
                        -this.transform.width * this.transform.pivotX, 
                        -this.transform.height * this.transform.pivotY);   
    }
    
    if (this.textComponent.outlineColor != "none")
    {
        context.strokeText(this.textComponent.text + this.textComponent._cursorText,
                          -this.transform.width * this.transform.pivotX, 
                          -this.transform.height * this.transform.pivotY);   
    }
    
    context.restore();
};