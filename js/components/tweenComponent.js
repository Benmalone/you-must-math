function TweenComponent(entity)
{
    this.entity = entity;
    this.tweens = {};
}

TweenComponent.prototype = Object.create(Component.prototype,
                                {
                                    constructor: 
                                    {
                                        configurable: true,
                                        enumerable: true,
                                        value: TweenComponent,
                                        writable: true
                                    }
                                });

TweenComponent.prototype.update = function()
{
    for (var property in this.tweens)
    {
        if (this.tweens.hasOwnProperty(property) && this.tweens[property][0] != null)
        {
            this.tweens[property][0].update();
        }
    }
};

TweenComponent.prototype.createTween = function(component, from, to, totalFrames, easing, callBack)
{
    // Ensures that the passed in component is connected to the same
    // entity as the tweenComponent
    if (component.entity != this.entity)
    {
        return;
    }
    
    var tween = new Tween(component, from, to, totalFrames, easing, callBack);
    
    if (this.tweens[component.constructor.name] == null)
    {
        this.tweens[component.constructor.name] = new Array();
    }
    
    this.tweens[component.constructor.name].push(tween);
    tween.manager = this;
    
    return tween;
};

TweenComponent.prototype.move = function(x1, y1, x2, y2, totalFrames, callBack)
{
    this.createTween(this.entity.transform, {x: x1, y: y1}, {x: x2, y: y2}, totalFrames, "smoothStep", callBack);
};

TweenComponent.prototype.clip = function(startWidth, endWidth, totalFrames, callBack)
{
    this.createTween(this.entity.imageRenderer, {mask: startWidth}, {mask: endWidth}, totalFrames, "smoothStep", callBack);
};

TweenComponent.prototype.removeTween = function(tween)
{
    this.tweens[tween.component.constructor.name].shift();
    
    if (this.tweens[tween.component.constructor.name].length == 0)
    {
        this.remove(tween.component);
    }
};

TweenComponent.prototype.remove = function(component) 
{
    delete this.tweens[component.constructor.name];
};

TweenComponent.prototype.stopAll = function() 
{
    this.tweens = {};
};