function AnimationComponent(entity)
{
    Component.call(this);
    
    this.entity = entity;
    this.imageComponent = entity.imageComponent;
    
    this.columns = this.imageComponent.image.width / this.entity.transform.width;
    this.currentFrameIndex = 0;
    this.currentFrame = 0;
    
    this.nextAnimationTime = Date.now();
    
    this.animations = [];
    
    this.clip;
    this.isPlaying = false;
}

AnimationComponent.prototype = Object.create(Component.prototype,
                                {
                                    constructor: 
                                    {
                                        configurable: true,
                                        enumerable: true,
                                        value: AnimationComponent,
                                        writable: true
                                    }
                                });

AnimationComponent.prototype.update = function() 
{
    if (this.isPlaying)
    {
        this._updatePlayingAnimation();
    }
};

AnimationComponent.prototype._updatePlayingAnimation = function() 
{
    if (this.nextAnimationTime < Date.now())
    {
        this.currentFrameIndex++
        this._stepOrLoopAnimation();
        this.setFrame(this.clip.frames[this.currentFrameIndex]);
        
        this.nextAnimationTime = Date.now() + this.clip.frameRate;
    }
};

AnimationComponent.prototype._stepOrLoopAnimation = function() 
{
    if (this.clip.loop)
    {
        this.isPlaying = true;
    } 
    else
    {
        this.stop();
    }
    
    if (this._isComplete())
    {
        this._performCompletion();
    }
};

AnimationComponent.prototype._isComplete = function() 
{
    return this.currentFrameIndex > this.clip.frames.length - 1;
};

AnimationComponent.prototype._performCompletion = function() 
{
    if (this.clip.loop)
    {
        this.currentFrameIndex = 0;
    } 
    else
    {
        this.currentFrameIndex = this.clip.frames.length - 1;
    }

    if (this.clip.onComplete)
    {
        this.clip.onComplete();
    }
}

AnimationComponent.prototype.add = function(key, frames, frameRate, loop, onComplete)
{
    this.animations[key] = 
    {
        name: key,
        frames: frames,
        frameRate: frameRate,
        loop: loop,
        onComplete: onComplete
    };
};

AnimationComponent.prototype.play = function(key)
{
    this.clip = this.animations[key];
    this.currentFrameIndex = 0;
    this.setFrame(this.clip.frames[this.currentFrameIndex]);
    this.isPlaying = true;
};

AnimationComponent.prototype.stop = function(key)
{
    this.isPlaying = false;
};

AnimationComponent.prototype.setFrame = function(frame)
{
    this.currentFrame = frame;
    
    this.imageComponent.sourceOffsetX = Math.floor(this.currentFrame % this.columns) * this.imageComponent.sourceWidth;
    this.imageComponent.sourceOffsetY = Math.floor(this.currentFrame / this.columns) * this.imageComponent.sourceHeight;
};