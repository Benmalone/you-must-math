function TextComponent(entity, text, fontFamily, fontSize, color) {
    
    Component.call(this);
    
    this.entity = entity;
    
    this.fontFamily = fontFamily;
    this.fontSize = fontSize;
    this.color = color;
    this.outlineColor = "none";
    this.textBaseline = "top";
    
    this._isCursor = false;
    this.cursorChangeDelay = 300;
    this._nextCursorChangeTime = Date.now();
    this._cursorText = "";
    
    this._text = "";
    this.text = text;
}

TextComponent.prototype = Object.create(Component.prototype,
                                {
                                    constructor: 
                                    {
                                        configurable: true,
                                        enumerable: true,
                                        value: TextComponent,
                                        writable: true
                                    }
                                });

TextComponent.prototype.update = function()
{
    if (this.isCursor)
    {
        this._updateCursor();
    }
};

TextComponent.prototype._updateCursor = function()
{
    if (this._nextCursorChangeTime < Date.now())
    {
        this._toggleCursor();
        this._nextCursorChangeTime = Date.now() + this.cursorChangeDelay;
    }
};

TextComponent.prototype._toggleCursor = function()
{
    if (this._cursorText == "")
    {
        this._cursorText = "|";
    }
    else 
    {
        this._cursorText = "";
    }
};

Object.defineProperty(TextComponent.prototype, "text", 
{
    get: function()
    {
        return this._text;
    },
    
    set: function(value)
    {
        this._text = value;
        this.measureText();
    }
});

Object.defineProperty(TextComponent.prototype, "isCursor", 
{
    get: function()
    {
        return this._isCursor;
    },
    
    set: function(value)
    {
        if (!value)
        {
            this._cursorText = "";
        }
        
        this._isCursor = value;
    }
});

TextComponent.prototype.measureText = function()
{
    var context = this.entity.game.context;
    
    context.save();
    
    context.fillStyle = this.color;
    context.strokeStyle = this.outlineColor;
    context.textBaseline = this.textBaseline;
    
    context.font = this.fontSize + "px " + this.fontFamily;
    
    var textWidth = context.measureText(this.text).width;
    var textHeight = context.measureText("M").width;
    
    this.entity.transform.setNativeSize(textWidth, textHeight);
    
    context.restore();
};

TextComponent.prototype.copyTextSize = function(text)
{
    var context = this.entity.game.context;
    
    context.save();
    
    context.fillStyle = this.color;
    context.strokeStyle = this.outlineColor;
    context.textBaseline = this.textBaseline;
    
    context.font = this.fontSize + "px " + this.fontFamily;
    
    var textWidth = context.measureText(text).width;
    var textHeight = context.measureText("M").width;
    
    this.entity.transform.setNativeSize(textWidth, textHeight);
    
    context.restore();
};