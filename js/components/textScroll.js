function TextScroll(textComponent, message, step, endDelay, onComplete)
{
    this.textComponent = textComponent;
    this.message = message;
    this.step = step;
    this.endDelay = endDelay;
    
    this.stepCount = 0;
    
    this.length = 0;
    
    this.onComplete = onComplete;
    
    this.manager;
    
    this.textComponent.copyTextSize(this.message);
}

TextScroll.prototype.update = function()
{
    
    this.stepCount++;
    
    if (this.step <= this.stepCount && this.length < this.message.length)
    {
        this.length++;
        
        this.textComponent.text = this.message.substr(0, this.length);
        
        this.stepCount = 0;
    } 
    else if (this.endDelay <= this.stepCount && this.length >= this.message.length)
    {
        if (this.onComplete)
        {
            this.onComplete();
        }
        
        this.manager.remove();
    }
    
    this.textComponent.copyTextSize(this.message);
};