function TextScrollComponent(entity)
{
    this.entity = entity;
    this.textComponent = entity.textComponent;
    
    this.textScrolls = [];
}

TextScrollComponent.prototype = Object.create(Component.prototype,
                                {
                                    constructor: 
                                    {
                                        configurable: true,
                                        enumerable: true,
                                        value: TextScrollComponent,
                                        writable: true
                                    }
                                });

TextScrollComponent.prototype.update = function()
{
    if (this.textScrolls[0])
    {
        this.textScrolls[0].update();
    }
};

TextScrollComponent.prototype.create = function(message, step, endDelay, onComplete)
{
    var textScroll = new TextScroll(this.textComponent, message, step, endDelay, onComplete);
    
    this.textScrolls.push(textScroll);
    textScroll.manager = this;
    
    return textScroll;
};

TextScrollComponent.prototype.remove = function()
{
    this.textScrolls.shift();
};

TextScrollComponent.prototype.jumpToEnd = function() 
{
    // TODO: Implement skippable text scrolls.
};

TextScrollComponent.prototype.stopAll = function() 
{
    this.textScrolls = [];
};