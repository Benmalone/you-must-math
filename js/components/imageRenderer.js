function ImageRenderer(entity) {
    
    Component.call(this);
    
    this.entity = entity;
    this.transform = entity.transform;

    this.imageComponent = entity.imageComponent;
    
    this.mask = null;
    
}

ImageRenderer.prototype = Object.create(Component.prototype,
                                {
                                    constructor: 
                                    {
                                        configurable: true,
                                        enumerable: true,
                                        value: ImageRenderer,
                                        writable: true
                                    }
                                });

ImageRenderer.prototype.render = function(context)
{
    context.save();
    
    context.translate(this.transform.x, this.transform.y);
    context.rotate(this.transform.rotation);
    
    if (this.mask != null)
    {
        this._clipImage(context);
    }
    
    context.drawImage(this.imageComponent.image, 
                      this.imageComponent.sourceOffsetX, 
                      this.imageComponent.sourceOffsetY, 
                      this.imageComponent.sourceWidth, 
                      this.imageComponent.sourceHeight,
                      -this.transform.width * this.transform.pivotX, 
                      -this.transform.height * this.transform.pivotY, 
                      this.transform.width, 
                      this.transform.height);
    
    context.restore();
};

ImageRenderer.prototype._clipImage = function(context)
{
    context.beginPath();
    context.rect(-this.transform.width * this.transform.pivotX,
                 -this.transform.height * this.transform.pivotY, 
                 this.mask,
                 this.transform.height);
    context.clip();
};

ImageRenderer.prototype.setMask = function(width)
{
    this.mask = width;
};