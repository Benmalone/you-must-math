function Transform(entity, x, y, width, height) {
    
    Component.call(this);
    
    this.entity = entity;
    
    this.x = x;
    this.y = y;
    this._z = 0;
    
    this.rotation = 0;
    
    this._scaleX = 1;
    this._scaleY = 1;
    
    this._width = width;
    this._height = height;
    
    this.pivotX = 0;
    this.pivotY = 0;
    
}

Transform.prototype = Object.create(Component.prototype,
                                {
                                    constructor:
                                    {
                                        configurable: true,
                                        enumerable: true,
                                        value: Transform,
                                        writable: true
                                    }
                                });

Transform.prototype.setNativeSize = function(width, height)
{
    var currentScaleX = this._scaleX;
    var currentScaleY = this._scaleY;
    
    this._width = width;
    this._height = height;
    this._scaleX = 1;
    this._scaleY = 1;
    
    this.scaleX = currentScaleX;
    this.scaleY = currentScaleY;
};

Object.defineProperty(Transform.prototype, "width", 
{
    get: function()
    {
        return this._width;
    },
    
    set: function(value)
    {
        this._scaleX = value / (this._width / this._scaleX)
        this._width = value;
    }
});

Object.defineProperty(Transform.prototype, "height", 
{
    get: function()
    {
        return this._height;
    },
    
    set: function(value)
    {
        this._scaleY = value / (this._height / this._scaleY)
        this._height = value;
    }
});

Object.defineProperty(Transform.prototype, "scaleX", 
{
    get: function()
    {
        return this._scaleX;
    },
    
    set: function(value)
    {
        this._width = value * (this._width / this._scaleX)
        this._scaleX = value;
    }
});

Object.defineProperty(Transform.prototype, "scaleY", 
{
    get: function()
    {
        return this._scaleY;
    },
    
    set: function(value)
    {
        this._height = value * (this._height / this._scaleY)
        this._scaleY = value;
    }
});

Object.defineProperty(Transform.prototype, "z", 
{
    get: function()
    {
        return this._z;
    },
    
    set: function(value)
    {
        this._z = value;
    
        if (this.entity.isEnabled)
        {
            this.entity.game.sortActiveEntitiesByZ();
        }
    }
});

Object.defineProperty(Transform.prototype, "top", 
{
    get: function()
    {
        return this.y - (this._height * this.pivotY);
    },
    
    set: function(value)
    {
        this.y = value + (this._height * this.pivotY);
    }
});

Object.defineProperty(Transform.prototype, "bottom", 
{
    get: function()
    {
        return this.y + (this._height * (1 - this.pivotY));
    },
    
    set: function(value)
    {
        this.y = value - (this._height * (1 - this.pivotY));
    }
});

Object.defineProperty(Transform.prototype, "left", 
{
    get: function()
    {
        return this.x - (this._width * this.pivotX);
    },
    
    set: function(value)
    {
        this.x = value + (this._width * this.pivotX);
    }
});

Object.defineProperty(Transform.prototype, "right", 
{
    get: function()
    {
        return this.x + (this._width * (1 - this.pivotX));
    },
    
    set: function(value)
    {
        this.x = value - (this._width * (1 - this.pivotX));
    }
});

Object.defineProperty(Transform.prototype, "centerX", 
{
    get: function()
    {
        return this.left + (this._width / 2);
    },
    
    set: function(value)
    {
        this.left = value - (this._width / 2);
    }
});

Object.defineProperty(Transform.prototype, "centerY", 
{
    get: function()
    {
        return this.top + (this._height / 2);
    },
    
    set: function(value)
    {
        this.top = value - (this._height / 2);
    }
});
