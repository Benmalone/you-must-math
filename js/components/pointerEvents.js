function PointerEvents(entity) {
    
    Component.call(this);
    
    this.entity = entity;
    this.transform = entity.transform;
    
    this.click = [];
    this.down = [];
    this.up = [];
    this.over = [];
    this.out = [];
    
    this.isDown = false;
    this.isOver = false;
    
    this.entity.game.eventManager.add(this.entity);
    
}

PointerEvents.prototype = Object.create(Component.prototype,
                                {
                                    constructor: 
                                    {
                                        configurable: true,
                                        enumerable: true,
                                        value: PointerEvents,
                                        writable: true
                                    }
                                });

PointerEvents.prototype.notify = function(event)
{    
    var eventHandlers = this[event];
    
    for (var i = 0; i < eventHandlers.length; i++)
    {
        eventHandlers[i].call(this.entity);
    }
};