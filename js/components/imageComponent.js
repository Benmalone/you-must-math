function ImageComponent(entity, image, sourceWidth, sourceHeight) {
    
    Component.call(this);
    
    this.entity = entity;
    
    this.image = image;
    this.sourceWidth = sourceWidth || image.width;
    this.sourceHeight = sourceHeight || image.height;
    this.sourceOffsetX = 0;
    this.sourceOffsetY = 0;
    
}

ImageComponent.prototype = Object.create(Component.prototype,
                                {
                                    constructor: 
                                    {
                                        configurable: true,
                                        enumerable: true,
                                        value: ImageComponent,
                                        writable: true
                                    }
                                });

ImageComponent.prototype.setImage = function(src, sourceWidth, sourceHeight) 
{
    this.image = this.entity.game.assetManager.getAsset(src);
    this.sourceWidth = sourceWidth || this.image.width;
    this.sourceHeight = sourceHeight || this.image.height;
};