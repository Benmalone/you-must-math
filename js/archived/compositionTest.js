var HALLWAY_SRC = "assets/images/hallway.jpg",
    PLAYER_SRC  = "assets/sprites/player.png";

var game = new Game("game", {load: load, setup: setup, update: update, render: render});
game.start();

function load() 
{
    game.assetManager.queueDownload(HALLWAY_SRC);
    game.assetManager.queueDownload(PLAYER_SRC);
}

function setup()
{
    var image = game.create.image(0, 0, 960, 540, HALLWAY_SRC);
    image.transform.scaleX = 0.5;
    image.transform.scaleY = 0.5;
    image.transform.setZ(-10);
    
    game.create.image(0, 0, 960, 540, HALLWAY_SRC);
    
    var label = game.create.label(0, 0, "Hello World", "sans-serif", 16, "blue");
    label.transform.setZ(-11);
    label.tweenComponent.createTween(label.transform, {x: 0}, {x: game.width}, 120, "smoothStep", function(){
        label.tweenComponent.createTween(label.transform, {scaleX: 1, scaleY: 1, rotation: 0}, {scaleX: 5, scaleY: 5, rotation: Math.PI * 10}, 120, "smoothStep");
    });
    
    label.tweenComponent.createTween(label.transform, {x: game.width, y: 0}, {x: game.width / 2, y: game.height / 2}, 120, "smoothStep");
    
    game.create.inputField(4, game.height - 150, 150, 24);
    
    var player = game.create.sprite(0,  game.height - 128, 128, 128, PLAYER_SRC);
    player.addAnimation("left", [2, 3, 4, 3], 167);
    
    var leftButton = game.create.button(game.width / 2 - 256, game.height - 128, 128, 48, makeEquation());
    leftButton.transform.scaleX = 2;
    leftButton.events.click.push(function() {
        player.playAnimation("left");
    });
    
    setInterval(function(){
        player.setActive(!player.isEnabled);
    }, 500);
}

function update() 
{
    // Not being used in demo
}

function render() 
{
    // Not being used in demo
}
