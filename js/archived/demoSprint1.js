// This was just a demo so no effort was up into making it clean, readable,
// and sensible


var game = new Game("game", {load: load, setup: setup, update: update, render: render});
game.start();

function load() 
{
    game.assetManager.queueDownload("assets/sprites/player.png");
    game.assetManager.queueDownload("assets/sprites/enemy.png");
    game.assetManager.queueDownload("assets/images/battle.png");
    game.assetManager.queueDownload("assets/images/credits.jpg");
    game.assetManager.queueDownload("assets/images/title.jpg");
    game.assetManager.queueDownload("assets/images/exploration.png");
    game.assetManager.queueDownload("assets/images/wizardVictory.png");
    game.assetManager.queueDownload("assets/images/conceptBat.jpg");
}

function setup()
{
    
    displayMenu();
    
}

function update() 
{
    // Not being used in demo
}

function render() 
{
    // Not being used in demo
}

function displayMenu()
{
    game.clear();
    
    var titleImage = game.assetManager.getAsset("assets/images/title.jpg");
    var titleScreen = new StaticImage(titleImage, 0, 0, 1280, 540);
    game.add(titleScreen);
    
    var startButton = new Button(game.width / 2 - 64, game.height / 2 + 196, 128, 48, "Start");
    game.add(startButton);
    game.eventManager.addEvent(startButton, "click", loadDemo);
    
    var creditButton = new Button(game.width / 2 - 64, game.height / 2 + 256, 128, 48, "Credits");
    game.add(creditButton);
    game.eventManager.addEvent(creditButton, "click", loadCredits);
	
	var mathButton = new Button(game.width / 2 - 64, game.height / 2 + 306, 128, 48, makeEquation());
    game.add(mathButton);
	
	
}

function loadDemo()
{
    game.clear();
    
    var navigationImage = game.assetManager.getAsset("assets/images/exploration.png");
    var navigation = new StaticImage(navigationImage, 0, 0, 1280, 540);
    game.add(navigation);
    
    var battleImage = game.assetManager.getAsset("assets/images/battle.png");
    var battle = new StaticImage(battleImage, -1280, 0, 1280, 540);
    game.add(battle);
    
    var playerImage = game.assetManager.getAsset("assets/sprites/player.png");
    var player = new Sprite(playerImage, 0,  game.height - 128, 128, 128);
    game.add(player);
    player.addAnimation("down", [0, 1], 167);
    player.addAnimation("left", [2, 3, 4, 3], 167);
    player.addAnimation("up", [5, 6], 167);
    player.addAnimation("right", [7, 8, 9, 8], 167);

    var enemyImage = game.assetManager.getAsset("assets/sprites/enemy.png");
    var enemy = new Sprite(enemyImage, game.width - 128,  game.height - 128, 128, 128);
    game.add(enemy);
    enemy.addAnimation("down", [0, 1], 167);
    enemy.addAnimation("left", [2, 3, 4, 3], 167);
    enemy.addAnimation("up", [5, 6], 167);
    enemy.addAnimation("right", [7, 8, 9, 8], 167);

    var leftButton = new Button(game.width / 2 - 256, game.height - 128, 128, 48, "Left");
    game.add(leftButton);
    
    game.eventManager.addEvent(leftButton, "click", function(){
        player.playAnimation("left");
        enemy.playAnimation("left");
        game.tweenManager.createTween(navigation, {x: navigation.x}, {x: 0}, 120, "smoothStep");
        game.tweenManager.createTween(battle, {x: battle.x}, {x: -game.width}, 120, "smoothStep");
    });
    
    var rightButton = new Button(game.width / 2 + 128, game.height - 128, 128, 48, "Right");
    game.add(rightButton);
    
    game.eventManager.addEvent(rightButton, "click", function(){
        player.playAnimation("right");
        enemy.playAnimation("right");
        game.tweenManager.createTween(navigation, {x: navigation.x}, {x: game.width}, 120, "smoothStep");
        game.tweenManager.createTween(battle, {x: battle.x}, {x: 0}, 120, "smoothStep");
    });

    var upButton = new Button(game.width / 2 - 64, game.height - 180, 128, 48, "Up");
    game.add(upButton);
    game.eventManager.addEvent(upButton, "click", function(){
        player.playAnimation("up");
        enemy.playAnimation("up");
    });

    var downButton = new Button(game.width / 2 - 64, game.height - 52, 128, 48, "Down");
    game.add(downButton);
    game.eventManager.addEvent(downButton, "click", function(){
        player.playAnimation("down");
        enemy.playAnimation("down");
    });
    
    var winButton = new Button(game.width - 156, 16, 128, 48, "Win");
    game.add(winButton);
    game.eventManager.addEvent(winButton, "click", loadVictory);
}

function loadCredits() 
{
    game.clear();
    
    var creditImage = game.assetManager.getAsset("assets/images/credits.jpg");
    var credits = new StaticImage(creditImage, 0, 0, 1280, 720);
    game.add(credits);
    
    var backButton = new Button(4, 4, 128, 48, "Back");
    game.add(backButton);
    game.eventManager.addEvent(backButton, "click", displayMenu);
}

function loadVictory()
{
    game.clear();
    
    var victoryImage = game.assetManager.getAsset("assets/images/wizardVictory.png");
    var victory = new StaticImage(victoryImage, 56, 56, 900, 140);
    game.add(victory);
    
    var batImage = game.assetManager.getAsset("assets/images/conceptBat.jpg");
    var bat = new StaticImage(batImage, game.width / 2, game.height / 2, game.width / 2, game.height / 2);
    game.add(bat);
    
    var restartButton = new Button(game.width / 2 - 64, game.height / 2, 128, 48, "Restart");
    game.add(restartButton);
    game.eventManager.addEvent(restartButton, "click", displayMenu);
}