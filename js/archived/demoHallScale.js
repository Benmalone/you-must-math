var HALLWAY_SRC = "assets/images/hallway.jpg";

var game = new Game("game", {load: load, setup: setup, update: update, render: render});
game.start();

function load() 
{
    game.assetManager.queueDownload(HALLWAY_SRC);
}

function setup()
{
    var mathAnswer = new InputBox(game.width / 2 - 64, game.height / 2 + 196, 128, 48, "");
    game.add(mathAnswer);
    
    var hallways = [new StaticImage(game.assetManager.getAsset(HALLWAY_SRC), 0, 0, 960, 540)];
    game.add(hallways[0]);
    
    for (var i = 1; i < 6; i++)
    {
        hallways.push(new StaticImage(game.assetManager.getAsset(HALLWAY_SRC), 0, 0, 960, 540));
        hallways[i].scaleX = hallways[i - 1].scaleX / 2;
        hallways[i].scaleY = hallways[i - 1].scaleY / 2;
        game.add(hallways[i]);
    }
    
    /*
    
    Testing for rounding error in floats. By positioning rather than
    scaling
    
    var closeHallway = new StaticImage(game.assetManager.getAsset(HALLWAY_SRC), 0, 0, 960, 540);
    game.add(closeHallway);
    
    var farHallway = new StaticImage(game.assetManager.getAsset(HALLWAY_SRC), 240, 135, 480, 270);
    game.add(farHallway);
    
    */
    
}

function update() 
{
    // Not being used in demo
}

function render() 
{
    // Not being used in demo
}
