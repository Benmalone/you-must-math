// Helper object directions
Direction = { 
    UP: 0,
	RIGHT: 1,
	DOWN: 2,
	LEFT: 3,
    
	vectors:
    [
		// UP
		{x: 0, y: -1},
		// RIGHT
		{x: 1, y: 0},
		// DOWN
		{x: 0, y: 1},
		// LEFT
		{x: -1, y: 0}
	],
    
    angles:
    [
		// UP
		3 * Math.PI / 2,
		// RIGHT
		0,
		// DOWN
		Math.PI / 2,
		// LEFT
		Math.PI
	],
    
    names:
    [
        "up",
        "right",
        "down",
        "left"
    ],
	
	getVector: function(direction)
    {
		return this.vectors[direction];
	},
    
    getAngle: function(direction)
    {
		return this.angles[direction];
	},
    
    getName: function(direction)
    {
        return this.names[direction];
    },
    
    getOppositeDirection: function(direction)
    {
        switch(direction)
        {
            case this.UP:
                return this.DOWN;
                break;
            case this.DOWN:
                return this.UP;
                break;
            case this.LEFT:
                return this.RIGHT;
                break;
            case this.RIGHT:
                return this.LEFT;
                break;
        }
    }
};