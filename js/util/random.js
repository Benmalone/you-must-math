function Random() {}

Random.intRange = function(min, max)
{
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

Random.floatRange = function(min, max)
{
    return Math.random() * (max - min + 1) + min;
};