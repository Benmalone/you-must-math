var operation = ['+', '-', 'x', '/'];
var numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
var opNum, num1, num2;
attackChoice = 0;
var equation, solution, answer;

function mathGen()
{
	
}

function operationGen()
{
	//generates a random number that corresponds to the operator
	opNum = Math.floor((Math.random() * 4));
}

function numberGen()
{
	//single digit simple generation
	num1 = Math.floor((Math.random() * 7) + 2);
	num2 = Math.floor((Math.random() * 7) + 2);
	num3 = Math.floor((Math.random() * 18) + 2);
	
	//double digit generation (25-50 and 1-10)
	num4 = Math.floor((Math.random() * 68) + 4);
	num15 = Math.floor((Math.random() * 25) + 25);
	num5 = Math.floor((Math.random() * 18) + 2);
	num6 = Math.floor((Math.random() * 7) + 2);
	
	//digit for easy multiplication
	num7 = Math.floor((Math.random() * 3) + 2);
	//digit for adv multiplication
	num8 = Math.floor((Math.random() * 8) + 4);
	num14 = Math.floor((Math.random() * 8) + 4);
	//digit for med multiplication 
	num9 = Math.floor((Math.random() * 15) + 10);
	num12 = Math.floor((Math.random() * 7) + 2);
	num13 = Math.floor((Math.random() * 7) + 2);
	//digit for med division
	num10 = Math.floor((Math.random() * 7) + 2);
	num11 = Math.floor((Math.random() * 40) + 10);
	
}

function setEnemyAttack(enemy)
{
	if(!bossBattle)
	{
		if(add == 1 && sub == 1)
		{
			attackChoice = Math.floor((Math.random() * 2));
		}
		else if (mult == 1 && div == 1)
		{
			attackChoice = Math.floor((Math.random() * 2) + 2);
		}
		else if(add == 1 && sub == 0)
		{
			attackChoice = 0;
		}
		else if(sub == 1 && add == 0)
		{
			attackChoice = 1;
		}
		else if(mult == 1 && div == 0)
		{
			attackChoice = 2;
		}
		else if(div == 1 && mult == 0)
		{
			attackChoice = 3;
		}
	}
	else
	{
		if(add == 1 && sub == 1 && mult == 0 && div == 0)
		{
			attackChoice = Math.floor((Math.random() * 2));
		}
		else if (mult == 1 && div == 1 && add == 0 && sub == 0)
		{
			attackChoice = Math.floor((Math.random() * 2) + 2);
		}
		else
			attackChoice = Math.floor((Math.random() * 4));
	}
	
	return attackChoice;
}

function makeEquation()
{
	numberGen();
	opNum = attackChoice;
	
	if(opNum == 0)
	{
		equation = num1 + " " + operation[opNum] + " " + num2 + " = ";
		solution = num1 + num2;
	}
	else if(opNum == 1 && num1 >= num2)
	{
		solution = num1 - num2;
		if(solution == 0 && num1 != 9)
		{
			num1++;
			solution = num1 - num2;
		}
		if(solution == 0 && num1 == 9)
		{
			num2--;
			solution = num1 - num2;
		}
		equation = num1 + " " + operation[opNum] + " " + num2 + " = ";
		
	}
	else if(opNum == 1 && num1 < num2)
	{
		solution = num2 - num1;
		if(solution == 0 && num2 != 9)
		{
			num2++;
			solution = num2 - num1;
		}
		else if(solution == 0 && num1 == 9)
		{
			num1--;
			solution = num2 - num1;
		}
		equation = num2 + " " + operation[opNum] + " " + num1 + " = ";
	}
	else if(opNum == 2)
	{
		equation = num1 + " " + operation[opNum] + " " + num7 + " = ";
		solution = num1 * num7;
	}
	else if(opNum == 3)
	{
		makeDivision();
	}

	return equation;
}

function makeMedEquation()
{
	numberGen();
	opNum = attackChoice;
	
	if(opNum == 0)
	{
		equation = num9 + " " + operation[opNum] + " " + num6 + " = ";
		solution = num9 + num6;
	}
	else if(opNum == 1 && num9 >= num6)
	{
		equation = num9 + " " + operation[opNum] + " " + num6 + " = ";
		solution = num9 - num6;
	}
	else if(opNum == 1 && num9 < num6)
	{
		solution = num6 - num9;
		if(solution == 0 && num9 == 9)
		{
			num9--;
			solution = num6 - num9;
		}
		if(solution == 0 && num9 != 9)
		{
			num6++;
			solution = num6 - num9;
		}
		equation = num6 + " " + operation[opNum] + " " + num9 + " = ";
	}
	else if(opNum == 2)
	{
		equation = num12 + " " + operation[opNum] + " " + num13 + " = ";
		solution = num12 * num13;
	}
	else if(opNum == 3)
	{
		makeMedDivision();
	}

	return equation;
}

function makeAdvEquation()
{
	opNum = attackChoice;
	numberGen();
	
	if(opNum == 0)
	{
		equation = num15 + " " + operation[opNum] + " " + num5 + " = ";
		solution = num15 + num5;
	}
	else if(opNum == 1 && num15 >= num5)
	{
		equation = num15 + " " + operation[opNum] + " " + num5 + " = ";
		solution = num15 - num5;
	}
	else if(opNum == 1 && num4 < num5)
	{
		equation = num5 + " " + operation[opNum] + " " + num4 + " = ";
		solution = num5 - num15;
	}
	else if(opNum == 2)
	{
		equation = num8 + " " + operation[opNum] + " " + num14 + " = ";
		solution = num8 * num14;
	}
	else if(opNum == 3)
	{
		makeAdvDivision();
	}

	return equation;
}

function makeSuperAdvEquation()
{
	opNum = attackChoice;
	numberGen();
	
	if(opNum == 0)
	{
		equation = num1 + " " + operation[opNum] + " " + num2 + " " + operation[opNum] + " " + num3 + " = ";
		solution = num1 + num2 + num3;
	}
	else if(opNum == 1 && num5 >= num6)
	{
		equation = num4 + " " + operation[opNum] + " " + num5 + " " + operation[opNum] + " " + num6 + " = ";
		solution = num4 - num5 - num6;
	}
	else if(opNum == 1 && num5 < num6)
	{
		equation = num4 + " " + operation[opNum] + " " + num5 + " " + operation[opNum] + " " + num6 + " = ";
		solution = num4 - num5 - num6;
	}
	else if(opNum == 2)
	{
		equation = num1 + " " + operation[opNum] + " " + num2 + " = ";
		solution = num1 * num2;
	}
	else if(opNum == 3 && num4 > num5 && num4%num5 == 0 && num5!=0)
	{
		equation = num4 + " " + operation[opNum] + " " + num5 + " = ";
		solution = num4 / num5;
	}
	else if(opNum == 3 && num4 < num5 && num5%num4 == 0 && num4!=0)
	{
		equation = num5 + " " + operation[opNum] + " " + num4 + " = ";
		solution = num5 / num4;
	}
	else
	{
		makeSuperAdvEquation();
	}
	 
	return equation;
}

function makeDivision()
{
	opNum = attackChoice;
	numberGen();
	
	if(opNum == 3 && num7 > num2 && num7%num2 == 0 && num2!=0)
	{
		equation = num7 + " " + operation[opNum] + " " + num2 + " = ";
		solution = num7 / num2;
	}
	else if(opNum == 3 && num7 < num2 && num2%num7 == 0 && num7!=0)
	{
		equation = num2 + " " + operation[opNum] + " " + num7 + " = ";
		solution = num2 / num7;
	}
	else
	{
		makeDivision();
	}
}

function makeMedDivision()
{
	opNum = attackChoice;
	numberGen();
	
	if(opNum == 3 && num11 > num10 && num11%num10 == 0 && num10!=0)
	{
		equation = num11 + " " + operation[opNum] + " " + num10 + " = ";
		solution = num11 / num10;
	}
	else if(opNum == 3 && num11 < num10 && num10%num11 == 0 && num11!=0)
	{
		equation = num10 + " " + operation[opNum] + " " + num11 + " = ";
		solution = num10 / num11;
	}
	else
	{
		makeMedDivision();
	}
}

function makeAdvDivision()
{
	opNum = attackChoice;
	numberGen();
	
	if(opNum == 3 && num4 > num6 && num4%num6 == 0 && num6!=0)
	{
		equation = num4 + " " + operation[opNum] + " " + num6 + " = ";
		solution = num4 / num6;
	}
	else if(opNum == 3 && num4 < num6 && num6%num4 == 0 && num4!=0)
	{
		equation = num6 + " " + operation[opNum] + " " + num4 + " = ";
		solution = num6 / num4;
	}
	else
	{
		makeAdvDivision();
	}
}

function checkSolution(response)
{
	answer = parseInt(response);
    
    return answer == solution;
}
