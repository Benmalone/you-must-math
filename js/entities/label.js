function Label(game, x, y, text, fontFamily, fontSize, color)
{
    Entity.call(this, game, x, y, 0, 0);
    
    this.textComponent = new TextComponent(this, text, fontFamily, fontSize, color);
    this.textComponent.isCursor = false;
    
    this.textRenderer = new TextRenderer(this, false);
    this.tweenComponent = new TweenComponent(this);
}

Label.prototype = Object.create(Entity.prototype,
                                {
                                    constructor: 
                                    {
                                        configurable: true,
                                        enumerable: true,
                                        value: Label,
                                        writable: true
                                    }
                                });

Label.prototype.update = function()
{
    this.textComponent.update();
    this.tweenComponent.update();
};

Label.prototype.render = function(context)
{
    this.textRenderer.render(context);
};
