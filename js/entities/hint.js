function Hint(game, x, y, fontFamily, fontSize, color)
{
    Entity.call(this, game, x, y, 0, 0);
    
    this.textComponent = new TextComponent(this, "", fontFamily, fontSize, color);
    this.textComponent.isCursor = false;
    this.textScrollComponent = new TextScrollComponent(this);
    this.textRenderer = new TextRenderer(this);
    this.tweenComponent = new TweenComponent(this);
}

Hint.prototype = Object.create(Entity.prototype,
                                {
                                    constructor: 
                                    {
                                        configurable: true,
                                        enumerable: true,
                                        value: Hint,
                                        writable: true
                                    }
                                });

Hint.prototype.update = function()
{
    this.textComponent.update();
    this.tweenComponent.update();
    this.textScrollComponent.update();
};

Hint.prototype.render = function(context)
{
    this.textRenderer.render(context);
};
