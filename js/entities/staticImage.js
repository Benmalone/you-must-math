function StaticImage(game, x, y, width, height, image)
{
    Entity.call(this, game, x, y, width, height);
    
    this.imageComponent = new ImageComponent(this, image, width, height);
    this.imageRenderer = new ImageRenderer(this);
    this.tweenComponent = new TweenComponent(this);
    
    this.events = new PointerEvents(this);
}

StaticImage.prototype = Object.create(Entity.prototype,
                                {
                                    constructor: 
                                    {
                                        configurable: true,
                                        enumerable: true,
                                        value: StaticImage,
                                        writable: true
                                    }
                                });

StaticImage.prototype.update = function()
{
    this.tweenComponent.update();
};

StaticImage.prototype.render = function(context)
{
    this.imageRenderer.render(context);
};
