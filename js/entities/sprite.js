function Sprite(game, x, y, width, height, image)
{
    Entity.call(this, game, x, y, width, height);
    
    this.imageComponent = new ImageComponent(this, image, width, height);
    this.imageRenderer = new ImageRenderer(this);
    this.tweenComponent = new TweenComponent(this);
    this.animations = new AnimationComponent(this);
    this.events = new PointerEvents(this);
}

Sprite.prototype = Object.create(Entity.prototype,
                                {
                                    constructor: 
                                    {
                                        configurable: true,
                                        enumerable: true,
                                        value: Sprite,
                                        writable: true
                                    }
                                });

Sprite.prototype.update = function()
{
    this.tweenComponent.update();
    this.animations.update();
};

Sprite.prototype.render = function(context)
{
    this.imageRenderer.render(context);
};

Sprite.prototype.setFrame = function(frame)
{
    this.animations.setFrame(frame);
};

/*
@deprecated
*/
Sprite.prototype.addAnimation = function(key, frames, frameRate, loop, onComplete)
{
    console.warn("Sprite.addAnimation is deprecated. Use sprite.animations.add instead.");
    this.animations.add(key, frames, frameRate, loop, onComplete);
};

/*
@deprecated
*/
Sprite.prototype.playAnimation = function(key)
{
    console.warn("Sprite.playAnimation is deprecated. Use sprite.animations.play instead.");
    this.animations.play(key);
};
