function Entity(game, x, y, width, height)
{
    this.game = game;
    this.transform = new Transform(this, x, y, width, height);
    
    // Only change 'isEnabled' property through the setActive method
    this.isEnabled = true;
}

/*
 @abstract
*/
Entity.prototype.render = function(context) {};

Entity.prototype.setActive = function(state) 
{
    
    this.isEnabled = state;
    
    if (this.isEnabled)
    {
        this.game.addActive(this);
        
        if (this.events)
        {
            this.game.eventManager.add(this);
        }
    }
    else
    {
        this.game.removeActive(this);
        
        if (this.events)
        {
            this.game.eventManager.remove(this);
        }
    }
    
};