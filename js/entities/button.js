function Button(game, x, y, width, height, text)
{
    Entity.call(this, game, x, y, width, height);
    
    // Note: Allowing use of literal values for now 
    this.textComponent = new TextComponent(this, text, "sans-serif", 16, "black");
    
    //this.boxRenderer = new BoxRenderer(this);
    this.textRenderer = new TextRenderer(this, false);
    
    this.events = new PointerEvents(this);
}

Button.prototype = Object.create(Entity.prototype,
                                {
                                    constructor: 
                                    {
                                        configurable: true,
                                        enumerable: true,
                                        value: Button,
                                        writable: true
                                    }
                                });

Button.prototype.render = function(context)
{
    //this.boxRenderer.render(context);
    this.textRenderer.render(context);
};
