function InputField(game, x, y, width, height)
{
    Entity.call(this, game, x, y, width, height);
    
    this.textComponent = new TextComponent(this, "", "sans-serif", 42, "white");
    this.textComponent.isCursor = true;
    
    this.textRenderer = new TextRenderer(this);
}

InputField.prototype = Object.create(Entity.prototype,
                                {
                                    constructor: 
                                    {
                                        configurable: true,
                                        enumerable: true,
                                        value: InputField,
                                        writable: true
                                    }
                                });

InputField.prototype.update = function()
{
    this.textComponent.update();
};

InputField.prototype.render = function(context)
{
    this.textRenderer.render(context);
};
