function Preloader(game)
{
    this.game = game;
    
    this.outlineSRC;
    this.progressSRC;
    this.titleSRC;
    
    this.outline;
    this.progress;
    this.title;
}

Preloader.prototype.setup = function()
{
    this._createLoadingScreen();
    this.game.assetManager.downloadQueue = [];
};

Preloader.prototype._createLoadingScreen = function()
{    
    this.outline = this.game.create.image(0, 0, 248, 44, this.outlineSRC);  
    this.outline.transform.centerX = this.game.width / 2;
    this.outline.transform.centerY = this.game.height / 2;
    
    this.progress = this.game.create.image(0, 0, 248, 44, this.progressSRC);
    this.progress.transform.centerX = this.outline.transform.centerX;
    this.progress.transform.centerY = this.outline.transform.centerY;
    
    this.title = this.game.create.image(0, 0, 407, 63, this.titleSRC);
    this.title.transform.centerX = this.outline.transform.centerX;
    this.title.transform.bottom = this.outline.transform.top - this.title.transform.height;
};

Preloader.prototype.update = function()
{
    if (this.progress)
    {
        var progressPercentage = (this.game.assetManager.successCount + this.game.assetManager.errorCount) / this.game.assetManager.downloadQueue.length;
        this.progress.imageRenderer.setMask(this.progress.transform.width * progressPercentage);
    }
};

Preloader.prototype.remove = function()
{
    this.game.remove(this.outline);
    this.game.remove(this.progress);
    this.game.remove(this.title);
};