function GameFactory(game)
{
    this.game = game;
}

GameFactory.prototype = 
{
  
    sprite: function(x, y, width, height, src)
    {
        var image = this.game.assetManager.getAsset(src);
        var sprite = new Sprite(this.game, x, y, width, height, image);
        
        this.game.add(sprite);
        
        return sprite;
    },
    
    image: function(x, y, width, height, src)
    {
        var image = this.game.assetManager.getAsset(src);
        var staticImage = new StaticImage(this.game, x, y, width, height, image);
        
        this.game.add(staticImage);
        
        return staticImage;
    },
    
    button: function(x, y, width, height, text)
    {
        var button = new Button(this.game, x, y, width, height, text)
        
        this.game.add(button);
        
        return button;
    },
    
    label: function(x, y, text, fontFamily, fontSize, color)
    {
        var label = new Label(this.game, x, y, text, fontFamily, fontSize, color);
        
        this.game.add(label);
        
        return label;
    },
    
    hint: function(x, y, fontFamily, fontSize, color)
    {
        var hint = new Hint(this.game, x, y, fontFamily, fontSize, color);
        
        this.game.add(hint);
        
        return hint;
    },
    
    inputField: function(x, y, width, height)
    {
        var inputField = new InputField(this.game, x, y, width, height);
        
        this.game.add(inputField);
        
        return inputField;
    }
    
};