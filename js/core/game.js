function Game(canvasId, states)
{
    this.canvas = document.getElementById(canvasId);
    this.context = this.canvas.getContext("2d");
    
    this.height = this.canvas.height;
    this.width = this.canvas.width;
    
    this.scaleX = 1;
    this.scaleY = 1;
    
    this.preload = states.preload;
    this.load = states.load;
    this.setup = states.setup;
    this.update = states.update;
    this.render = states.render;
        
    this.entities = [];
    this.activeEntities = [];
    
    this.preloader = new Preloader(this);
    this.assetManager = new AssetManager();
    this.eventManager = new EventManager(this);
    this.create = new GameFactory(this);
    
    this.isLoading = true;
    
    this.elapsedTimeInSeconds = 0;
}

Game.prototype =
{
    start: function() {
        this._preloadAssets();
        this._internalUpdate();
    },
    
    _preloadAssets: function()
    {
        var self = this;
        
        if (this.preload) 
        {
            this.preload();
            
            this.assetManager.downloadAll(function() {
                self.preloader.setup();
                self._loadAssets();
                self.isLoading = true;
            });
        }
        else
        {
            this._loadAssets();
        }
    },
    
    _loadAssets: function()
    {
        var self = this;
        
        this.load();
            
        this.assetManager.downloadAll(function() {
            self.preloader.remove();
            self.setup();
            self.isLoading = false;
        });
    },
    
    _internalUpdate: function(timestamp)
    {
        requestAnimationFrame(this._internalUpdate.bind(this));
        
        if (!this.isLoading)
        {            
            this._updateEntitiesAndGameLogic();
        }
        else
        {
            this.preloader.update();
        }
        
        this._internalRender();
        
        this.elapsedTimeInSeconds = Math.floor(timestamp / 1000);
    },
    
    _updateEntitiesAndGameLogic: function()
    {
        for (var i = 0; i < this.activeEntities.length; i++)
        {
            if (this.activeEntities[i].update)
            {
                this.activeEntities[i].update();
            }  
        }

        if (this.update)
        {
            this.update();
        }
    },
    
    _internalRender: function()
    {        
        this._clearCanvas();
        this._renderActiveEntitiesAndCustomLogic();
    },
    
    _clearCanvas: function()
    {
        this.context.fillStyle = "black";
        this.context.fillRect(0, 0, this.width, this.height);
    },
    
    _renderActiveEntitiesAndCustomLogic: function()
    {
        for (var i = 0; i < this.activeEntities.length; i++)
        {
            this.activeEntities[i].render(this.context);   
        }
        
        if (this.render && !this.isLoading)
        {
            this.render();        
        }
    },
    
    add: function(entity)
    {
        if (this.entities.indexOf(entity) == -1) 
        {
            this.entities.push(entity);
            this.addActive(entity);
        }
    },
    
    remove: function(entity)
    {
        var indexToRemove = this.entities.indexOf(entity);
        
        if (indexToRemove != -1) 
        {
            if(entity.isEnabled)
            {
                this.removeActive(entity);
            }
            
            this.entities.splice(indexToRemove, 1);
        }
    },
    
    addActive: function(entity)
    {
        if (entity.isEnabled && this.activeEntities.indexOf(entity) == -1)
        {
            this.activeEntities.push(entity);
            this.sortActiveEntitiesByZ();
        }
    },
    
    removeActive: function(entity)
    {
        var indexToRemove = this.activeEntities.indexOf(entity);
        
        if (indexToRemove != -1)
        {
            this.activeEntities.splice(indexToRemove, 1);
        }
    },
    
    clear: function()
    {
        this.entities = [];
        this.activeEntities = [];
        this.eventManager.interactiveEntities = [];
    },
    
    sortActiveEntitiesByZ: function()
    {
        this.activeEntities.sort(function(entity1, entity2) {
            return entity2.transform.z - entity1.transform.z;
        });
    },
    
    setScale: function()
    {
        var widthScale = 1; 
        var heightScale = 1;

        if (this.canvas.style.width != "")
        {
            var displayedWidth = this.canvas.style.width;
            displayedWidth = displayedWidth.substr(0, displayedWidth.length - 2);
            widthScale = displayedWidth / this.canvas.width;
        }

        this.scaleX = widthScale;

        if (this.canvas.style.height != "")
        {
            var displayedHeight = this.canvas.style.height;
            displayedHeight = displayedHeight.substr(0, displayedHeight.length - 2);
            heightScale = displayedHeight / this.canvas.height;
        }

        this.scaleY = heightScale;
    }

};