window.addEventListener("resize", OnResizeCalled, false);

function OnResizeCalled()
{
	var canvas = document.getElementById("game");
	var tempDisplay = canvas.style.display;
	
	canvas.width = 1280;
	canvas.height = 720;
	
	var rect = canvas.getBoundingClientRect();
	
	var gameWidth = window.innerWidth;
	var gameHeight = window.innerHeight;
	var scaleToFitX = (gameWidth / canvas.width); // - 0.36*(gameWidth / canvas.width); 36 = 18% * 2
	var scaleToFitY = (gameHeight / canvas.height);
	
	var currentScreenRatio = gameWidth/gameHeight;
	var optimalRatio = Math.min(scaleToFitX, scaleToFitY);
	
	canvas.style.position = "fixed";
	canvas.style.top = 0*gameHeight + "px";
	
	if (currentScreenRatio >= 1.77 && currentScreenRatio <= 1.77)   // 1.77 close to perfect
	{
		canvas.style.width = gameWidth + "px";
		canvas.style.height = gameHeight + "px";
		canvas.style.left = 0 + "px";
	}
	else
	{
		canvas.style.width = canvas.width * optimalRatio + "px";
		canvas.style.height = canvas.height * optimalRatio + "px";
		canvas.style.left = ( (gameWidth - (canvas.width * optimalRatio)) / 2 ) + "px";
	}
	
	canvas.style.display = tempDisplay;
}