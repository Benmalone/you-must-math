function EventManager(game)
{
    this.interactiveEntities = [];
    
    this.game = game;
    
    this._init();
}

EventManager.Events = {
    OVER: "over",
    OUT: "out",
    DOWN: "down",
    UP: "up",
    CLICK: "click"
};

EventManager.prototype._init = function()
{
    this.game.canvas.addEventListener("mousedown", this._checkMouseDownEvent.bind(this), false);
    this.game.canvas.addEventListener("mouseup", this._checkMouseUpEvent.bind(this), false);
    this.game.canvas.addEventListener("mousemove", this._checkMouseMoveEvent.bind(this), false);
    window.addEventListener("resize", this.game.setScale.bind(this.game), false);
};

EventManager.prototype._checkMouseDownEvent = function(event)
{    
    var x = event.pageX - this.game.canvas.offsetLeft;
    var y = event.pageY - this.game.canvas.offsetTop;
    
    for (var i = this.interactiveEntities.length - 1; i >= 0; i--)
    {
        var entity = this.interactiveEntities[i];
        
        // Need to check if the entity exists because more than one
        // entity can be removed from 'interactiveEntities' during
        // events which could cause the index to be referencing
        // an undefined element.
        if (entity != null && entity.isEnabled)
        {
        
            if (this.mouseOverEntity(event, entity)) 
            {
                entity.events.isDown = true;
                entity.events.notify.call(entity.events, EventManager.Events.DOWN);
            }

        }
    }
    
    return false;
};

EventManager.prototype._checkMouseUpEvent = function(event)
{    
    var x = event.pageX - this.game.canvas.offsetLeft;
    var y = event.pageY - this.game.canvas.offsetTop;
    
    for (var i = this.interactiveEntities.length - 1; i >= 0; i--)
    {
        var entity = this.interactiveEntities[i];
        
        // Need to check if the entity exists because more than one
        // entity can be removed from 'interactiveEntities' during
        // events which could cause the index to be referencing
        // an undefined element.
        if (entity != null && entity.isEnabled)
        {
        
            if (this.mouseOverEntity(event, entity)) 
            {                
                
                if (entity.events.isDown)
                {
                    entity.events.notify.call(entity.events, EventManager.Events.UP);
                    entity.events.notify.call(entity.events, EventManager.Events.CLICK);
                }
                
                entity.events.isDown = false;
                
            }

        }
    }
    
    return false;
};

EventManager.prototype._checkMouseMoveEvent = function(event)
{     
    for (var i = this.interactiveEntities.length - 1; i >= 0; i--)
    {
        var entity = this.interactiveEntities[i];
        
        if (entity != null && entity.isEnabled)
        {
        
            if (this.mouseOverEntity(event, entity)) 
            {
                if (!entity.events.isOver) 
                {
                    entity.events.isOver = true;
                    entity.events.notify.call(entity.events, EventManager.Events.OVER);
                }
            }
            else
            {
                if (entity.events.isOver)
                {
                    entity.events.isOver = false;
                    entity.events.notify.call(entity.events, EventManager.Events.OUT);
                }
                
                if (entity.events.isDown)
                {
                    entity.events.isDown = false;
                    entity.events.notify.call(entity.events, EventManager.Events.UP);
                }
            }

        }
    }
    
    return false;
};

EventManager.prototype.mouseOverEntity = function(mouseEvent, entity)
{
    var x = this.getScaledMouseX(mouseEvent.pageX - this.game.canvas.offsetLeft);
    var y = this.getScaledMouseY(mouseEvent.pageY - this.game.canvas.offsetTop);
    
    return this.pointOverEntity(x, y, entity);
};

EventManager.prototype.pointOverEntity = function(x, y, entity)
{
    return x >= entity.transform.left &&
           x <= entity.transform.right &&
           y >= entity.transform.top && 
           y <= entity.transform.bottom;
};

EventManager.prototype.getScaledMouseX = function(mouseX)
{
    var widthScale = 1; 
    
    if (this.game.canvas.style.width != "")
    {
        var displayedWidth = this.game.canvas.style.width;
        displayedWidth = displayedWidth.substr(0, displayedWidth.length - 2);
        widthScale = displayedWidth / this.game.canvas.width;
    }

    return mouseX / widthScale;
};

EventManager.prototype.getScaledMouseY = function(mouseY)
{
    var heightScale = 1;
    
    if (this.game.canvas.style.height != "")
    {
        var displayedHeight = this.game.canvas.style.height;
        displayedHeight = displayedHeight.substr(0, displayedHeight.length - 2);
        heightScale = displayedHeight / this.game.canvas.height;
    }
    
    return mouseY / heightScale;
};

EventManager.prototype.add = function(entity)
{
    var index = this.interactiveEntities.indexOf(entity);
    
    if (index == -1)
    {
        this.interactiveEntities.push(entity);
    }
};

EventManager.prototype.remove = function(entity)
{
    var indexToRemove = this.interactiveEntities.indexOf(entity);

    if (indexToRemove != -1) 
    {
        this.interactiveEntities.splice(indexToRemove, 1);
    }
};

/*
@deprecated
*/
EventManager.prototype.addEvent = function(entity, event, eventHandler)
{
    entity.events[event].push(eventHandler);
    
    if (this.interactiveEntities.indexOf(entity) != -1)
    {
        this.interactiveEntities.push(entity);   
    }
};