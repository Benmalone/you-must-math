function LoadCredits(game, startMenu)
{
    this.game = game;
	
    this.startMenu = startMenu;
	
    this._setupCreditsImage();
    this._setupBackButton();
    
    this.close();
}
LoadCredits.prototype._setupCreditsImage = function()
{
    this.creditsPage = this.game.create.image(0, 0, 1280, 720, CREDITS_PAGE_SRC);
}

LoadCredits.prototype._setupBackButton = function()
{
    this._createAndPositionBackButton();
    this._setupBackButtonEvents();
};

LoadCredits.prototype._createAndPositionBackButton = function()
{
    this.backButton = this.game.create.image(0, 0, 200, 80, BACK_BUTTON_INVERT_SRC);
    this.backButton.transform.pivotX = 0.5;
    this.backButton.transform.pivotY = 0.5;
    this.backButton.transform.centerX = this.game.width / 2.01;
    this.backButton.transform.bottom = this.game.height - 10;
    this.backButton.transform.z = -1;
};

LoadCredits.prototype._setupBackButtonEvents = function()
{
    var self = this;
    
    this.backButton.events.over.push(function()
    {
        self.backButton.transform.scaleX = 1.1;
        self.backButton.transform.scaleY = 1.1;
    });
    
    this.backButton.events.out.push(function()
    {
        self.backButton.transform.scaleX = 1;
        self.backButton.transform.scaleY = 1;
    });
    
    this.backButton.events.down.push(function()
    {
        self.backButton.imageComponent.setImage(BACK_BUTTON_INVERT_SRC);
    });
    
    this.backButton.events.up.push(function()
    {
        self.backButton.imageComponent.setImage(BACK_BUTTON_SRC);
    });
    
    this.backButton.events.click.push(function()
    {
        self.close();
        self.startMenu.open();
    });
	window.addEventListener("keyup", function(e){    
		if((e.which == 27 || e.which == 13) && loadCredits.backButton.isEnabled)
		{
			self.close();
			self.startMenu.open();
		}
    });
};

LoadCredits.prototype.close = function()
{
	this.creditsPage.setActive(false);
    this.backButton.setActive(false);
};

LoadCredits.prototype.open = function()
{    
	
	this.creditsPage.setActive(true);
    this.backButton.setActive(true);
};