function CreateMenu(game, newGameMenu, navMenu) 
{
    this.game = game;
    
    this.newGameMenu = newGameMenu;
    this.navMenu = navMenu;
    
    this.saveIndex;
    this.difficulty = "Easy";
    this.operators = "Add/Sub";
    
    this.displayOverwriteWarning = true;
    
    this._setupCreateBox();
    this._setupInputField();
    this._setupDifficultyButtons();
    this._setupOperatorButtons();
    this._setupContinueButton();
    this._setupBackButton();
    this._setupCreationHints();
    this._setupKeys();
    
    this.close();
}

CreateMenu.prototype._setupCreateBox = function()
{
    this.createBox = this.game.create.image(0, 0, 464, 487, CHARACTER_CREATE_BOX_SRC);
    this.createBox.transform.centerX = this.game.width / 2;
    this.createBox.transform.centerY = (this.game.height / 2) - 75;
    this.createBox.transform.z = -10;
};

CreateMenu.prototype._setupInputField = function()
{
    this._createAndPositionNameField();
    this._setupInputElement();
    this._setupKeyEvents();
    this._setupFocusEvents();
};

CreateMenu.prototype._createAndPositionNameField = function()
{
    this.nameField = this.game.create.inputField(0,0,0,0);
    this.nameField.textComponent.isCursor = false;
    this.nameField.transform.top = this.createBox.transform.top + 68;
    this.nameField.transform.left = this.createBox.transform.left + 50;
    this.nameField.transform.z = -11;
};

CreateMenu.prototype._setupKeyEvents = function()
{
    var self = this;
    
    this.nameInputElement.addEventListener("input", function(e)
    {
        if (self.nameField.isEnabled)
        {
            self.nameField.textComponent.text = e.target.value;
        }
	});
};

CreateMenu.prototype._setupFocusEvents = function()
{
    var self = this;
    
    this.nameInputElement.addEventListener("focus", function(e)
    {
        self.nameField.textComponent.isCursor = true;
	});
    
    this.nameInputElement.addEventListener("blur", function(e)
    {
        self.nameField.textComponent.isCursor = false;
	});
};

CreateMenu.prototype._setupInputElement = function()
{
    this.nameInputElement = document.createElement("input");
    this.nameInputElement.type = "text";
    this.nameInputElement.setAttribute("maxlength", "12");
    this.nameInputElement.style.cssText = "position: absolute; opacity: 0";
    document.body.appendChild(this.nameInputElement);
    
    this.positionNameElement();
};

CreateMenu.prototype.positionNameElement = function()
{
    var canvasLeft = this.game.canvas.offsetLeft;;
    var canvasTop = this.game.canvas.style.top.substr(0, this.game.canvas.style.top.length - 2);
    
    var x = (this.nameField.transform.left * this.game.scaleX) + this.game.canvas.offsetLeft;
    var y = (this.nameField.transform.top * this.game.scaleY) + canvasTop;
    this.nameInputElement.style.left = x + "px";
    this.nameInputElement.style.top = y + "px";
    this.nameInputElement.style.width = (355 * this.game.scaleX) + "px";
    this.nameInputElement.style.height = (50 * this.game.scaleY) + "px";
};

CreateMenu.prototype._setupDifficultyButtons = function()
{
    this._setupEasyButton();
    this._setupNormalButton();
    this._setupHardButton();
};

CreateMenu.prototype._setupEasyButton = function()
{
    this.easyButton = this.game.create.image(0, 0, 72, 72, EASY_BUTTON_INVERT_SRC);
    this.easyButton.transform.pivotX = 0.5;
    this.easyButton.transform.pivotY = 0.5;
    this.easyButton.transform.centerX = this.createBox.transform.centerX - this.easyButton.transform.width * 1.5;
    this.easyButton.transform.centerY = this.createBox.transform.centerY - this.easyButton.transform.height / 2.5;
    this.easyButton.transform.z = this.createBox.transform.z - 1;
    
    var self = this;
    
    this.easyButton.events.over.push(function()
    {
        self.easyButton.transform.scaleX = 1.1;
        self.easyButton.transform.scaleY = 1.1;
    });
    
    this.easyButton.events.out.push(function()
    {
        self.easyButton.transform.scaleX = 1;
        self.easyButton.transform.scaleY = 1;
    });
    
    this.easyButton.events.click.push(function()
    {
        self.easyButton.imageComponent.setImage(EASY_BUTTON_SRC);
        self.normalButton.imageComponent.setImage(NORMAL_BUTTON_INVERT_SRC);
        self.hardButton.imageComponent.setImage(HARD_BUTTON_INVERT_SRC);
        self.difficulty = "Easy";
    });
};

CreateMenu.prototype._setupNormalButton = function()
{
    this.normalButton = this.game.create.image(0, 0, 72, 72, NORMAL_BUTTON_SRC);
    this.normalButton.transform.pivotX = 0.5;
    this.normalButton.transform.pivotY = 0.5;
    this.normalButton.transform.centerX = this.createBox.transform.centerX;
    this.normalButton.transform.centerY = this.createBox.transform.centerY - this.normalButton.transform.height / 2.5;
    this.normalButton.transform.z = this.createBox.transform.z - 1;
    
    var self = this;
    
    this.normalButton.events.over.push(function()
    {
        self.normalButton.transform.scaleX = 1.1;
        self.normalButton.transform.scaleY = 1.1;
    });
    
    this.normalButton.events.out.push(function()
    {
        self.normalButton.transform.scaleX = 1;
        self.normalButton.transform.scaleY = 1;
    });
    
    this.normalButton.events.click.push(function()
    {
        self.easyButton.imageComponent.setImage(EASY_BUTTON_INVERT_SRC);
        self.normalButton.imageComponent.setImage(NORMAL_BUTTON_SRC);
        self.hardButton.imageComponent.setImage(HARD_BUTTON_INVERT_SRC);
        self.difficulty = "Normal";
    });
};

CreateMenu.prototype._setupHardButton = function()
{
    this.hardButton = this.game.create.image(0, 0, 72, 72, HARD_BUTTON_INVERT_SRC);
    this.hardButton.transform.pivotX = 0.5;
    this.hardButton.transform.pivotY = 0.5;
    this.hardButton.transform.centerX = this.createBox.transform.centerX + this.hardButton.transform.width * 1.5;
    this.hardButton.transform.centerY = this.createBox.transform.centerY - this.hardButton.transform.height / 2.5;
    this.hardButton.transform.z = this.createBox.transform.z - 1;
    
    var self = this;
    
    this.hardButton.events.over.push(function()
    {
        self.hardButton.transform.scaleX = 1.1;
        self.hardButton.transform.scaleY = 1.1;
    });
    
    this.hardButton.events.out.push(function()
    {
        self.hardButton.transform.scaleX = 1;
        self.hardButton.transform.scaleY = 1;
    });
    
    this.hardButton.events.click.push(function()
    {
        self.easyButton.imageComponent.setImage(EASY_BUTTON_INVERT_SRC);
        self.normalButton.imageComponent.setImage(NORMAL_BUTTON_INVERT_SRC);
        self.hardButton.imageComponent.setImage(HARD_BUTTON_SRC);
        self.difficulty = "Hard";
    });
};

CreateMenu.prototype._setupOperatorButtons = function()
{
    this._setupAddAndSubtractButton();
    this._setupMultiplyAndDivideButton();
    this._setupAllOperatorsButton();
};

CreateMenu.prototype._setupAddAndSubtractButton = function()
{
    this.addAndSubtractButton = this.game.create.image(0, 0, 72, 72, ADD_SUB_BUTTON_SRC);
    this.addAndSubtractButton.transform.pivotX = 0.5;
    this.addAndSubtractButton.transform.pivotY = 0.5;
    this.addAndSubtractButton.transform.centerX = this.createBox.transform.centerX - this.addAndSubtractButton.transform.width * 1.5;
    this.addAndSubtractButton.transform.centerY = this.createBox.transform.centerY + this.addAndSubtractButton.transform.height / 0.75;
    this.addAndSubtractButton.transform.z = this.createBox.transform.z - 1;
    
    var self = this;
    
    this.addAndSubtractButton.events.over.push(function()
    {
        self.addAndSubtractButton.transform.scaleX = 1.1;
        self.addAndSubtractButton.transform.scaleY = 1.1;
    });
    
    this.addAndSubtractButton.events.out.push(function()
    {
        self.addAndSubtractButton.transform.scaleX = 1;
        self.addAndSubtractButton.transform.scaleY = 1;
    });
    
    this.addAndSubtractButton.events.click.push(function()
    {
        self.addAndSubtractButton.imageComponent.setImage(ADD_SUB_BUTTON_SRC);
        self.multiplyAndDivideButton.imageComponent.setImage(MULT_DIV_BUTTON_INVERT_SRC);
        self.allOperatorsButton.imageComponent.setImage(ALL_OPERATORS_BUTTON_INVERT_SRC);
        self.operators = "Add/Sub";
    });
};

CreateMenu.prototype._setupMultiplyAndDivideButton = function()
{
    this.multiplyAndDivideButton = this.game.create.image(0, 0, 72, 72, MULT_DIV_BUTTON_INVERT_SRC);
    this.multiplyAndDivideButton.transform.pivotX = 0.5;
    this.multiplyAndDivideButton.transform.pivotY = 0.5;
    this.multiplyAndDivideButton.transform.centerX = this.createBox.transform.centerX;
    this.multiplyAndDivideButton.transform.centerY = this.createBox.transform.centerY + this.multiplyAndDivideButton.transform.height / 0.75;
    this.multiplyAndDivideButton.transform.z = this.createBox.transform.z - 1;
    
    var self = this;
    
    this.multiplyAndDivideButton.events.over.push(function()
    {
        self.multiplyAndDivideButton.transform.scaleX = 1.1;
        self.multiplyAndDivideButton.transform.scaleY = 1.1;
    });
    
    this.multiplyAndDivideButton.events.out.push(function()
    {
        self.multiplyAndDivideButton.transform.scaleX = 1;
        self.multiplyAndDivideButton.transform.scaleY = 1;
    });
    
    this.multiplyAndDivideButton.events.click.push(function()
    {
        self.addAndSubtractButton.imageComponent.setImage(ADD_SUB_BUTTON_INVERT_SRC);
        self.multiplyAndDivideButton.imageComponent.setImage(MULT_DIV_BUTTON_SRC);
        self.allOperatorsButton.imageComponent.setImage(ALL_OPERATORS_BUTTON_INVERT_SRC);
        self.operators = "Multi/Div";
    });
};

CreateMenu.prototype._setupAllOperatorsButton = function()
{
    this.allOperatorsButton = this.game.create.image(0, 0, 72, 72, ALL_OPERATORS_BUTTON_INVERT_SRC);
    this.allOperatorsButton.transform.pivotX = 0.5;
    this.allOperatorsButton.transform.pivotY = 0.5;
    this.allOperatorsButton.transform.centerX = this.createBox.transform.centerX + this.allOperatorsButton.transform.width * 1.5;
    this.allOperatorsButton.transform.centerY = this.createBox.transform.centerY + this.allOperatorsButton.transform.height / 0.75;
    this.allOperatorsButton.transform.z = this.createBox.transform.z - 1;
    
    var self = this;
    
    this.allOperatorsButton.events.over.push(function()
    {
        self.allOperatorsButton.transform.scaleX = 1.1;
        self.allOperatorsButton.transform.scaleY = 1.1;
    });
    
    this.allOperatorsButton.events.out.push(function()
    {
        self.allOperatorsButton.transform.scaleX = 1;
        self.allOperatorsButton.transform.scaleY = 1;
    });
    
    this.allOperatorsButton.events.click.push(function()
    {
        self.addAndSubtractButton.imageComponent.setImage(ADD_SUB_BUTTON_INVERT_SRC);
        self.multiplyAndDivideButton.imageComponent.setImage(MULT_DIV_BUTTON_INVERT_SRC);
        self.allOperatorsButton.imageComponent.setImage(ALL_OPERATORS_BUTTON_SRC);
        self.operators = "All";
    });
};
CreateMenu.prototype._setupKeys = function()
{
	var self = this;
	var indexSlot = 0;
	window.addEventListener("keydown", function(e){    
		//Using Up Arrow and W
		if(e.which == 38 && createMenu.backButton.isEnabled)
		{
			if(indexSlot == 0)
			{
				indexSlot = 7;
				createMenu.easyButton.imageComponent.setImage(EASY_BUTTON_SRC);
				createMenu.backButton.imageComponent.setImage(BACK_BUTTON_INVERT_SRC);
			}
			else if(indexSlot == 1)
			{
				indexSlot = 7;
				createMenu.normalButton.imageComponent.setImage(NORMAL_BUTTON_SRC);
				createMenu.backButton.imageComponent.setImage(BACK_BUTTON_INVERT_SRC);
			}
			else if(indexSlot == 2)
			{
				indexSlot = 7;
				createMenu.hardButton.imageComponent.setImage(HARD_BUTTON_SRC);
				createMenu.backButton.imageComponent.setImage(BACK_BUTTON_INVERT_SRC);
			}
			else if(indexSlot == 3)
			{
				indexSlot = indexSlot - 3;
				createMenu.addAndSubtractButton.imageComponent.setImage(ADD_SUB_BUTTON_SRC);
				createMenu.easyButton.imageComponent.setImage(EASY_BUTTON_SELECTED_SRC);
				createMenu.normalButton.imageComponent.setImage(NORMAL_BUTTON_INVERT_SRC);
				createMenu.hardButton.imageComponent.setImage(HARD_BUTTON_INVERT_SRC);
				createMenu.difficulty = "Easy";
			}			
			else if(indexSlot == 4)
			{
				indexSlot = indexSlot - 3;
				createMenu.multiplyAndDivideButton.imageComponent.setImage(MULT_DIV_BUTTON_SRC);
				createMenu.easyButton.imageComponent.setImage(EASY_BUTTON_INVERT_SRC);
				createMenu.normalButton.imageComponent.setImage(NORMAL_BUTTON_SELECTED_SRC);
				createMenu.hardButton.imageComponent.setImage(HARD_BUTTON_INVERT_SRC);
				createMenu.difficulty = "Normal";
			}	
			else if(indexSlot == 5)
			{
				indexSlot = indexSlot - 3;
				createMenu.allOperatorsButton.imageComponent.setImage(ALL_OPERATORS_BUTTON_SRC);
				createMenu.easyButton.imageComponent.setImage(EASY_BUTTON_INVERT_SRC);
				createMenu.normalButton.imageComponent.setImage(NORMAL_BUTTON_INVERT_SRC);
				createMenu.hardButton.imageComponent.setImage(HARD_BUTTON_SELECTED_SRC);
				createMenu.difficulty = "Hard";
			}	
			else if(indexSlot == 6)
			{
				indexSlot = 3;
				createMenu.continueButton.imageComponent.setImage(CONTINUE_BUTTON_SRC);
				createMenu.addAndSubtractButton.imageComponent.setImage(ADD_SUB_BUTTON_SELECTED_SRC);
				createMenu.multiplyAndDivideButton.imageComponent.setImage(MULT_DIV_BUTTON_INVERT_SRC);
				createMenu.allOperatorsButton.imageComponent.setImage(ALL_OPERATORS_BUTTON_INVERT_SRC);
				createMenu.difficulty = "Add/Sub";
			}	
			else if(indexSlot == 7)
			{
				indexSlot = 6;
				createMenu.backButton.imageComponent.setImage(BACK_BUTTON_SRC);
				createMenu.continueButton.imageComponent.setImage(CONTINUE_BUTTON_INVERT_SRC);
			}
			
		}
		//Down Arrow and S
		if(e.which == 40 && createMenu.backButton.isEnabled)
		{
			if(indexSlot == 0)
			{
				indexSlot = indexSlot + 3;
				createMenu.easyButton.imageComponent.setImage(EASY_BUTTON_SRC);
				createMenu.addAndSubtractButton.imageComponent.setImage(ADD_SUB_BUTTON_SELECTED_SRC);
				createMenu.multiplyAndDivideButton.imageComponent.setImage(MULT_DIV_BUTTON_INVERT_SRC);
				createMenu.allOperatorsButton.imageComponent.setImage(ALL_OPERATORS_BUTTON_INVERT_SRC);
				createMenu.operators = "Add/Sub";

			}			
			else if(indexSlot == 1)
			{
				indexSlot = indexSlot + 3;				
				createMenu.normalButton.imageComponent.setImage(NORMAL_BUTTON_SRC);
				createMenu.addAndSubtractButton.imageComponent.setImage(ADD_SUB_BUTTON_INVERT_SRC);
				createMenu.multiplyAndDivideButton.imageComponent.setImage(MULT_DIV_BUTTON_SELECTED_SRC);
				createMenu.allOperatorsButton.imageComponent.setImage(ALL_OPERATORS_BUTTON_INVERT_SRC);
				createMenu.operators = "Multi/Div";
			}			
			else if(indexSlot == 2)
			{
				indexSlot = indexSlot + 3;
				createMenu.hardButton.imageComponent.setImage(HARD_BUTTON_SRC);
				createMenu.addAndSubtractButton.imageComponent.setImage(ADD_SUB_BUTTON_INVERT_SRC);
				createMenu.multiplyAndDivideButton.imageComponent.setImage(MULT_DIV_BUTTON_INVERT_SRC);
				createMenu.allOperatorsButton.imageComponent.setImage(ALL_OPERATORS_BUTTON_SELECTED_SRC);
				createMenu.operators = "All";
			}			
			else if(indexSlot == 3)
			{
				indexSlot = 6;
				createMenu.continueButton.imageComponent.setImage(CONTINUE_BUTTON_INVERT_SRC);
				createMenu.addAndSubtractButton.imageComponent.setImage(ADD_SUB_BUTTON_SRC);
			}			
			else if(indexSlot == 4)
			{
				indexSlot = 6;
				createMenu.continueButton.imageComponent.setImage(CONTINUE_BUTTON_INVERT_SRC);
				createMenu.multiplyAndDivideButton.imageComponent.setImage(MULT_DIV_BUTTON_SRC);
			}	
			else if(indexSlot == 5)
			{
				indexSlot = 6;
				createMenu.allOperatorsButton.imageComponent.setImage(ALL_OPERATORS_BUTTON_SRC);
				createMenu.continueButton.imageComponent.setImage(CONTINUE_BUTTON_INVERT_SRC);
			}			
			else if(indexSlot == 6)
			{
				indexSlot = 7;
				createMenu.continueButton.imageComponent.setImage(CONTINUE_BUTTON_SRC);
				createMenu.backButton.imageComponent.setImage(BACK_BUTTON_INVERT_SRC);
			}	
			else if(indexSlot == 7)
			{
				indexSlot = 0;
				createMenu.backButton.imageComponent.setImage(BACK_BUTTON_SRC);
				createMenu.easyButton.imageComponent.setImage(EASY_BUTTON_SELECTED_SRC);
				createMenu.normalButton.imageComponent.setImage(NORMAL_BUTTON_INVERT_SRC);
				createMenu.hardButton.imageComponent.setImage(HARD_BUTTON_INVERT_SRC);
				createMenu.difficulty = "Easy";
			}
		}			
	//Using Right Arrow and D
		if(e.which == 39 && createMenu.backButton.isEnabled)
		{		
			if(indexSlot == 0)
			{				
				indexSlot++;
				createMenu.easyButton.imageComponent.setImage(EASY_BUTTON_INVERT_SRC);
				createMenu.normalButton.imageComponent.setImage(NORMAL_BUTTON_SELECTED_SRC);	
				createMenu.difficulty = "Normal";			
			}			
			else if(indexSlot == 1)
			{			
				indexSlot++;
				createMenu.normalButton.imageComponent.setImage(NORMAL_BUTTON_INVERT_SRC);
				createMenu.hardButton.imageComponent.setImage(HARD_BUTTON_SELECTED_SRC);
				createMenu.difficulty = "Hard";
			}		
			else if(indexSlot == 2)
			{		
				indexSlot = indexSlot - 2;
				createMenu.hardButton.imageComponent.setImage(HARD_BUTTON_INVERT_SRC);
				createMenu.easyButton.imageComponent.setImage(EASY_BUTTON_SELECTED_SRC);
				createMenu.difficulty = "Easy";
			}	
			else if(indexSlot == 3)
			{
				indexSlot++;
				createMenu.addAndSubtractButton.imageComponent.setImage(ADD_SUB_BUTTON_INVERT_SRC);
				createMenu.multiplyAndDivideButton.imageComponent.setImage(MULT_DIV_BUTTON_SELECTED_SRC);
				createMenu.operators = "Multi/Div";
			}			
			else if(indexSlot == 4)
			{
				indexSlot++;
				createMenu.multiplyAndDivideButton.imageComponent.setImage(MULT_DIV_BUTTON_INVERT_SRC);
				createMenu.allOperatorsButton.imageComponent.setImage(ALL_OPERATORS_BUTTON_SELECTED_SRC);
				createMenu.operators = "All";
			}	
			else if(indexSlot == 5)
			{
				indexSlot = indexSlot - 2;
				createMenu.allOperatorsButton.imageComponent.setImage(ALL_OPERATORS_BUTTON_INVERT_SRC);
				createMenu.addAndSubtractButton.imageComponent.setImage(ADD_SUB_BUTTON_SELECTED_SRC);
				createMenu.operators = "Add/Sub";
			}
		}		
	//Using Left Arrow and A	
		if(e.which == 37 && createMenu.backButton.isEnabled)
		{			
			if(indexSlot == 0)
			{				
				indexSlot = indexSlot + 2;
				createMenu.easyButton.imageComponent.setImage(EASY_BUTTON_INVERT_SRC);
				createMenu.hardButton.imageComponent.setImage(HARD_BUTTON_SELECTED_SRC);
				createMenu.difficulty = "Hard";				
			}			
			else if(indexSlot == 1)
			{			
				indexSlot--;
				createMenu.normalButton.imageComponent.setImage(NORMAL_BUTTON_INVERT_SRC);
				createMenu.easyButton.imageComponent.setImage(EASY_BUTTON_SELECTED_SRC);
				createMenu.difficulty = "Easy";
			}		
			else if(indexSlot == 2)
			{		
				indexSlot--;
				createMenu.hardButton.imageComponent.setImage(HARD_BUTTON_INVERT_SRC);
				createMenu.normalButton.imageComponent.setImage(NORMAL_BUTTON_SELECTED_SRC);
				createMenu.difficulty = "Normal";
			}	
			else if(indexSlot == 3)
			{
				indexSlot = indexSlot + 2;
				createMenu.addAndSubtractButton.imageComponent.setImage(ADD_SUB_BUTTON_INVERT_SRC);
				createMenu.allOperatorsButton.imageComponent.setImage(ALL_OPERATORS_BUTTON_SELECTED_SRC);
				createMenu.operators = "All";
			}			
			else if(indexSlot == 4)
			{
				indexSlot--;
				createMenu.multiplyAndDivideButton.imageComponent.setImage(MULT_DIV_BUTTON_INVERT_SRC);
				createMenu.addAndSubtractButton.imageComponent.setImage(ADD_SUB_BUTTON_SELECTED_SRC);
				createMenu.operators = "Add/Sub";
			}	
			else if(indexSlot == 5)
			{
				indexSlot--;
				createMenu.allOperatorsButton.imageComponent.setImage(ALL_OPERATORS_BUTTON_INVERT_SRC);
				createMenu.multiplyAndDivideButton.imageComponent.setImage(MULT_DIV_BUTTON_SELECTED_SRC);
				createMenu.operators = "Multi/Div";
			}
		}
		if(e.which == 13 && createMenu.backButton.isEnabled)
		{
			if(indexSlot == 6)
			{
				if (self.nameField.textComponent.text == "")
				{										
					self.hint.textScrollComponent.create("Remember to fill in your character's name.", 1, 0, null);
					
					if (!SaveData.isSlotEmpty(self.saveIndex))
					{
						this.displayOverwriteWarning = true;
					}
					else
					{
						this.displayOverwriteWarning = false;
					}
				}
				else if (self.displayOverwriteWarning)
				{								
					self.hint.textScrollComponent.create("Clicking continue will overwrite old save.", 1, 0, function()
					{
						self.displayOverwriteWarning = false;
					});
				}
				else
				{
					indexSlot = 0;
					createMenu.continueButton.imageComponent.setImage(CONTINUE_BUTTON_SRC);
					player = new Player(self.game, self.nameField.textComponent.text, self.difficulty, self.operators, 1, 0, 0, "None", "none", self.saveIndex);
					player.save();
                    
                    background.setActive(false);
                    self.close();
					self.navMenu.open();
				}			
			}
		}
	});
	window.addEventListener("keyup", function(e){
		if(e.which == 13 && createMenu.backButton.isEnabled)
		{
			if(indexSlot == 7)
			{
				{
					indexSlot = 0;
					createMenu.backButton.imageComponent.setImage(BACK_BUTTON_SRC);
					self.close();
					self.newGameMenu.open();
				}
			}
		}
	});
};

CreateMenu.prototype._setupContinueButton = function()
{
    this._createAndPositionContinueButton();
    this._setupContinueButtonEvents();
};

CreateMenu.prototype._createAndPositionContinueButton = function()
{
    this.continueButton = this.game.create.image(0, 0, 264, 80, CONTINUE_BUTTON_SRC);
    this.continueButton.transform.pivotX = 0.5;
    this.continueButton.transform.pivotY = 0.5;
    this.continueButton.transform.centerX = this.createBox.transform.centerX;
    this.continueButton.transform.top = this.createBox.transform.bottom - this.continueButton.transform.height - 25;
    this.continueButton.transform.z = this.createBox.transform.z - 1;
};

CreateMenu.prototype._setupContinueButtonEvents = function()
{
    var self = this;
    
    this.continueButton.events.over.push(function()
    {
        self.continueButton.transform.scaleX = 1.1;
        self.continueButton.transform.scaleY = 1.1;
    });
    
    this.continueButton.events.out.push(function()
    {
        self.continueButton.transform.scaleX = 1;
        self.continueButton.transform.scaleY = 1;
    });
    
    this.continueButton.events.down.push(function()
    {
        self.continueButton.imageComponent.setImage(CONTINUE_BUTTON_INVERT_SRC);
    });
    
    this.continueButton.events.up.push(function()
    {
        self.continueButton.imageComponent.setImage(CONTINUE_BUTTON_SRC);
    });
    
    this.continueButton.events.click.push(function()
    {
        if (self.nameField.textComponent.text == "")
        {										
            self.hint.textScrollComponent.create("Remember to fill in your character's name.", 1, 0, null);
            
            if (!SaveData.isSlotEmpty(self.saveIndex))
            {
                this.displayOverwriteWarning = true;
            }
            else
            {
                this.displayOverwriteWarning = false;
            }
        }
        else if (self.displayOverwriteWarning)
        {								
            self.hint.textScrollComponent.create("Clicking continue will overwrite old save.", 1, 0, function()
            {
                self.displayOverwriteWarning = false;
            });
        }
        else
        {
            player = new Player(self.game, self.nameField.textComponent.text, self.difficulty, self.operators, 1, 0, 0, "None", "none", self.saveIndex);
            player.save();
                    
            background.setActive(false);
            self.close();
            self.navMenu.open();
        }

    });
};

CreateMenu.prototype._setupBackButton = function()
{
    this._createAndPositionBackButton();
    this._setupBackButtonEvents();
};

CreateMenu.prototype._createAndPositionBackButton = function()
{
    this.backButton = this.game.create.image(0, 0, 200, 80, BACK_BUTTON_SRC);
    this.backButton.transform.pivotX = 0.5;
    this.backButton.transform.pivotY = 0.5;
    this.backButton.transform.centerX = this.game.width / 2;
    this.backButton.transform.bottom = this.game.height - 10;
    this.backButton.transform.z = -1;
};

CreateMenu.prototype._setupBackButtonEvents = function()
{
    var self = this;
    
    this.backButton.events.over.push(function()
    {
        self.backButton.transform.scaleX = 1.1;
        self.backButton.transform.scaleY = 1.1;
    });
    
    this.backButton.events.out.push(function()
    {
        self.backButton.transform.scaleX = 1;
        self.backButton.transform.scaleY = 1;
    });
    
    this.backButton.events.down.push(function()
    {
        self.backButton.imageComponent.setImage(BACK_BUTTON_INVERT_SRC);
    });
    
    this.backButton.events.up.push(function()
    {
        self.backButton.imageComponent.setImage(BACK_BUTTON_SRC);
    });
    
    this.backButton.events.click.push(function()
    {
        self.close();
        self.newGameMenu.open();
    });
    
	window.addEventListener("keydown", function(e){    
		if(e.which == 27 && createMenu.backButton.isEnabled)
		{
			self.close();
			self.newGameMenu.open();
		}
    });
};

CreateMenu.prototype._setupCreationHints = function()
{
    this.hint = this.game.create.hint(0, 0, "sans-serif", 42, "white");
    this.hint.transform.top = this.continueButton.transform.bottom + this.hint.textComponent.fontSize;
    this.hint.transform.left = this.createBox.transform.left - 150;
};

CreateMenu.prototype.close = function()
{
    this.createBox.setActive(false);
    this.easyButton.setActive(false);
    this.normalButton.setActive(false);
    this.hardButton.setActive(false);
    this.addAndSubtractButton.setActive(false);
    this.multiplyAndDivideButton.setActive(false);
    this.allOperatorsButton.setActive(false);
    this.continueButton.setActive(false);
    this.backButton.setActive(false);
    this.nameField.setActive(false);
    this.hint.setActive(false);
    this.hint.textComponent.text = "";
    
    this.nameInputElement.disabled = true;
    this.nameInputElement.style.zIndex = -1000;
    this.nameInputElement.value = "";
    this.nameField.textComponent.text = "";
};

CreateMenu.prototype.open = function(saveIndex)
{    
    this.saveIndex = saveIndex;
    
    if (!SaveData.isSlotEmpty(saveIndex)) {
        this.displayOverwriteWarning = true;
    }
    else
    {
        this.displayOverwriteWarning = false;
    }
    
    this.createBox.setActive(true);
    this.easyButton.setActive(true);
    this.normalButton.setActive(true);
    this.hardButton.setActive(true);
    this.addAndSubtractButton.setActive(true);
    this.multiplyAndDivideButton.setActive(true);
    this.allOperatorsButton.setActive(true);
    this.continueButton.setActive(true);
    this.backButton.setActive(true);
    this.nameField.setActive(true);
    this.hint.setActive(true);
    this.nameInputElement.disabled = false;
    this.nameInputElement.style.zIndex = 1000;
    
    this.easyButton.events.click[0]();
    this.addAndSubtractButton.events.click[0]();
};