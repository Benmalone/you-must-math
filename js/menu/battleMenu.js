var bossBattle, specialAttack, add, sub, mult, div, attackChoice, lastAttack;
var health;
var enemiesDefeated = 0;

function BattleMenu(game, player) 
{
    this.game = game;
    this.player = player;
    
    this.state = BattleMenu.States.DEFEND;
    
    this.indexAttack = 0;
    
    this._createPanels();
    this._createButtons();
    this._createText();
    this._createAttackEffects();
	this._createKeyPad();
    this._createHealthBars();
    this._createMenu();
    this._createPotion();
}

BattleMenu.States = 
{
    DEFEND: 0,
    ATTACK: 1
};

BattleMenu.prototype._createPanels = function()
{	
    this.wordPanel = this.game.create.image(0, this.game.height - 180, 960, 180, WORD_PANEL_SRC);
    this.wordPanel.transform.z = 10
    
    this.rightPanel = this.game.create.image(this.game.width - 320, this.game.height - 330, 320, 330, BOTTOM_RIGHT_PANEL_SRC);
    this.rightPanel.transform.z = 10
    
	this.keyPanel = this.game.create.image(this.game.width - 320, 0, 320, 390, MAP_SRC);
    this.keyPanel.transform.z = 10
	
	this.button0 = this.game.create.image(this.game.width - 195, this.game.height - 460, 70, 70, ZERO_BUTTON_SRC);
    this.button0.transform.z = 9
	this.button1 = this.game.create.image(this.game.width - 265, this.game.height - 670, 70, 70, ONE_BUTTON_SRC);
    this.button1.transform.z = 9
	this.button2 = this.game.create.image(this.game.width - 195, this.game.height - 670, 70, 70, TWO_BUTTON_SRC);
    this.button2.transform.z = 9
	this.button3 = this.game.create.image(this.game.width - 125, this.game.height - 670, 70, 70, THREE_BUTTON_SRC);
    this.button3.transform.z = 9
	this.button4 = this.game.create.image(this.game.width - 265, this.game.height - 600, 70, 70, FOUR_BUTTON_SRC);
    this.button4.transform.z = 9
	this.button5 = this.game.create.image(this.game.width - 195, this.game.height - 600, 70, 70, FIVE_BUTTON_SRC);
    this.button5.transform.z = 9
	this.button6 = this.game.create.image(this.game.width - 125, this.game.height - 600, 70, 70, SIX_BUTTON_SRC);
    this.button6.transform.z = 9
	this.button7 = this.game.create.image(this.game.width - 265, this.game.height - 530, 70, 70, SEVEN_BUTTON_SRC);
    this.button7.transform.z = 9
	this.button8 = this.game.create.image(this.game.width - 195, this.game.height - 530, 70, 70, EIGHT_BUTTON_SRC);
    this.button8.transform.z = 9
	this.button9 = this.game.create.image(this.game.width - 125, this.game.height - 530, 70, 70, NINE_BUTTON_SRC);
    this.button9.transform.z = 9
	this.buttonCL = this.game.create.image(this.game.width - 265, this.game.height - 460, 70, 70, CLEAR_BUTTON_SRC);
    this.buttonCL.transform.z = 9
	this.buttonENT = this.game.create.image(this.game.width - 125, this.game.height - 460, 70, 70, ENTER_BUTTON_SRC);
    this.buttonENT.transform.z = 9  
    
	this.itemPanel = this.game.create.image(0, 0, 175, 175, ITEM_BUTTON_SRC);
	this.itemPanel.transform.scaleX = .7;
	this.itemPanel.transform.scaleY = .7;
    this.itemPanel.transform.centerX = this.rightPanel.transform.centerX;
    this.itemPanel.transform.centerY = this.rightPanel.transform.centerY - 15;
};

BattleMenu.prototype._createButtons = function()
{
    this._createBattleButtons();
    this._setupBattleButtons();
};

BattleMenu.prototype._createText = function()
{
    this.hint = this.game.create.hint(16, this.game.height - 150, "sans-serif", 42, "white");
    
    this.mathProblem = this.game.create.label(this.game.width / 4, this.game.height / 4, "", "sans-serif", 42, "white");
	this.mathProblem.textComponent.outlineColor = "black";
    this.mathProblem.transform.z = -20;
    this.mathProblem.setActive(false);
    
    this.mathOutline = this.game.create.label(this.game.width / 4, this.game.height - 115, "", "sans-serif", 42, "none");
    this.mathOutline.textComponent.outlineColor = "white";
    this.mathOutline.transform.z = -20;
    this.mathOutline.setActive(false);
    
    this.answerField = this.game.create.inputField(this.mathOutline.transform.x + 150, this.mathOutline.transform.y, 60, 40);
    this.answerField.transform.z = -20;
    this.answerField.setActive(false);
};

BattleMenu.prototype._createHealthBars = function()
{
    this.healthBar = this.game.create.image(this.game.width - 280, this.game.height - 290, 248, 44, HEARTS_SRC);
	this.damageBar = this.game.create.image(this.game.width - 280, this.game.height - 290, 248, 44, HEARTS_DAMAGED_SRC);
    this.damageBar.transform.z = 9
    
    this.monsterHealthBar = this.game.create.image(30, 20, 248, 44, HEARTS_SRC);
    this.monsterDamageBar = this.game.create.image(30, 20, 248, 44, HEARTS_DAMAGED_SRC);
    this.monsterDamageBar.transform.z = 9;
};

BattleMenu.prototype._createPotion = function()
{
    var self = this;
    
    this.potion = this.game.create.sprite(0, 0, 100, 132, RED_POTION_SRC);
    this.potion.transform.z = -10;
    this.potion.transform.scaleX = .7;
    this.potion.transform.scaleY = .7;
    this.potion.transform.centerX = this.itemPanel.transform.centerX;
    this.potion.transform.centerY = this.itemPanel.transform.centerY;

    this.potion.events.click.push(function()
    {
        if (self.player.item.type == "health" && self.player.health < Player.MAX_HEALTH)
        {
            var currentHealth = self.player.health;
            
            self.player.useItem();
            sndPotion.play();
            self.potion.setActive(false);
            
            self.healthBar.tweenComponent.clip(currentHealth, self.player.health, 15);
        }
        else if (self.player.item.type != "health")
        {
            self.player.useItem();
            sndPotion.play();
            self.potion.setActive(false);
        }
    });
    
	window.addEventListener("keydown", function(e){    
		if(e.which == 80 && battleMenu.potion.isEnabled)
		{
			if (self.player.item.type == "health" && self.player.health < Player.MAX_HEALTH)
			{
				var currentHealth = self.player.health;
				
				self.player.useItem();
				sndPotion.play();
				self.potion.setActive(false);
				
				self.healthBar.tweenComponent.clip(currentHealth, self.player.health, 15);
			}
			else if (self.player.item.type != "health")
			{
				self.player.useItem();
				sndPotion.play();
				self.potion.setActive(false);
			}
		}
    });
};

BattleMenu.prototype._displayPotion = function()
{
    if (player.item == null)
    {
        this.potion.setActive(false);
    }
    else
    {
        this.potion.imageComponent.setImage(player.item.src);
        this.potion.setActive(true);
    }
};

BattleMenu.prototype._displayEnemy = function()
{	
	this.enemy.sprite.setActive(true);
    
    this.monsterHealthBar.imageRenderer.setMask(this.enemy.health);
    this.monsterDamageBar.imageRenderer.setMask(this.enemy.health);
	
	if (this.enemy.attack == "Bite")
	{
		this.enemyEffectToPlay = this.attackBiteEffect;
	}
	else if (this.enemy.attack == "Slash")
	{
		this.enemyEffectToPlay = this.attackSlashEffect;
	}
	else if (this.enemy.attack == "Punch")
	{
		this.enemyEffectToPlay = this.attackPunchEffect;
	}
	else if (this.enemy.attack == "Fireball")
	{
		this.enemyEffectToPlay = this.attackFireballEffect;
	}	
};

BattleMenu.prototype._displayPlayer = function()
{
    this.healthBar.imageRenderer.setMask(this.player.health);
    this.damageBar.imageRenderer.setMask(Player.MAX_HEALTH);
	
	health = this.player.health;
}

BattleMenu.prototype._createAttackEffects = function()
{
    this.addAttack = this._setupAttackEffects(ADD_ATTACK_SRC);
    this.subAttack = this._setupAttackEffects(SUB_ATTACK_SRC);
    this.multiAttack = this._setupAttackEffects(MULTIPLY_ATTACK_SRC);
    this.divideAttack = this._setupAttackEffects(DIVIDE_ATTACK_SRC);
    
    this._setupEnemyAttackEffects();
};

BattleMenu.prototype._setupAttackEffects = function(src)
{
    var attackEffect = this.game.create.sprite(0, 0, 128, 128, src);
    attackEffect.animations.add("attack", [0, 1, 2, 3, 4], 120, true, function()
    {
        attackEffect.animations.stop();
        attackEffect.setActive(false);
    });
    attackEffect.transform.pivotX = 0.5;
    attackEffect.transform.pivotY = 0.5;
    attackEffect.transform.scaleX = 4;
    attackEffect.transform.scaleY = 4;
    attackEffect.setActive(false);
    
    return attackEffect;
};

BattleMenu.prototype._setupEnemyAttackEffects = function()
{
    var self = this;
    
    this.attackBiteEffect = this.game.create.sprite(480, 270, 200, 200, ENEMY_BITE_SRC);
    this.attackSlashEffect = this.game.create.sprite(480, 270, 300, 300, ENEMY_SLASH_SRC);
	this.attackPunchEffect = this.game.create.sprite(480, 270, 300, 300, ENEMY_PUNCH_SRC);
	this.attackFireballEffect = this.game.create.sprite(480, 270, 300, 300, ENEMY_FIREBALL_SRC);
	
	this.attackBiteEffect.animations.add("attack", [0, 1, 2], 120, true, function()
    {
        self.attackBiteEffect.animations.stop();
        self.attackBiteEffect.setActive(false);
    });
    this.attackBiteEffect.transform.pivotX = 0.5;
    this.attackBiteEffect.transform.pivotY = 0.5;
    this.attackBiteEffect.transform.scaleX = 10;
    this.attackBiteEffect.transform.scaleY = 10;
    this.attackBiteEffect.setActive(false);
	
	this.attackSlashEffect.animations.add("attack", [0, 1, 2, 3], 120, true, function()
    {
        self.attackSlashEffect.animations.stop();
        self.attackSlashEffect.setActive(false);
    });
    this.attackSlashEffect.transform.pivotX = 0.5;
    this.attackSlashEffect.transform.pivotY = 0.5;
    this.attackSlashEffect.transform.scaleX = 6;
    this.attackSlashEffect.transform.scaleY = 6;
    this.attackSlashEffect.setActive(false);
	
	this.attackPunchEffect.animations.add("attack", [0, 1, 2, 3, 4], 120, true, function()
    {
        self.attackPunchEffect.animations.stop();
        self.attackPunchEffect.setActive(false);
    });
    this.attackPunchEffect.transform.pivotX = 0.5;
    this.attackPunchEffect.transform.pivotY = 0.5;
    this.attackPunchEffect.transform.scaleX = 6;
    this.attackPunchEffect.transform.scaleY = 6;
    this.attackPunchEffect.setActive(false);
	
	this.attackFireballEffect.animations.add("attack", [0, 1, 2, 3], 120, true, function()
    {
        self.attackFireballEffect.animations.stop();
        self.attackFireballEffect.setActive(false);
    });
    this.attackFireballEffect.transform.pivotX = 0.47;
    this.attackFireballEffect.transform.pivotY = 0.5;
    this.attackFireballEffect.transform.scaleX = 6;
    this.attackFireballEffect.transform.scaleY = 6;
    this.attackFireballEffect.setActive(false);
};

BattleMenu.prototype._start = function()
{
    var self = this;
	
    self.hint.textComponent.text = "";
    self.hint.setActive(true);
    
	soundNavigation.pause();
    if (soundNavigation.currentTime)
    {
        soundNavigation.currentTime = 0;
    }
    
	soundBattle.loop = true;
	soundBattle.play();
	
    this.hint.textScrollComponent.create(this.enemy.name + ": " + this.enemy.pun, 2, 150, function()
    {
        self.defendButton.setActive(true);
        self.hint.setActive(false);
        self.hint.textComponent.text = "";
    });
};

BattleMenu.prototype._createBattleButtons = function()
{
    this.defendButton = this.game.create.image(0, 0, 300, 130, DEFEND_BUTTON_SRC);
    this.defendButton.transform.x = (this.wordPanel.transform.width - this.defendButton.transform.width) / 2;
    this.defendButton.transform.y = this.wordPanel.transform.y + (this.wordPanel.transform.height / 8);
    this.defendButton.setActive(false);
    
    this.attackButton = this.game.create.image(0, 0, 300, 130, ATTACK_BUTTON_SRC);
    this.attackButton.transform.x = (this.wordPanel.transform.width - this.attackButton.transform.width) / 2;
    this.attackButton.transform.y = this.wordPanel.transform.y + (this.wordPanel.transform.height / 8);
    this.attackButton.setActive(false);
	
	this.addButton = this.game.create.image(0, 0, 110, 110, ADD_BUTTON_SRC);
    this.addButton.transform.x = (this.wordPanel.transform.width - this.addButton.transform.width) / 6;
    this.addButton.transform.y = this.wordPanel.transform.y + (this.wordPanel.transform.height / 5);
    this.addButton.setActive(false);
    
	this.minusButton = this.game.create.image(0, 0, 110, 110, MINUS_BUTTON_SRC);
    this.minusButton.transform.x = (this.wordPanel.transform.width - this.minusButton.transform.width) / 2.5;
    this.minusButton.transform.y = this.wordPanel.transform.y + (this.wordPanel.transform.height / 5);
    this.minusButton.setActive(false);
    
	this.multiplyButton = this.game.create.image(0, 0, 110, 110, MULTIPLY_BUTTON_SRC);
    this.multiplyButton.transform.x = (this.wordPanel.transform.width - this.multiplyButton.transform.width) / 1.6;
    this.multiplyButton.transform.y = this.wordPanel.transform.y + (this.wordPanel.transform.height / 5);
    this.multiplyButton.setActive(false);
    
	this.divideButton = this.game.create.image(0, 0, 110, 110, DIVIDE_BUTTON_SRC);
    this.divideButton.transform.x = (this.wordPanel.transform.width - this.divideButton.transform.width) / 1.17;
    this.divideButton.transform.y = this.wordPanel.transform.y + (this.wordPanel.transform.height / 5);
    this.divideButton.setActive(false);
};

BattleMenu.prototype._setupBattleButtons = function()
{
    
    var self = this;
    
    // Start Defend Sequence
    this.defendButton.events.click.push(function()
    {
		sndButton.play();
        var defendButton = this;
		add = self.enemy.add;
		sub = self.enemy.sub;
		mult = self.enemy.mult;
		div = self.enemy.div;
		setEnemyAttack(this.enemy);
		lastAttack = attackChoice;
        var problem = self.getMathEquation();
        
        self.state = BattleMenu.States.DEFEND;
        
        self._showMathLabels.call(self, problem);
        
        self.mathProblem.tweenComponent.move(self.mathProblem.transform.x,
                                             self.game.height / 2,
                                             self.mathOutline.transform.x,
                                             self.mathOutline.transform.y,
                                             self.player.speed,
                                             self._doPlayerHit.bind(self));
        
        self.defendButton.setActive(false);
    });
	window.addEventListener("keydown", function(e){    
		if(e.which == 13 && battleMenu.defendButton.isEnabled)
		{
			self.defendButton.imageComponent.setImage(DEFEND_BUTTON_INVERT_SRC);
		}
	});
	window.addEventListener("keyup", function(e){    
		if(e.which == 13 && battleMenu.defendButton.isEnabled)
		{
			self.defendButton.imageComponent.setImage(DEFEND_BUTTON_SRC);
			sndButton.play();
			var defendButton = this;
			add = self.enemy.add;
			sub = self.enemy.sub;
			mult = self.enemy.mult;
			div = self.enemy.div;
			setEnemyAttack(this.enemy);
			lastAttack = attackChoice;
			var problem = self.getMathEquation();
			
			self.state = BattleMenu.States.DEFEND;
			
			self._showMathLabels.call(self, problem);
			
			self.mathProblem.tweenComponent.move(self.mathProblem.transform.x,
												 self.game.height / 2,
												 self.mathOutline.transform.x,
												 self.mathOutline.transform.y,
												 self.player.speed,
												 self._doPlayerHit.bind(self));
			
			self.defendButton.setActive(false);
		}
	});
    
    // Start Attack Sequence
    this.attackButton.events.click.push(function()
    {
		sndButton.play();
        self.attackButton.setActive(false);
		
        self._showOperators();
    });
    
	window.addEventListener("keydown", function(e){    
		if(e.which == 13 && battleMenu.attackButton.isEnabled)
		{
			self.attackButton.imageComponent.setImage(ATTACK_BUTTON_INVERT_SRC);
		}
	});
    
	window.addEventListener("keyup", function(e){    
		if(e.which == 13 && battleMenu.attackButton.isEnabled)
		{			
			self.attackButton.imageComponent.setImage(ATTACK_BUTTON_SRC);
			sndButton.play();
			self.attackButton.setActive(false);
			
			self._showOperators();
		}
	});
	
	window.addEventListener("keydown", function(e) {    
		if(e.which == 39 && (battleMenu.addButton.isEnabled || battleMenu.multiplyButton.isEnabled))
		{
			if (player.operators == "All")
            {
                self.indexAttack++;
                
                if(self.indexAttack >= 4)
                {
                    self.indexAttack = 0;
                }
            }
            else if (player.operators == "Add/Sub")
            {
                if (self.indexAttack != 0)
                {
                    self.indexAttack = 0;
                }
                else
                {
                    self.indexAttack = 1;
                }
            }
            else if (player.operators == "Multi/Div")
            {
                if (self.indexAttack != 2)
                {
                    self.indexAttack = 2;
                }
                else
                {
                    self.indexAttack = 3;
                }
            }
            
			self._setAttackImages();
		}
	});
    
	window.addEventListener("keydown", function(e) {   
		if(e.which == 37 && (battleMenu.addButton.isEnabled || battleMenu.multiplyButton.isEnabled))
		{
            if (player.operators == "All")
            {
                self.indexAttack--;
			
                if(self.indexAttack <= -1)
                {
                    self.indexAttack = 3;
                }
            }
            else if (player.operators == "Add/Sub")
            {
                if (self.indexAttack != 0)
                {
                    self.indexAttack = 0;
                }
                else
                {
                    self.indexAttack = 1;
                }
            }
            else if (player.operators == "Multi/Div")
            {
                if (self.indexAttack != 2)
                {
                    self.indexAttack = 2;
                }
                else
                {
                    self.indexAttack = 3;
                }
            }
                   
			self._setAttackImages();
		}
	});

	window.addEventListener("keydown", function(e){    
		if(e.which == 13 && battleMenu.addButton.isEnabled && self.indexAttack == 0)
		{
			self.effectToPlay = self.addAttack;
        
			sndButton.play();
			var addButton = this;
			if(lastAttack == 1 && !self.enemy.isBoss)
			{
				specialAttack = true;
			}
			attackChoice = 0;
			
			var problem = self.getMathEquation();
			
			self._showMathLabels.call(self, problem);
			
			self.state = BattleMenu.States.ATTACK;
			
			self.mathProblem.tweenComponent.move(self.mathProblem.transform.x,
												 self.game.height / 2,
												 self.mathOutline.transform.x,
												 self.mathOutline.transform.y,
												 self.player.speed,
												 self._doAttackMissed.bind(self));
														
			self._hideOperators.call(self);
		}
	});
    
	window.addEventListener("keydown", function(e){    
		if(e.which == 13 && battleMenu.minusButton.isEnabled && self.indexAttack == 1)
		{
			self.effectToPlay = self.subAttack;
        
			sndButton.play();
			var minusButton = this;
			attackChoice = 1;
			if(lastAttack == 0 && !self.enemy.isBoss)
			{
				specialAttack = true;
			}
			
			var problem = self.getMathEquation();
			
			self._showMathLabels.call(self, problem);
			
			self.state = BattleMenu.States.ATTACK;
			
			self.mathProblem.tweenComponent.move(self.mathProblem.transform.x,
												 self.game.height / 2,
												 self.mathOutline.transform.x,
												 self.mathOutline.transform.y,
												 self.player.speed,
												 self._doAttackMissed.bind(self));
														
			self._hideOperators.call(self);
		}
	});
    
	window.addEventListener("keydown", function(e){    
		if(e.which == 13 && battleMenu.multiplyButton.isEnabled && self.indexAttack == 2)
		{
			self.effectToPlay = self.multiAttack;
        
			sndButton.play();
			var multiplyButton = this;
			attackChoice = 2;
			if(lastAttack == 3 && !self.enemy.isBoss)
			{
				specialAttack = true;
			}
			
			var problem = self.getMathEquation();
			
			self._showMathLabels.call(self, problem);
			
			self.state = BattleMenu.States.ATTACK;
			
			self.mathProblem.tweenComponent.move(self.mathProblem.transform.x,
												 self.game.height / 2,
												 self.mathOutline.transform.x,
												 self.mathOutline.transform.y,
												 self.player.speed,
												 self._doAttackMissed.bind(self));
														
			self._hideOperators.call(self);
		}
	});
    
	window.addEventListener("keydown", function(e){    
		if(e.which == 13 && battleMenu.divideButton.isEnabled && self.indexAttack == 3)
		{
			self.effectToPlay = self.divideAttack;
        
			sndButton.play();
			
			attackChoice = 3;
			if(lastAttack == 2 && !self.enemy.isBoss)
			{
				specialAttack = true;
			}
			
			var problem = self.getMathEquation();
			
			self._showMathLabels.call(self, problem);
			
			self.state = BattleMenu.States.ATTACK;
			
			self.mathProblem.tweenComponent.move(self.mathProblem.transform.x,
												 self.game.height / 2,
												 self.mathOutline.transform.x,
												 self.mathOutline.transform.y,
												 self.player.speed,
												 self._doAttackMissed.bind(self));
			self._hideOperators.call(self);
		}
	});
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//Start Attack with Add Sequence
    this.addButton.events.click.push(function()
    {
		self.effectToPlay = self.addAttack;
        
        sndButton.play();
		var addButton = this;
		if(lastAttack == 1 && !self.enemy.isBoss)
		{
			specialAttack = true;
		}
		attackChoice = 0;
        
        var problem = self.getMathEquation();
        
        self._showMathLabels.call(self, problem);
        
        self.state = BattleMenu.States.ATTACK;
        
        self.mathProblem.tweenComponent.move(self.mathProblem.transform.x,
                                             self.game.height / 2,
                                             self.mathOutline.transform.x,
                                             self.mathOutline.transform.y,
                                             self.player.speed,
                                             self._doAttackMissed.bind(self));
													
		self._hideOperators.call(self);
	});
	window.addEventListener("keydown", function(e){    
		if(e.which == 65 && battleMenu.addButton.isEnabled)
		{
			self.addButton.imageComponent.setImage(ADD_BUTTON_INVERT_SRC);
		}
	});
    window.addEventListener("keyup", function(e){    
		if(e.which == 65 && battleMenu.addButton.isEnabled)
		{
			self.addButton.imageComponent.setImage(ADD_BUTTON_SRC);			
			self.effectToPlay = self.addAttack;
        
			sndButton.play();
			var addButton = this;
			if(lastAttack == 1 && !self.enemy.isBoss)
			{
				specialAttack = true;
			}
			attackChoice = 0;
			
			var problem = self.getMathEquation();
			
			self._showMathLabels.call(self, problem);
			
			self.state = BattleMenu.States.ATTACK;
			
			self.mathProblem.tweenComponent.move(self.mathProblem.transform.x,
												 self.game.height / 2,
												 self.mathOutline.transform.x,
												 self.mathOutline.transform.y,
												 self.player.speed,
												 self._doAttackMissed.bind(self));
														
			self._hideOperators.call(self);
		}
	});
    // Start Attack with Minus Sequence
    this.minusButton.events.click.push(function()
    {
		self.effectToPlay = self.subAttack;
        
        sndButton.play();
		var minusButton = this;
		attackChoice = 1;
		if(lastAttack == 0 && !self.enemy.isBoss)
		{
			specialAttack = true;
		}
        
        var problem = self.getMathEquation();
        
        self._showMathLabels.call(self, problem);
        
        self.state = BattleMenu.States.ATTACK;
        
        self.mathProblem.tweenComponent.move(self.mathProblem.transform.x,
                                             self.game.height / 2,
                                             self.mathOutline.transform.x,
                                             self.mathOutline.transform.y,
                                             self.player.speed,
                                             self._doAttackMissed.bind(self));
													
		self._hideOperators.call(self);
	});
    window.addEventListener("keydown", function(e){    
		if(e.which == 83 && battleMenu.minusButton.isEnabled)
		{
			self.minusButton.imageComponent.setImage(MINUS_BUTTON_INVERT_SRC);
		}
	});
	 window.addEventListener("keyup", function(e){    
		if(e.which == 83 && battleMenu.minusButton.isEnabled)
		{
			self.minusButton.imageComponent.setImage(MINUS_BUTTON_SRC);
			self.effectToPlay = self.subAttack;
        
			sndButton.play();
			var minusButton = this;
			attackChoice = 1;
			if(lastAttack == 0 && !self.enemy.isBoss)
			{
				specialAttack = true;
			}
			
			var problem = self.getMathEquation();
			
			self._showMathLabels.call(self, problem);
			
			self.state = BattleMenu.States.ATTACK;
			
			self.mathProblem.tweenComponent.move(self.mathProblem.transform.x,
												 self.game.height / 2,
												 self.mathOutline.transform.x,
												 self.mathOutline.transform.y,
												 self.player.speed,
												 self._doAttackMissed.bind(self));
														
			self._hideOperators.call(self);
		}
	});
	
    // Start Attack with Multiplication Sequence
    this.multiplyButton.events.click.push(function()
    {
		self.effectToPlay = self.multiAttack;
        
        sndButton.play();
		var multiplyButton = this;
		attackChoice = 2;
		if(lastAttack == 3 && !self.enemy.isBoss)
		{
			specialAttack = true;
		}
        
        var problem = self.getMathEquation();
        
        self._showMathLabels.call(self, problem);
        
        self.state = BattleMenu.States.ATTACK;
        
        self.mathProblem.tweenComponent.move(self.mathProblem.transform.x,
                                             self.game.height / 2,
                                             self.mathOutline.transform.x,
                                             self.mathOutline.transform.y,
                                             self.player.speed,
                                             self._doAttackMissed.bind(self));
													
		self._hideOperators.call(self);
	});
    
    window.addEventListener("keydown", function(e){    
		if(e.which == 68 && battleMenu.multiplyButton.isEnabled)
		{
			self.multiplyButton.imageComponent.setImage(MULTIPLY_BUTTON_INVERT_SRC);
		}
	});
	window.addEventListener("keyup", function(e){    
		if(e.which == 68 && battleMenu.multiplyButton.isEnabled)
		{
			self.multiplyButton.imageComponent.setImage(MULTIPLY_BUTTON_SRC);
			self.effectToPlay = self.multiAttack;
        
			sndButton.play();
			var multiplyButton = this;
			attackChoice = 2;
			if(lastAttack == 3 && !self.enemy.isBoss)
			{
				specialAttack = true;
			}
			
			var problem = self.getMathEquation();
			
			self._showMathLabels.call(self, problem);
			
			self.state = BattleMenu.States.ATTACK;
			
			self.mathProblem.tweenComponent.move(self.mathProblem.transform.x,
												 self.game.height / 2,
												 self.mathOutline.transform.x,
												 self.mathOutline.transform.y,
												 self.player.speed,
												 self._doAttackMissed.bind(self));
														
			self._hideOperators.call(self);
		}
	});
    // Start Attack with Division Sequence
    this.divideButton.events.click.push(function()
    {
		self.effectToPlay = self.divideAttack;
        
        sndButton.play();
        
		attackChoice = 3;
		if(lastAttack == 2 && !self.enemy.isBoss)
		{
			specialAttack = true;
		}
        
        var problem = self.getMathEquation();
        
        self._showMathLabels.call(self, problem);
        
        self.state = BattleMenu.States.ATTACK;
        
        self.mathProblem.tweenComponent.move(self.mathProblem.transform.x,
                                             self.game.height / 2,
                                             self.mathOutline.transform.x,
                                             self.mathOutline.transform.y,
                                             self.player.speed,
                                             self._doAttackMissed.bind(self));
		self._hideOperators.call(self);
	});
	
    window.addEventListener("keydown", function(e){    
		if(e.which == 70 && battleMenu.divideButton.isEnabled)
		{
			self.divideButton.imageComponent.setImage(DIVIDE_BUTTON_INVERT_SRC);
		}
	});
	window.addEventListener("keyup", function(e){    
		if(e.which == 70 && battleMenu.divideButton.isEnabled)
		{
			self.divideButton.imageComponent.setImage(DIVIDE_BUTTON_SRC);
			self.effectToPlay = self.divideAttack;
        
			sndButton.play();
			
			attackChoice = 3;
			if(lastAttack == 2 && !self.enemy.isBoss)
			{
				specialAttack = true;
			}
			
			var problem = self.getMathEquation();
			
			self._showMathLabels.call(self, problem);
			
			self.state = BattleMenu.States.ATTACK;
			
			self.mathProblem.tweenComponent.move(self.mathProblem.transform.x,
												 self.game.height / 2,
												 self.mathOutline.transform.x,
												 self.mathOutline.transform.y,
												 self.player.speed,
												 self._doAttackMissed.bind(self));
			self._hideOperators.call(self);
		}
	});
};

BattleMenu.prototype.getMathEquation = function()
{
	if (player.difficulty == "Easy")
    {
		return makeEquation();
    }
    else if (player.difficulty == "Normal")
    {
		return makeMedEquation();
    }
    else if (player.difficulty == "Hard")
    {
		return makeAdvEquation();
    }
};

BattleMenu.prototype._doGameOver = function()
{
    var self = this;
    
    self.hint.setActive(true);
    self.hint.textScrollComponent.create("You've been defeated!", 2, 70, function(){
        soundBattle.pause();
        if (soundBattle.currentTime) 
        {
            soundBattle.currentTime = 0;  
        }  
		sndDeath.play();
        self.disable();		
        //restart();								
		//self.disable();
		endGame.open(); 
    });
					 
};

BattleMenu.prototype._displayEnemyHitMessage = function()
{
    var self = this;
    
    self.hint.setActive(true);
															
    self.hint.textScrollComponent.create(self.enemy.name + " hit you.", 2, 30, function()
    {
        self.attackButton.setActive(true);
        self.hint.setActive(false);
        self.hint.textComponent.text = "";
    });
};

BattleMenu.prototype._showMathLabels = function(problem)
{
    this.mathProblem.setActive(true);
    this.mathProblem.textComponent.text = problem;
        
    this.mathOutline.setActive(true);
    this.mathOutline.textComponent.text = problem;
        
    this.answerField.setActive(true);
    this.answerField.textComponent.text = "";
    
    this.answerField.transform.left = this.mathOutline.transform.right;
};

BattleMenu.prototype._hideMathLabels = function()
{
    this.mathProblem.setActive(false);
    this.mathOutline.setActive(false);
    this.answerField.setActive(false);
};

BattleMenu.prototype._doPlayerHit = function()
{
    this.state = BattleMenu.States.ATTACK;

    sndDamage.play();

    this.player.hit(this.enemy.damage);
    this.healthBar.imageRenderer.setMask(this.player.health);

    if (this.player.isDead())
    {
        this._doGameOver.call(this);
    }
    else
    {
        this._displayEnemyHitMessage.call(this);
    }

    this._hideMathLabels.call(this);

    this.enemyEffectToPlay.setActive(true);
    this.enemyEffectToPlay.animations.play("attack");
};

BattleMenu.prototype._doAttackMissed = function() 
{
    this.state = BattleMenu.States.DEFEND;
            
    this.mathProblem.setActive(false);
    this.mathOutline.setActive(false);
    this.answerField.setActive(false);
    
    this._displayMissHint();
};

BattleMenu.prototype._displayMissHint = function()
{
    var self = this;
    
    this.hint.setActive(true);
    
    this.hint.textScrollComponent.create("You missed.", 2, 30, function()
    {
        self.defendButton.setActive(true);
        self.hint.setActive(false);
        self.hint.textComponent.text = "";
    });
};

BattleMenu.prototype._hideOperators = function()
{
    this.addButton.setActive(false);
    this.minusButton.setActive(false);
    this.multiplyButton.setActive(false);
    this.divideButton.setActive(false);  
};

BattleMenu.prototype._showOperators = function()
{    
    if (player.operators == "All")
    {
        this.addButton.transform.x = (this.wordPanel.transform.width - this.addButton.transform.width) / 6;
        this.minusButton.transform.x = (this.wordPanel.transform.width - this.minusButton.transform.width) / 2.5;
        this.multiplyButton.transform.x = (this.wordPanel.transform.width - this.multiplyButton.transform.width) / 1.6;
        this.divideButton.transform.x = (this.wordPanel.transform.width - this.divideButton.transform.width) / 1.17;
        
        this.addButton.setActive(true);
        this.minusButton.setActive(true);
        this.multiplyButton.setActive(true);
        this.divideButton.setActive(true);
    }
    else if (player.operators == "Add/Sub")
    {
        this.addButton.transform.x = (this.wordPanel.transform.width - this.addButton.transform.width) / 2.5;
        this.minusButton.transform.x = (this.wordPanel.transform.width - this.minusButton.transform.width) / 1.6;
        
        this.addButton.setActive(true);
        this.minusButton.setActive(true);
    }
    else if (player.operators == "Multi/Div")
    {
        this.multiplyButton.transform.x = (this.wordPanel.transform.width - this.multiplyButton.transform.width) / 2.5;
        this.divideButton.transform.x = (this.wordPanel.transform.width - this.divideButton.transform.width) / 1.6;
        
        this.multiplyButton.setActive(true);
        this.divideButton.setActive(true);
    }
};

BattleMenu.prototype._createKeyPad = function() 
{
    var self = this;
    this._createKeyPadButtons();
    
    this.buttonENT.events.click.push(function()
    {
        if (self.answerField.isEnabled && self.answerField.textComponent.text != "" && checkSolution(self.answerField.textComponent.text))
        {
            self.mathProblem.tweenComponent.stopAll();
            
            self._hideMathLabels();
            
            if (self.state == BattleMenu.States.ATTACK)
            {
				if(specialAttack == true)
				{
                    self.enemy.hit(self.player.damage * 2);
				}
                else
                {
                    self.enemy.hit(self.player.damage);
                }
                
                self.monsterHealthBar.imageRenderer.setMask(self.enemy.health);
                
				if (self.enemy.isDead())
                {
                    self._doBattleComplete.call(self);
                }
                else
                {
					self._displayPlayerHitMessage.call(self);
                }
                
				specialAttack = false;
                
                self.effectToPlay.transform.centerX = self.enemy.sprite.transform.centerX;
                self.effectToPlay.transform.centerY = self.enemy.sprite.transform.centerY;
                self.effectToPlay.setActive(true);
                self.effectToPlay.animations.play("attack");
            }
            else
            {
                self.hint.setActive(true);
                self.hint.textScrollComponent.create("You blocked the attack.", 2, 30, function()
                {
                    self.attackButton.setActive(true);
                    self.hint.setActive(false);
                    self.hint.textComponent.text = "";
                });
            }
            
        } 
        else if (self.answerField.isEnabled && self.answerField.textComponent.text != "")
        {
			self.mathProblem.tweenComponent.stopAll();
			if (self.state == BattleMenu.States.ATTACK)
			{
				self._doAttackMissed.call(self);
			}
			else
			{
				self._doPlayerHit.call(self);
			}
        }

        self.answerField.textComponent.text = "";
    });
    
    this.buttonCL.events.click.push(function()
    {
        self.answerField.textComponent.text = "";
    });
    
};

BattleMenu.prototype._doBattleComplete = function()
{
    var self = this;
    
    sndEnemyDeath.play();
	
	enemiesDefeated += 1;
    
    self.hint.setActive(true);
    self.hint.textScrollComponent.create("You defeated " + self.enemy.name + "!", 2, 30, function(){
        soundBattle.pause();
        if (soundBattle.currentTime) 
        {
            soundBattle.currentTime = 0;  
        }
        self.hint.setActive(false);
        self.hint.textComponent.text = "";
        self.disable();

        if (bossBattle)
        {
            
            player.save();
            endGame.open();
        }
        else
        {
            navMenu.open();
        }
    });

    self.game.remove(self.enemy.sprite);
};

BattleMenu.prototype._displayPlayerHitMessage = function()
{
    var self = this;
    
    sndDamage.play();
    self.hint.setActive(true);
    if(specialAttack == true)
    {
        self.hint.textScrollComponent.create("You critically hit " + self.enemy.name + "!", 2, 30, function()
        {
            self.defendButton.setActive(true);
            self.hint.setActive(false);
            self.hint.textComponent.text = "";
        });
    }
    else
    {
        self.hint.textScrollComponent.create("You hit " + self.enemy.name + ".", 2, 30, function()
        {
            self.defendButton.setActive(true);
            self.hint.setActive(false);
            self.hint.textComponent.text = "";
        });
    }
};

BattleMenu.prototype._createKeyPadButtons = function()
{	
    var self = this;
    
    this.button0.events.click.push(function() {
		sndButton.play();
        self.answerField.textComponent.text += 0;
    });
    
    this.button0.events.out.push(function() {
        this.imageComponent.setImage(ZERO_BUTTON_SRC);
    });
    
    this.button0.events.over.push(function() {
        this.imageComponent.setImage(ZERO_LIGHT_BUTTON_SRC);
    });
    
    this.button0.events.down.push(function() {
        this.imageComponent.setImage(ZERO_INVERT_BUTTON_SRC);
    });
    
    this.button0.events.up.push(function() {
        this.imageComponent.setImage(ZERO_BUTTON_SRC);
    });
    //one
    this.button1.events.click.push(function() {
		sndButton.play();
        self.answerField.textComponent.text += 1;
    });
    
    this.button1.events.out.push(function() {
        this.imageComponent.setImage(ONE_BUTTON_SRC);
    });
    
    this.button1.events.over.push(function() {
        this.imageComponent.setImage(ONE_LIGHT_BUTTON_SRC);
    });
    
    this.button1.events.down.push(function() {
        this.imageComponent.setImage(ONE_INVERT_BUTTON_SRC);
    });
    
    this.button1.events.up.push(function() {
        this.imageComponent.setImage(ONE_BUTTON_SRC);
    });
	//two
    this.button2.events.click.push(function() {
		sndButton.play();
        self.answerField.textComponent.text += 2;
    });
    
    this.button2.events.out.push(function() {
        this.imageComponent.setImage(TWO_BUTTON_SRC);
    });
    
    this.button2.events.over.push(function() {
        this.imageComponent.setImage(TWO_LIGHT_BUTTON_SRC);
    });
    
    this.button2.events.down.push(function() {
        this.imageComponent.setImage(TWO_INVERT_BUTTON_SRC);
    });
    
    this.button2.events.up.push(function() {
        this.imageComponent.setImage(TWO_BUTTON_SRC);
    });
	//three
    this.button3.events.click.push(function() {
		sndButton.play();
        self.answerField.textComponent.text += 3;
    });
    
    this.button3.events.out.push(function() {
        this.imageComponent.setImage(THREE_BUTTON_SRC);
    });
    
    this.button3.events.over.push(function() {
        this.imageComponent.setImage(THREE_LIGHT_BUTTON_SRC);
    });
    
    this.button3.events.down.push(function() {
        this.imageComponent.setImage(THREE_INVERT_BUTTON_SRC);
    });
    
    this.button3.events.up.push(function() {
        this.imageComponent.setImage(THREE_BUTTON_SRC);
    });
	//four
    this.button4.events.click.push(function() {
		sndButton.play();
        self.answerField.textComponent.text += 4;
    });
    
    this.button4.events.out.push(function() {
        this.imageComponent.setImage(FOUR_BUTTON_SRC);
    });
    
    this.button4.events.over.push(function() {
        this.imageComponent.setImage(FOUR_LIGHT_BUTTON_SRC);
    });
    
    this.button4.events.down.push(function() {
        this.imageComponent.setImage(FOUR_INVERT_BUTTON_SRC);
    });
    
    this.button4.events.up.push(function() {
        this.imageComponent.setImage(FOUR_BUTTON_SRC);
    });
	//five
    this.button5.events.click.push(function() {
		sndButton.play();
        self.answerField.textComponent.text += 5;
    });
    
    this.button5.events.out.push(function() {
        this.imageComponent.setImage(FIVE_BUTTON_SRC);
    });
    
    this.button5.events.over.push(function() {
        this.imageComponent.setImage(FIVE_LIGHT_BUTTON_SRC);
    });
    
    this.button5.events.down.push(function() {
        this.imageComponent.setImage(FIVE_INVERT_BUTTON_SRC);
    });
    
    this.button5.events.up.push(function() {
        this.imageComponent.setImage(FIVE_BUTTON_SRC);
    });
	//six
    this.button6.events.click.push(function() {
		sndButton.play();
        self.answerField.textComponent.text += 6;
    });
    
    this.button6.events.out.push(function() {
        this.imageComponent.setImage(SIX_BUTTON_SRC);
    });
    
    this.button6.events.over.push(function() {
        this.imageComponent.setImage(SIX_LIGHT_BUTTON_SRC);
    });
    
    this.button6.events.down.push(function() {
        this.imageComponent.setImage(SIX_INVERT_BUTTON_SRC);
    });
    
    this.button6.events.up.push(function() {
        this.imageComponent.setImage(SIX_BUTTON_SRC);
    });
	//seven
    this.button7.events.click.push(function() {
		sndButton.play();
        self.answerField.textComponent.text += 7;
    });
    
    this.button7.events.out.push(function() {
        this.imageComponent.setImage(SEVEN_BUTTON_SRC);
    });
    
    this.button7.events.over.push(function() {
        this.imageComponent.setImage(SEVEN_LIGHT_BUTTON_SRC);
    });
    
    this.button7.events.down.push(function() {
        this.imageComponent.setImage(SEVEN_INVERT_BUTTON_SRC);
    });
    
    this.button7.events.up.push(function() {
        this.imageComponent.setImage(SEVEN_BUTTON_SRC);
    });
	//eight
    this.button8.events.click.push(function() {
		sndButton.play();
        self.answerField.textComponent.text += 8;
    });
    
    this.button8.events.out.push(function() {
        this.imageComponent.setImage(EIGHT_BUTTON_SRC);
    });
    
    this.button8.events.over.push(function() {
        this.imageComponent.setImage(EIGHT_LIGHT_BUTTON_SRC);
    });
    
    this.button8.events.down.push(function() {
        this.imageComponent.setImage(EIGHT_INVERT_BUTTON_SRC);
    });
    
    this.button8.events.up.push(function() {
        this.imageComponent.setImage(EIGHT_BUTTON_SRC);
    });
	//nine
    this.button9.events.click.push(function() {
		sndButton.play();
        self.answerField.textComponent.text += 9;
    });
    
    this.button9.events.out.push(function() {
        this.imageComponent.setImage(NINE_BUTTON_SRC);
    });
    
    this.button9.events.over.push(function() {
        this.imageComponent.setImage(NINE_LIGHT_BUTTON_SRC);
    });
    
    this.button9.events.down.push(function() {
        this.imageComponent.setImage(NINE_INVERT_BUTTON_SRC);
    });
    
    this.button9.events.up.push(function() {
        this.imageComponent.setImage(NINE_BUTTON_SRC);
    });
	//clear    
    this.buttonCL.events.out.push(function() {
        this.imageComponent.setImage(CLEAR_BUTTON_SRC);
    });
    
    this.buttonCL.events.over.push(function() {
        this.imageComponent.setImage(CLEAR_LIGHT_BUTTON_SRC);
    });
    
    this.buttonCL.events.down.push(function() {
        this.imageComponent.setImage(CLEAR_INVERT_BUTTON_SRC);
    });
    
    this.buttonCL.events.up.push(function() {
        this.imageComponent.setImage(CLEAR_BUTTON_SRC);
    });
	//enter    
    this.buttonENT.events.out.push(function() {
        this.imageComponent.setImage(ENTER_BUTTON_SRC);
    });
    
    this.buttonENT.events.over.push(function() {
        this.imageComponent.setImage(ENTER_LIGHT_BUTTON_SRC);
    });
    
    this.buttonENT.events.down.push(function() {
        this.imageComponent.setImage(ENTER_INVERT_BUTTON_SRC);
    });
    
    this.buttonENT.events.up.push(function() {
        this.imageComponent.setImage(ENTER_BUTTON_SRC);
    });

    this.addButton.events.over.push(function() {
        this.imageComponent.setImage(ADD_BUTTON_SRC);
    });
    this.addButton.events.out.push(function() {
        this.imageComponent.setImage(ADD_BUTTON_DARK_SRC);
    });
	this.addButton.events.down.push(function() {
        this.imageComponent.setImage(ADD_BUTTON_INVERT_SRC);
    });    
    this.addButton.events.up.push(function() {
        this.imageComponent.setImage(ADD_BUTTON_DARK_SRC);
    });
	this.minusButton.events.over.push(function() {
        this.imageComponent.setImage(MINUS_BUTTON_SRC);
    });
	this.minusButton.events.out.push(function() {
        this.imageComponent.setImage(MINUS_BUTTON_DARK_SRC);
    });
	this.minusButton.events.down.push(function() {
        this.imageComponent.setImage(MINUS_BUTTON_INVERT_SRC);
    });    
    this.minusButton.events.up.push(function() {
        this.imageComponent.setImage(MINUS_BUTTON_DARK_SRC);
    });
	this.multiplyButton.events.over.push(function() {
        this.imageComponent.setImage(MULTIPLY_BUTTON_SRC);
    });
	this.multiplyButton.events.out.push(function() {
        this.imageComponent.setImage(MULTIPLY_BUTTON_DARK_SRC);
    });
	this.multiplyButton.events.down.push(function() {
        this.imageComponent.setImage(MULTIPLY_BUTTON_INVERT_SRC);
    });    
    this.multiplyButton.events.up.push(function() {
        this.imageComponent.setImage(MULTIPLY_BUTTON_DARK_SRC);
    });
	this.divideButton.events.over.push(function() {
        this.imageComponent.setImage(DIVIDE_BUTTON_SRC);
    });
	this.divideButton.events.out.push(function() {
        this.imageComponent.setImage(DIVIDE_BUTTON_DARK_SRC);
    });
	this.divideButton.events.down.push(function() {
        this.imageComponent.setImage(DIVIDE_BUTTON_INVERT_SRC);
    });    
    this.divideButton.events.up.push(function() {
        this.imageComponent.setImage(DIVIDE_BUTTON_DARK_SRC);
    });
	this.attackButton.events.over.push(function() {
        this.imageComponent.setImage(ATTACK_BUTTON_INVERT_SRC);
    });	
	this.attackButton.events.out.push(function() {
        this.imageComponent.setImage(ATTACK_BUTTON_SRC);
    });	
	this.defendButton.events.over.push(function() {
        this.imageComponent.setImage(DEFEND_BUTTON_INVERT_SRC);
    });
	this.defendButton.events.out.push(function() {
        this.imageComponent.setImage(DEFEND_BUTTON_SRC);
    });
};

BattleMenu.prototype._createMenu = function()
{
	var self = this;
    
	this.menuButtonB = this.game.create.image(0, 0, 175, 100, MENU_BUTTON_SRC);
	this.menuButtonB.transform.scaleX = 0.9;
    this.menuButtonB.transform.scaleY = 0.8;
    this.menuButtonB.transform.pivotX = 0.5;
    this.menuButtonB.transform.pivotY = 0.5;
    this.menuButtonB.transform.centerX = this.rightPanel.transform.centerX;
    this.menuButtonB.transform.bottom = this.rightPanel.transform.bottom - 30;
    
    this.menuButtonB.events.over.push(function()
    {
        self.menuButtonB.transform.scaleX = 1;
        self.menuButtonB.transform.scaleY = 0.9;
    });
    
    this.menuButtonB.events.out.push(function()
    {
        self.menuButtonB.transform.scaleX = 0.9;
        self.menuButtonB.transform.scaleY = 0.8;
    });
	
	this.menuButtonB.events.up.push(function() {
        this.imageComponent.setImage(MENU_BUTTON_SRC);
    });
	this.menuButtonB.events.down.push(function() {
        this.imageComponent.setImage(MENU_INVERT_BUTTON_SRC);
    });
	this.menuButtonB.events.click.push(function()
	{
		self.menuButtonB.setActive(false)
		quitMenu.open();
	});	
	window.addEventListener("keyup", function(e)
	{
		if(e.which == 27 && battleMenu.itemPanel.isEnabled)
		{
			self.menuButtonB.setActive(false)
			quitMenu.open();
		}
	});
}

BattleMenu.prototype.disable = function()
{
    for (var property in this)
    {
        if (this.hasOwnProperty(property))
        {

            if (this[property] instanceof Entity)
            {
                this[property].setActive(false);
            }
            else if (this[property] instanceof Array)
            {
                for (var i = 0; i < this[property].length; i++)
                {
                    if (this[property][i] instanceof Entity)
                    {
                        this[property][i].setActive(false);
                    }
                }
            }
        }
    }
    
    if (this.player != null) {
        this.player.resetStats();
    }
    
    if (this.enemy != null && this.enemy.sprite != null) 
    {
        this.game.remove(this.enemy.sprite);
    }
    
    this.hint.textScrollComponent.stopAll();
};


BattleMenu.prototype.open = function(enemy)
{
    this.enemy = enemy;
    this.player = player;
    
    this.wordPanel.setActive(true);
    this.rightPanel.setActive(true); 
	this.keyPanel.setActive(true);
    this.itemPanel.setActive(true);
    this.menuButtonB.setActive(true);
    
    this.healthBar.setActive(true);
    this.damageBar.setActive(true);
    this.monsterHealthBar.setActive(true);
    this.monsterDamageBar.setActive(true);
    
    this._displayEnemy();
    this._displayPlayer();
    this._displayPotion();
    this._displayIndexedAttack();
    
    specialAttack = false;
    bossBattle = this.enemy.isBoss;
    this.state = BattleMenu.States.DEFEND;
    
    // Enable all keypad buttons.
    for (var property in this)
    {
        if (this.hasOwnProperty(property))
        {
            if (property.substr(0, 6) == "button")
            {
                this[property].setActive(true);
            }
        }
    }
    
    this._start();
};

BattleMenu.prototype._displayIndexedAttack = function()
{
    if (player.operators == "All")
    {
        if (this.indexAttack < 0 || this.indexAttack > 3)
        {
            this.indexAttack = 0;
        }
    }
    else if (player.operators == "Add/Sub")
    {
        if (this.indexAttack < 0 || this.indexAttack > 1)
        {
            this.indexAttack = 0;
        }
    }
    else if (player.operators == "Multi/Div")
    {
        if (this.indexAttack < 2 || this.indexAttack > 3)
        {
            this.indexAttack = 2;
        }
    }
    
    this._setAttackImages();
};

BattleMenu.prototype._setAttackImages = function()
{
    if (this.indexAttack == 0)
    {
        this.addButton.imageComponent.setImage(ADD_BUTTON_SRC);
        this.minusButton.imageComponent.setImage(MINUS_BUTTON_DARK_SRC);
        this.multiplyButton.imageComponent.setImage(MULTIPLY_BUTTON_DARK_SRC);
        this.divideButton.imageComponent.setImage(DIVIDE_BUTTON_DARK_SRC);
    }			
    else if(this.indexAttack == 1)
    {
        this.addButton.imageComponent.setImage(ADD_BUTTON_DARK_SRC);
        this.minusButton.imageComponent.setImage(MINUS_BUTTON_SRC);
        this.multiplyButton.imageComponent.setImage(MULTIPLY_BUTTON_DARK_SRC);
        this.divideButton.imageComponent.setImage(DIVIDE_BUTTON_DARK_SRC);
    }			
    else if(this.indexAttack == 2)
    {
        this.addButton.imageComponent.setImage(ADD_BUTTON_DARK_SRC);
        this.minusButton.imageComponent.setImage(MINUS_BUTTON_DARK_SRC);
        this.multiplyButton.imageComponent.setImage(MULTIPLY_BUTTON_SRC);
        this.divideButton.imageComponent.setImage(DIVIDE_BUTTON_DARK_SRC);
    }
    else if(this.indexAttack == 3)
    {
        this.addButton.imageComponent.setImage(ADD_BUTTON_DARK_SRC);
        this.minusButton.imageComponent.setImage(MINUS_BUTTON_DARK_SRC);
        this.multiplyButton.imageComponent.setImage(MULTIPLY_BUTTON_DARK_SRC);
        this.divideButton.imageComponent.setImage(DIVIDE_BUTTON_SRC);
    }
};
