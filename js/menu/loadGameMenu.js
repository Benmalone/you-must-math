function LoadGameMenu(game, startMenu, navMenu) 
{
    this.game = game;
    
    this.startMenu = startMenu;
    this.navMenu = navMenu;
    
    this._setupSlots();
    this._setupSlotData();
    this._setupBackButton();
    
    this.close();
}

LoadGameMenu.prototype._setupSlots = function()
{
    var self = this;
    
    this.slots = [];
    
    for (var i = 0; i < 3; i++)
    {
        var slot = this.game.create.image(0, 0, 290, 180, SLOT_SRC);
        slot.transform.pivotX = 0.5;
        slot.transform.pivotY = 0.5;
        slot.transform.scaleX = 1.1;
        slot.transform.scaleY = 1.1;
        slot.transform.centerX = this.game.width / 2;
        slot.transform.top = (slot.transform.height * 1.05) * i;
        slot.transform.z = -10;
        
        this._setupSlotEvents(slot);
        
        this.slots[i] = slot;
    }
	
	//Keypressing
	var indexSlot = 0;
	this.slots[indexSlot].imageComponent.setImage(SLOT_INVERT_SRC);
	
	window.addEventListener("keydown", function(e){    
		if((e.which == 38 || e.which == 87) && loadGameMenu.backButton.isEnabled)
		{
			indexSlot--;
			if(indexSlot <= -1)
			{
				indexSlot = 3;
			}
			if(indexSlot == 0)
			{
				loadGameMenu.slots[indexSlot].imageComponent.setImage(SLOT_INVERT_SRC);
				loadGameMenu.slots[1].imageComponent.setImage(SLOT_SRC);
			}			
			else if(indexSlot == 1)
			{	
				loadGameMenu.slots[indexSlot].imageComponent.setImage(SLOT_INVERT_SRC);
				loadGameMenu.slots[2].imageComponent.setImage(SLOT_SRC);	
			}			
			else if(indexSlot == 2)
			{
				loadGameMenu.backButton.imageComponent.setImage(BACK_BUTTON_SRC);
				loadGameMenu.slots[indexSlot].imageComponent.setImage(SLOT_INVERT_SRC);
			}		
			else if(indexSlot == 3)
			{
				loadGameMenu.backButton.imageComponent.setImage(BACK_BUTTON_INVERT_SRC);
				loadGameMenu.slots[0].imageComponent.setImage(SLOT_SRC);
			}
		}   
		if((e.which == 83 || e.which == 40) && loadGameMenu.backButton.isEnabled)
		{
			indexSlot++;
			if(indexSlot >= 4)
			{
				indexSlot = 0;
			}
			if(indexSlot == 0)
			{
				loadGameMenu.backButton.imageComponent.setImage(BACK_BUTTON_SRC);
				loadGameMenu.slots[indexSlot].imageComponent.setImage(SLOT_INVERT_SRC);
			}			
			else if(indexSlot == 1)
			{
				loadGameMenu.slots[indexSlot].imageComponent.setImage(SLOT_INVERT_SRC);
				loadGameMenu.slots[0].imageComponent.setImage(SLOT_SRC);				
			}		
			else if(indexSlot == 2)
			{
				loadGameMenu.slots[indexSlot].imageComponent.setImage(SLOT_INVERT_SRC);
				loadGameMenu.slots[1].imageComponent.setImage(SLOT_SRC);				
			}	
			else if(indexSlot == 3)
			{
				loadGameMenu.backButton.imageComponent.setImage(BACK_BUTTON_INVERT_SRC);
				loadGameMenu.slots[2].imageComponent.setImage(SLOT_SRC);				
			}
		} 
	});
	window.addEventListener("keyup", function(e){    
		if(e.which == 13 && loadGameMenu.backButton.isEnabled)
		{
			if(indexSlot == 3)
			{
				indexSlot = 0;
				loadGameMenu.slots[indexSlot].imageComponent.setImage(SLOT_INVERT_SRC);
				loadGameMenu.backButton.imageComponent.setImage(BACK_BUTTON_SRC);
			}
		}
	});
};

LoadGameMenu.prototype._setupSlotEvents = function(slot)
{
    var self = this;
    var slot = slot
    
    slot.events.over.push(function()
    {
        if (!SaveData.isSlotEmpty(self._getSlotIndex(slot)))
        {
            slot.imageComponent.setImage(SLOT_SELECTED_SRC);
        }
    });
    
    slot.events.out.push(function()
    {
        if (!SaveData.isSlotEmpty(self._getSlotIndex(slot)))
        {
            slot.imageComponent.setImage(SLOT_SRC);
        }
    });
    
    slot.events.down.push(function()
    {
        if (!SaveData.isSlotEmpty(self._getSlotIndex(slot)))
        {
            slot.imageComponent.setImage(SLOT_INVERT_SRC);
        }
    });
    
    slot.events.up.push(function()
    {
        if (!SaveData.isSlotEmpty(self._getSlotIndex(slot)))
        {
            slot.imageComponent.setImage(SLOT_SRC);
        }
    });
    
    slot.events.click.push(function()
    {
        var index = self._getSlotIndex(slot);
        
        if (!SaveData.isSlotEmpty(index))
        {
            var stats = SaveData.getPlayerStat(index);
            player = new Player(self.game, stats.name, stats.difficulty, stats.operators, stats.level, stats.levelsCompleted, stats.score, stats.rank, stats.itemType, index);
            self.close();
            self.navMenu.open();
            background.setActive(false);
        }
    });
	
	
	//Keypressing
	var indexSlot2 = 0;
	
	window.addEventListener("keydown", function(e){    
		if((e.which == 87 || e.which == 38) && loadGameMenu.backButton.isEnabled)
		{
			indexSlot2--;
			if(indexSlot2 <= -1)
			{
				indexSlot2 = 3;
			}
		}  
		if((e.which == 83 || e.which == 40) && loadGameMenu.backButton.isEnabled)
		{
			indexSlot2++;
			if(indexSlot2 >= 4)
			{
				indexSlot2 = 0;
			}
		}  
	});
	window.addEventListener("keyup", function(e){    
		if(e.which == 13 && loadGameMenu.backButton.isEnabled)
		{
			if(indexSlot2 == 0)
			{
				if (!SaveData.isSlotEmpty(indexSlot2))
				{
					var stats = SaveData.getPlayerStat(indexSlot2);
					player = new Player(self.game, stats.name, stats.difficulty, stats.operators, stats.level, stats.levelsCompleted, stats.score, stats.rank, stats.itemType, indexSlot2);
					self.close();
					self.navMenu.open();
					background.setActive(false);
				}				
			}			
			else if(indexSlot2 == 1)
			{        
				if (!SaveData.isSlotEmpty(indexSlot2))
				{
					var stats = SaveData.getPlayerStat(indexSlot2);
					player = new Player(self.game, stats.name, stats.difficulty, stats.operators, stats.level, stats.levelsCompleted, stats.score, stats.rank, stats.itemType, indexSlot2);
					self.close();
					self.navMenu.open();
					background.setActive(false);
				}				
			}		
			else if(indexSlot2 == 2)
			{        
				if (!SaveData.isSlotEmpty(indexSlot2))
				{
					var stats = SaveData.getPlayerStat(indexSlot2);
					player = new Player(self.game, stats.name, stats.difficulty, stats.operators, stats.level, stats.levelsCompleted, stats.score, stats.rank, stats.itemType, indexSlot2);
					self.close();
					self.navMenu.open();
					background.setActive(false);
				}				
			}	
			else if(indexSlot2 == 3)
			{
				setTimeout(function(){
					indexSlot2 = 0;
					self.close();
					self.startMenu.open();	
				}, 10);					
			}
		}
	});
};

LoadGameMenu.prototype._setupSlotData = function()
{
    this._setupSlotLabels();
    this._setupNameLabels();
    this._setupDifficultyLabels();
    this._setupOperatorLabels();
    this._setupLevelLabels();
    this._setupRankLabels();
    this._setupLevelsCompletedLabels();
    this._setupNoDataLabels();
};

LoadGameMenu.prototype._setupSlotLabels = function()
{
    this.slotLabels = [];
    
    for (var i = 0; i < this.slots.length; i++) 
    {
        var label = this.game.create.label(0, 0, "Slot " + (i + 1), "sans-serif", 20, "white");
        label.transform.top = this.slots[i].transform.top + 32;
        label.transform.left = this.slots[i].transform.left + 95;
        label.transform.z = -11;
        label.transform.rotation = -Math.PI / 20;
        
        this.slotLabels[i] = label;
    }
};

LoadGameMenu.prototype._setupNameLabels = function()
{
    this.nameLabels = [];
    
    for (var i = 0; i < this.slots.length; i++) 
    {
        var label = this.game.create.label(0, 0, "Name: ", "sans-serif", 16, "white");
        label.transform.top = this.slots[i].transform.top + 75;
        label.transform.left = this.slots[i].transform.left + 70;
        label.transform.z = -11;
        
        this.nameLabels[i] = label;
    }
};

LoadGameMenu.prototype._setupDifficultyLabels= function()
{
    this.difficultyLabels = [];
    
    for (var i = 0; i < this.slots.length; i++) 
    {
        var label = this.game.create.label(0, 0, "Difficulty: ", "sans-serif", 16, "white");
        label.transform.top = this.slots[i].transform.top + 93;
        label.transform.left = this.slots[i].transform.left + 70;
        label.transform.z = -11;
        
        this.difficultyLabels[i] = label;
    }
};

LoadGameMenu.prototype._setupOperatorLabels = function()
{
    this.operatorLabels = [];
    
    for (var i = 0; i < this.slots.length; i++) 
    {
        var label = this.game.create.label(0, 0, "Operators: ", "sans-serif", 16, "white");
        label.transform.top = this.slots[i].transform.top + 111;
        label.transform.left = this.slots[i].transform.left + 70;
        label.transform.z = -11;
        
        this.operatorLabels[i] = label;
    }
};

LoadGameMenu.prototype._setupLevelLabels= function()
{
    this.levelLabels = [];
    
    for (var i = 0; i < this.slots.length; i++) 
    {
        var label = this.game.create.label(0, 0, "Level: ", "sans-serif", 16, "white");
        label.transform.top = this.slots[i].transform.top + 129;
        label.transform.left = this.slots[i].transform.left + 70;
        label.transform.z = -11;
        
        this.levelLabels[i] = label;
    }
};

LoadGameMenu.prototype._setupRankLabels = function()
{
    this.rankLabels = [];
    
    for (var i = 0; i < this.slots.length; i++) 
    {
        var label = this.game.create.label(0, 0, "Highest Rank: ", "sans-serif", 16, "white");
        label.transform.top = this.slots[i].transform.top + 147;
        label.transform.left = this.slots[i].transform.left + 70;
        label.transform.z = -11;
        
        this.rankLabels[i] = label;
    }
};

LoadGameMenu.prototype._setupLevelsCompletedLabels = function()
{
    this.levelsCompletedLabels  = [];
    
    for (var i = 0; i < this.slots.length; i++) 
    {
        var label = this.game.create.label(0, 0, "Levels Completed: ", "sans-serif", 16, "white");
        label.transform.top = this.slots[i].transform.top + 165;
        label.transform.left = this.slots[i].transform.left + 70;
        label.transform.z = -11;
        
        this.levelsCompletedLabels[i] = label;
    }
};

LoadGameMenu.prototype._setupNoDataLabels = function()
{
    this.noDataLabels = [];
    
    for (var i = 0; i < this.slots.length; i++) 
    {
        var label = this.game.create.label(0, 0, "No Data", "sans-serif", 24, "white");
        label.transform.top = this.slots[i].transform.top + 118;
        label.transform.left = this.slots[i].transform.left + 130;
        label.transform.z = -11;
        
        this.noDataLabels[i] = label;
    }
};

LoadGameMenu.prototype._setupBackButton = function()
{
    this._createAndPositionBackButton();
    this._setupBackButtonEvents();
};

LoadGameMenu.prototype._createAndPositionBackButton = function()
{
    this.backButton = this.game.create.image(0, 0, 200, 80, BACK_BUTTON_SRC);
    this.backButton.transform.pivotX = 0.5;
    this.backButton.transform.pivotY = 0.5;
    this.backButton.transform.centerX = (this.game.width / 2) + 20;
    this.backButton.transform.bottom = this.game.height - 10;
    this.backButton.transform.z = -1;
};

LoadGameMenu.prototype._setupBackButtonEvents = function()
{
    var self = this;
    
    this.backButton.events.over.push(function()
    {
        self.backButton.transform.scaleX = 1.1;
        self.backButton.transform.scaleY = 1.1;
    });
    
    this.backButton.events.out.push(function()
    {
        self.backButton.transform.scaleX = 1;
        self.backButton.transform.scaleY = 1;
    });
    
    this.backButton.events.down.push(function()
    {
        self.backButton.imageComponent.setImage(BACK_BUTTON_INVERT_SRC);
    });
    
    this.backButton.events.up.push(function()
    {
        self.backButton.imageComponent.setImage(BACK_BUTTON_SRC);
    });
    
    this.backButton.events.click.push(function()
    {
        self.close();
        self.startMenu.open();
    });
	window.addEventListener("keydown", function(e){    
		if(e.which == 27 && loadGameMenu.backButton.isEnabled)
		{
			self.close();
			self.startMenu.open();
		}
    });
};

LoadGameMenu.prototype.close = function()
{
    this._disable(this.slots);
    this._disable(this.slotLabels);
    this._disable(this.nameLabels);
    this._disable(this.difficultyLabels);
    this._disable(this.levelLabels);
    this._disable(this.operatorLabels);
    this._disable(this.rankLabels);
    this._disable(this.levelsCompletedLabels);
    this._disable(this.noDataLabels);
    this.backButton.setActive(false);
};

LoadGameMenu.prototype.open = function()
{    
    this._enableSlots();
    this.backButton.setActive(true);
};

LoadGameMenu.prototype._disable = function(entities)
{    
    for (var i = 0; i < entities.length; i++)
    {
        entities[i].setActive(false);
    }
};

LoadGameMenu.prototype._enableSlots = function()
{    
    for (var i = 0; i < this.slots.length; i++)
    {        
        if (!SaveData.isSlotEmpty(i))
        {
            var playerStat = SaveData.getPlayerStat(i);
            
            this.nameLabels[i].textComponent.text = "Name: " + playerStat.name;
            this.nameLabels[i].setActive(true);
            
            this.difficultyLabels[i].textComponent.text = "Difficulty: " + playerStat.difficulty;
            this.difficultyLabels[i].setActive(true);
            
            this.operatorLabels[i].textComponent.text = "Operators: " + playerStat.operators;
            this.operatorLabels[i].setActive(true);
            
            this.levelLabels[i].textComponent.text = "Level: " + playerStat.level;
            this.levelLabels[i].setActive(true);
            
            this.rankLabels[i].textComponent.text = "Highest Rank: " + playerStat.rank;
            this.rankLabels[i].setActive(true);
            
            this.levelsCompletedLabels[i].textComponent.text = "Levels Completed: " + playerStat.levelsCompleted;
            this.levelsCompletedLabels[i].setActive(true);
        }
        else
        {
            this.noDataLabels[i].setActive(true);
        }
        
        this.slots[i].setActive(true);
        this.slotLabels[i].setActive(true);
    }
};

LoadGameMenu.prototype._getSlotIndex = function(slot)
{    
    return this.slots.indexOf(slot);
};
