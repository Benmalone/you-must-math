function NewOptions(game)
{
    this.game = game;
	
    this.previousMenu = startMenu;
	
	this._setupArrowButtons();
    this._setupBackButton();
    
    this.openTime;
    
    this.close();
}
NewOptions.prototype._setupArrowButtons = function()
{
	var self = this;
	var indexOptions = 0;
	var volumeLabelMusic = 100;
	var volumeValueMusic = 1;
	var volumeLabelSE = 100;
	var volumeValueSE = 1;
	
	this.musicLabel = this.game.create.image(game.width / 3.45, game.height - 790, 220, 333, SOUND_MUSIC_LABEL_SRC);
	this.musicBox = this.game.create.image(game.width / 3.25, game.height / 1.8, 160, 90, VOLUME_BOX_SRC);
    this.volumeMusic = this.game.create.label(this.game.width - 835, this.game.height / 1.7, volumeLabelMusic, "sans-serif", 42, "white");	
	
    this.increaseMusic = this.game.create.image(0, 0, 167, 184, SOUND_UP_BUTTON_SRC);
    this.increaseMusic.transform.centerX = this.game.width / 2.7;
    this.increaseMusic.transform.bottom = this.game.height / 1.8;
    
	this.increaseMusic.events.up.push(function() {
        this.imageComponent.setImage(SOUND_UP_BUTTON_SRC);
    });
	this.increaseMusic.events.down.push(function() {
        this.imageComponent.setImage(SOUND_UP_BUTTON_INVERT_SRC);
    });
    
    this.increaseMusic.events.click.push(function() {
		indexOptions = 0;
		sndButton.play();
		if(volumeLabelMusic != 100)
		{
			volumeValueMusic = volumeValueMusic + .1;
			soundNavigation.volume = volumeValueMusic;
			soundBattle.volume = volumeValueMusic;
			soundOpening.volume = volumeValueMusic;
			endCutscene.volume = volumeValueMusic;
			volumeLabelMusic = volumeLabelMusic + 10;
			self.volumeMusic.textComponent.text = volumeLabelMusic;
		}
    });
    
    this.decreaseMusic = this.game.create.image(0, 0, 100, 213, SOUND_DOWN_BUTTON_SRC);
    this.decreaseMusic.transform.centerX = this.game.width / 2.7;
    this.decreaseMusic.transform.centerY = this.game.height / 1.21;
    
	this.decreaseMusic.events.up.push(function() {
        this.imageComponent.setImage(SOUND_DOWN_BUTTON_SRC);
    });
	this.decreaseMusic.events.down.push(function() {
        this.imageComponent.setImage(SOUND_DOWN_BUTTON_INVERT_SRC);
    });
    
    this.decreaseMusic.events.click.push(function() {
		indexOptions = 1;
		sndButton.play();
		if(volumeLabelMusic != 0)
		{
			volumeValueMusic = volumeValueMusic - .1;
			soundNavigation.volume = volumeValueMusic;
			soundBattle.volume = volumeValueMusic;
			soundOpening.volume = volumeValueMusic;
			endCutscene.volume = volumeValueMusic;
			volumeLabelMusic = volumeLabelMusic - 10;
			self.volumeMusic.textComponent.text = volumeLabelMusic;
		}
    });
	
	this.seLabel = this.game.create.image(game.width / 1.83, game.height - 790, 226, 333, SOUND_EFFECTS_LABEL_SRC);
	this.seBox = this.game.create.image(game.width / 1.75, game.height / 1.8, 160, 90, VOLUME_BOX_SRC);
	this.volumeSE = this.game.create.label(this.game.width - 502, this.game.height / 1.7, volumeLabelSE, "sans-serif", 42, "white");	
	
	this.increaseSoundEffects = this.game.create.image(0, 0, 167, 184, SOUND_UP_BUTTON_SRC);
    this.increaseSoundEffects.transform.centerX = this.game.width / 1.58;
    this.increaseSoundEffects.transform.bottom = this.game.height / 1.8;
    
	this.increaseSoundEffects.events.up.push(function() {
        this.imageComponent.setImage(SOUND_UP_BUTTON_SRC);
    });
	this.increaseSoundEffects.events.down.push(function() {
        this.imageComponent.setImage(SOUND_UP_BUTTON_INVERT_SRC);
    });
    
    this.increaseSoundEffects.events.click.push(function() {
		indexOptions = 2;		
		sndButton.play();
		if(volumeLabelSE != 100)
		{
			volumeValueSE = volumeValueSE + .1;	
			sndPotion.volume = volumeValueSE;
			sndDamage.volume = volumeValueSE;
			sndDeath.volume = volumeValueSE;	
			sndEnemyDeath.volume = volumeValueSE;	
			sndButton.volume = volumeValueSE;
			volumeLabelSE = volumeLabelSE + 10;
			self.volumeSE.textComponent.text = volumeLabelSE;
		}
    });
    
    this.decreaseSoundEffects = this.game.create.image(0, 0, 100, 213, SOUND_DOWN_BUTTON_SRC);
    this.decreaseSoundEffects.transform.centerX = this.game.width / 1.58;
    this.decreaseSoundEffects.transform.centerY = this.game.height / 1.21;
    
	this.decreaseSoundEffects.events.up.push(function() {
        this.imageComponent.setImage(SOUND_DOWN_BUTTON_SRC);
    });
	this.decreaseSoundEffects.events.down.push(function() {
        this.imageComponent.setImage(SOUND_DOWN_BUTTON_INVERT_SRC);
    });
    
    this.decreaseSoundEffects.events.click.push(function() {
		indexOptions = 3;
		sndButton.play();
		if(volumeLabelSE != 0)
		{
			volumeValueSE = volumeValueSE - .1;
			sndPotion.volume = volumeValueSE;
			sndDamage.volume = volumeValueSE;
			sndDeath.volume = volumeValueSE;	
			sndEnemyDeath.volume = volumeValueSE;	
			sndButton.volume = volumeValueSE;
			volumeLabelSE = volumeLabelSE - 10;
			self.volumeSE.textComponent.text = volumeLabelSE;
		}
    });
	
	//Keypress
	var indexOptions = 0;
	window.addEventListener("keydown", function(e){    
		//up
		if((e.which == 38 || e.which == 87) && newOptions.backButton.isEnabled)
		{
			if(indexOptions == 0 || indexOptions == 1)
			{
				sndButton.play();
				if(volumeLabelMusic != 100)
				{
					volumeValueMusic = volumeValueMusic + .1;
					soundNavigation.volume = volumeValueMusic;
					soundBattle.volume = volumeValueMusic;
					soundOpening.volume = volumeValueMusic;
					volumeLabelMusic = volumeLabelMusic + 10;
					self.volumeMusic.textComponent.text = volumeLabelMusic;
				}
			}
			if(indexOptions == 1)
			{
				indexOptions--;
			}
			if(indexOptions == 2 || indexOptions == 3)
			{	
				sndButton.play();
				if(volumeLabelSE != 100)
				{
					volumeValueSE = volumeValueSE + .1;	
					sndPotion.volume = volumeValueSE;
					sndDamage.volume = volumeValueSE;
					sndDeath.volume = volumeValueSE;	
					sndEnemyDeath.volume = volumeValueSE;	
					sndButton.volume = volumeValueSE;
					volumeLabelSE = volumeLabelSE + 10;
					self.volumeSE.textComponent.text = volumeLabelSE;
				}
			}
			if(indexOptions == 3)
			{
				indexOptions--;
			}
		}
		//down
		if((e.which == 40 || e.which == 83) && newOptions.backButton.isEnabled)
		{
			if(indexOptions == 0 || indexOptions == 1)
			{
				sndButton.play();
				if(volumeLabelMusic != 0)
				{
					volumeValueMusic = volumeValueMusic - .1;
					soundNavigation.volume = volumeValueMusic;
					soundBattle.volume = volumeValueMusic;
					soundOpening.volume = volumeValueMusic;
					volumeLabelMusic = volumeLabelMusic - 10;
					self.volumeMusic.textComponent.text = volumeLabelMusic;
				}
			}
			if(indexOptions == 0)
			{
				indexOptions++;
			}
			if(indexOptions == 2 || indexOptions == 3)
			{
				sndButton.play();
				if(volumeLabelSE != 0)
				{
					volumeValueSE = volumeValueSE - .1;
					sndPotion.volume = volumeValueSE;
					sndDamage.volume = volumeValueSE;
					sndDeath.volume = volumeValueSE;	
					sndEnemyDeath.volume = volumeValueSE;	
					sndButton.volume = volumeValueSE;
					volumeLabelSE = volumeLabelSE - 10;
					self.volumeSE.textComponent.text = volumeLabelSE;
				}
			}
			if(indexOptions == 2)
			{
				indexOptions++;
			}
		}
		//left
		if((e.which == 37 || e.which == 65) && newOptions.backButton.isEnabled)
		{
			if(indexOptions == 0 || indexOptions == 1)
			{
				indexOptions = indexOptions + 2;
				newOptions.increaseMusic.imageComponent.setImage(SOUND_UP_BUTTON_SRC);
				newOptions.decreaseMusic.imageComponent.setImage(SOUND_DOWN_BUTTON_SRC);
			}
			else if(indexOptions == 2 || indexOptions == 3)
			{
				indexOptions = 4;
				newOptions.backButton.imageComponent.setImage(BACK_BUTTON_INVERT_SRC);
				newOptions.increaseSoundEffects.imageComponent.setImage(SOUND_UP_BUTTON_SRC);
				newOptions.decreaseSoundEffects.imageComponent.setImage(SOUND_DOWN_BUTTON_SRC);
			}
			else if(indexOptions == 4)
			{
				indexOptions = 1;
				newOptions.decreaseMusic.imageComponent.setImage(SOUND_DOWN_BUTTON_INVERT_SRC);
				newOptions.backButton.imageComponent.setImage(BACK_BUTTON_SRC);
			}
		}
		//right
		if((e.which == 39 || e.which == 68) && newOptions.backButton.isEnabled)
		{
			if(indexOptions == 0 || indexOptions == 1)
			{
				indexOptions = 4;
				newOptions.backButton.imageComponent.setImage(BACK_BUTTON_INVERT_SRC);
				newOptions.increaseMusic.imageComponent.setImage(SOUND_UP_BUTTON_SRC);
				newOptions.decreaseMusic.imageComponent.setImage(SOUND_DOWN_BUTTON_SRC);
			}
			else if(indexOptions == 2 || indexOptions == 3)
			{
				indexOptions = indexOptions - 2;
			}
			else if(indexOptions == 4)
			{
				indexOptions--;
				newOptions.decreaseSoundEffects.imageComponent.setImage(SOUND_DOWN_BUTTON_INVERT_SRC);
				newOptions.backButton.imageComponent.setImage(BACK_BUTTON_SRC);
			}
		}
		
		if(indexOptions == 0)
		{
			newOptions.increaseMusic.imageComponent.setImage(SOUND_UP_BUTTON_INVERT_SRC);
			newOptions.increaseSoundEffects.imageComponent.setImage(SOUND_UP_BUTTON_SRC);
			newOptions.decreaseMusic.imageComponent.setImage(SOUND_DOWN_BUTTON_SRC);
			newOptions.decreaseSoundEffects.imageComponent.setImage(SOUND_DOWN_BUTTON_SRC);
		}
		if(indexOptions == 1)
		{
			newOptions.increaseMusic.imageComponent.setImage(SOUND_UP_BUTTON_SRC);
			newOptions.increaseSoundEffects.imageComponent.setImage(SOUND_UP_BUTTON_SRC);
			newOptions.decreaseMusic.imageComponent.setImage(SOUND_DOWN_BUTTON_INVERT_SRC);
			newOptions.decreaseSoundEffects.imageComponent.setImage(SOUND_DOWN_BUTTON_SRC);
		}
		if(indexOptions == 2)
		{
			newOptions.increaseMusic.imageComponent.setImage(SOUND_UP_BUTTON_SRC);
			newOptions.increaseSoundEffects.imageComponent.setImage(SOUND_UP_BUTTON_INVERT_SRC);
			newOptions.decreaseMusic.imageComponent.setImage(SOUND_DOWN_BUTTON_SRC);
			newOptions.decreaseSoundEffects.imageComponent.setImage(SOUND_DOWN_BUTTON_SRC);
		}
		if(indexOptions == 3)
		{
			newOptions.increaseMusic.imageComponent.setImage(SOUND_UP_BUTTON_SRC);
			newOptions.increaseSoundEffects.imageComponent.setImage(SOUND_UP_BUTTON_SRC);
			newOptions.decreaseMusic.imageComponent.setImage(SOUND_DOWN_BUTTON_SRC);
			newOptions.decreaseSoundEffects.imageComponent.setImage(SOUND_DOWN_BUTTON_INVERT_SRC);
		}
    });
	window.addEventListener("keyup", function(e){ 
		if(e.which == 13 && newOptions.backButton.isEnabled && indexOptions == 4)
		{
			self.close();
			self.previousMenu.open();			
		}
	});
	
};
NewOptions.prototype._setupBackButton = function()
{
    this._createAndPositionBackButton();
    this._setupBackButtonEvents();
};
NewOptions.prototype._createAndPositionBackButton = function()
{
    this.backButton = this.game.create.image(0, 0, 200, 80, BACK_BUTTON_SRC);
    this.backButton.transform.pivotX = 0.5;
    this.backButton.transform.pivotY = 0.5;
    this.backButton.transform.centerX = this.game.width / 2;
    this.backButton.transform.bottom = this.game.height - 10;
    this.backButton.transform.z = -1;
};
NewOptions.prototype._setupBackButtonEvents = function()
{
    var self = this;
    
    this.backButton.events.over.push(function()
    {
        self.backButton.transform.scaleX = 1.1;
        self.backButton.transform.scaleY = 1.1;
    });
    
    this.backButton.events.out.push(function()
    {
        self.backButton.transform.scaleX = 1;
        self.backButton.transform.scaleY = 1;
    });
    
    this.backButton.events.down.push(function()
    {
        self.backButton.imageComponent.setImage(BACK_BUTTON_INVERT_SRC);
    });
    
    this.backButton.events.up.push(function()
    {
        self.backButton.imageComponent.setImage(BACK_BUTTON_SRC);
    });
    
    this.backButton.events.click.push(function()
    {
        self.close();
        self.previousMenu.open();
    });
	window.addEventListener("keyup", function(e){    
		if(e.which == 27 && newOptions.backButton.isEnabled)
		{
			self.close();
			self.previousMenu.open();
		}
    });
};

NewOptions.prototype.close = function()
{
    this.backButton.setActive(false);
	this.musicBox.setActive(false);
	this.seBox.setActive(false);
	this.musicLabel.setActive(false);
	this.seLabel.setActive(false);
    this.increaseMusic.setActive(false);
    this.decreaseMusic.setActive(false);
    this.increaseSoundEffects.setActive(false);
    this.decreaseSoundEffects.setActive(false);
    this.volumeMusic.setActive(false);
    this.volumeSE.setActive(false);
    
    if (this.previousMenu == navMenu)
    {
        this.previousMenu.startTime = this.previousMenu.startTime + Math.round((Date.now() - this.openTime) / 1000);
    }
};

NewOptions.prototype.open = function(previousMenu)
{
    if (previousMenu != null) 
    {
        this.previousMenu = previousMenu;
    }
    else
    {
        this.previousMenu = startMenu;
    }
    
    if (previousMenu == navMenu)
    {
        this.openTime = Date.now();
    }
    
    background.setActive(true);
    
    this.backButton.setActive(true);
	this.musicBox.setActive(true);
	this.seBox.setActive(true);
	this.musicLabel.setActive(true);
	this.seLabel.setActive(true);
    this.increaseMusic.setActive(true);
    this.decreaseMusic.setActive(true);
    this.increaseSoundEffects.setActive(true);
    this.decreaseSoundEffects.setActive(true);
    this.volumeMusic.setActive(true);
    this.volumeSE.setActive(true);
};