function StartMenu(game) 
{
    this.game = game;
    
    this.newGameMenu;
    this.loadGameMenu;
    this.loadCredits;
    this.newOptions;
    
    this._setupNewGameButton();
    this._setupLoadGameButton();
    this._setupTutorialButton();
    this._setupOptionsButton();
    this._setupCreditsButton();
    this._setupKeys();
}

StartMenu.prototype._setupNewGameButton = function()
{
    var self = this;
    
	soundOpening.loop = true;
    soundOpening.play();
    this.signPic = this.game.create.image(game.width / 3.3, 0, 502, 346, SIGN_SRC);
	
    this.newButton = this.game.create.image(0, 0, 280, 80, NEW_BUTTON_SRC);
    this.newButton.transform.pivotX = 0.5;
    this.newButton.transform.pivotY = 0.5;
    this.newButton.transform.centerX = this.game.width / 2.57;
    this.newButton.transform.centerY = this.game.height / 1.7;
	
    this.newButton.events.over.push(function()
    {
        self.newButton.transform.scaleX = 1.1;
        self.newButton.transform.scaleY = 1.1;
    });
    
    this.newButton.events.out.push(function()
    {
        self.newButton.transform.scaleX = 1;
        self.newButton.transform.scaleY = 1;
    });
    
    
    this.newButton.events.down.push(function()
    {
        self.newButton.imageComponent.setImage(NEW_BUTTON_INVERT_SRC);
    });
    
    this.newButton.events.up.push(function()
    {
        self.newButton.imageComponent.setImage(NEW_BUTTON_SRC);
    });
    
    this.newButton.events.click.push(function()
    {
        self.close();
        self.newGameMenu.open();
    });
	window.addEventListener("keydown", function(e){    
		if(e.which == 78 && startMenu.newButton.isEnabled)
		{
			self.close();
			self.newGameMenu.open();
		}
    }); 
};

StartMenu.prototype._setupLoadGameButton = function() 
{
    var self = this;
    
    this.loadButton = this.game.create.image(0, 0, 280, 80, LOAD_BUTTON_SRC);
    this.loadButton.transform.pivotX = 0.5;
    this.loadButton.transform.pivotY = 0.5;
    this.loadButton.transform.centerX = this.game.width / 1.61;
    this.loadButton.transform.centerY = this.game.height / 1.7;
    
    this.loadButton.events.over.push(function()
    {
        self.loadButton.transform.scaleX = 1.1;
        self.loadButton.transform.scaleY = 1.1;
    });
    
    this.loadButton.events.out.push(function()
    {
        self.loadButton.transform.scaleX = 1;
        self.loadButton.transform.scaleY = 1;
    });
    
    this.loadButton.events.down.push(function()
    {
        self.loadButton.imageComponent.setImage(LOAD_BUTTON_INVERT_SRC);
    });
    
    this.loadButton.events.up.push(function()
    {
        self.loadButton.imageComponent.setImage(LOAD_BUTTON_SRC);
    });
    
    this.loadButton.events.click.push(function()
    {
        self.close();
        self.loadGameMenu.open();
    });
	window.addEventListener("keydown", function(e){    
		if(e.which == 76 && startMenu.loadButton.isEnabled)
		{
			self.close();
			self.loadGameMenu.open();
		}
    });
};

StartMenu.prototype._setupTutorialButton = function() 
{
    var self = this;
    
    this.tutorialButton = this.game.create.image(0, 0, 280, 80, TUTORIAL_BUTTON_SRC);
    this.tutorialButton.transform.pivotX = 0.5;
    this.tutorialButton.transform.pivotY = 0.5;
    this.tutorialButton.transform.centerX = this.newButton.transform.centerX;
    this.tutorialButton.transform.top = this.newButton.transform.bottom + this.newButton.transform.height / 2.8;
    
    this.tutorialButton.events.over.push(function()
    {
        self.tutorialButton.transform.scaleX = 1.1;
        self.tutorialButton.transform.scaleY = 1.1;
    });
    
    this.tutorialButton.events.out.push(function()
    {
        self.tutorialButton.transform.scaleX = 1;
        self.tutorialButton.transform.scaleY = 1;
    });
    
    this.tutorialButton.events.down.push(function()
    {
        self.tutorialButton.imageComponent.setImage(TUTORIAL_BUTTON_INVERT_SRC);
    });
    
    this.tutorialButton.events.up.push(function()
    {
        self.tutorialButton.imageComponent.setImage(TUTORIAL_BUTTON_SRC);
    });
    
    this.tutorialButton.events.click.push(function()
    {
        self.close();
        tutorialMenu.open();
    });
	window.addEventListener("keydown", function(e){    
		if(e.which == 84 && startMenu.tutorialButton.isEnabled)
		{
			self.close();
			tutorialMenu.open();
		}
    });
};

StartMenu.prototype._setupOptionsButton = function() 
{
    var self = this;
    
    this.optionsButton = this.game.create.image(0, 0, 280, 80, OPTIONS_BUTTON_SRC);
    this.optionsButton.transform.pivotX = 0.5;
    this.optionsButton.transform.pivotY = 0.5;
    this.optionsButton.transform.centerX = this.loadButton.transform.centerX;
    this.optionsButton.transform.top = this.loadButton.transform.bottom + this.loadButton.transform.height / 2.8;
    
    this.optionsButton.events.over.push(function()
    {
        self.optionsButton.transform.scaleX = 1.1;
        self.optionsButton.transform.scaleY = 1.1;
    });
    
    this.optionsButton.events.out.push(function()
    {
        self.optionsButton.transform.scaleX = 1;
        self.optionsButton.transform.scaleY = 1;
    });
    
    this.optionsButton.events.down.push(function()
    {
        self.optionsButton.imageComponent.setImage(OPTIONS_BUTTON_INVERT_SRC);
    });
    
    this.optionsButton.events.up.push(function()
    {
        self.optionsButton.imageComponent.setImage(OPTIONS_BUTTON_SRC);
    });
    
    this.optionsButton.events.click.push(function()
    {
        self.close();
        self.newOptions.open();
    });
	window.addEventListener("keydown", function(e){    
		if(e.which == 79 && startMenu.optionsButton.isEnabled)
		{
			self.close();
			self.newOptions.open();
		}
    });
};

StartMenu.prototype._setupCreditsButton = function() 
{
    var self = this;
    
    this.creditsButton = this.game.create.image(0, 0, 280, 80, CREDITS_BUTTON_SRC);
    this.creditsButton.transform.pivotX = 0.5;
    this.creditsButton.transform.pivotY = 0.5;
    this.creditsButton.transform.centerX = this.optionsButton.transform.centerX / 1.25;
    this.creditsButton.transform.top = this.optionsButton.transform.bottom + this.optionsButton.transform.height / 2.9;
    
    this.creditsButton.events.over.push(function()
    {
        self.creditsButton.transform.scaleX = 1.1;
        self.creditsButton.transform.scaleY = 1.1;
    });
    
    this.creditsButton.events.out.push(function()
    {
        self.creditsButton.transform.scaleX = 1;
        self.creditsButton.transform.scaleY = 1;
    });
    
    this.creditsButton.events.down.push(function()
    {
        self.creditsButton.imageComponent.setImage(CREDITS_BUTTON_INVERT_SRC);
    });
    
    this.creditsButton.events.up.push(function()
    {
        self.creditsButton.imageComponent.setImage(CREDITS_BUTTON_SRC);
    });
    
    this.creditsButton.events.click.push(function()
    {
        self.close();
        self.loadCredits.open();
    });
	window.addEventListener("keydown", function(e){    
		if(e.which == 67 && startMenu.creditsButton.isEnabled)
		{
			self.close();
			self.loadCredits.open();
		}
    });
};

StartMenu.prototype._setupKeys = function() 
{
	var self = this;
	var indexStart = 0;
	
	this.newButton.transform.scaleX = 1.1;
	this.newButton.transform.scaleY = 1.1;

	window.addEventListener("keydown", function(e){    
	//Using Up Arrow and W
		if((e.which == 87 || e.which == 38) && startMenu.creditsButton.isEnabled)
		{
			if(indexStart == 0 || indexStart == 1)
			{
				indexStart--;
				startMenu.newButton.transform.scaleX = 1;
				startMenu.newButton.transform.scaleY = 1;
				startMenu.loadButton.transform.scaleX = 1;
				startMenu.loadButton.transform.scaleY = 1;
				startMenu.creditsButton.transform.scaleX = 1.1;
				startMenu.creditsButton.transform.scaleY = 1.1;
			}
			else if(indexStart == 2)
			{				
				indexStart = indexStart - 2;
				startMenu.tutorialButton.transform.scaleX = 1;
				startMenu.tutorialButton.transform.scaleY = 1;
				startMenu.newButton.transform.scaleX = 1.1;
				startMenu.newButton.transform.scaleY = 1.1;
			}
			else if(indexStart == 3)
			{
				indexStart = indexStart - 2;
				startMenu.optionsButton.transform.scaleX = 1;
				startMenu.optionsButton.transform.scaleY = 1;
				startMenu.loadButton.transform.scaleX = 1.1;
				startMenu.loadButton.transform.scaleY = 1.1;
			}
			else if(indexStart == 4)
			{
				indexStart = indexStart - 2;
				startMenu.creditsButton.transform.scaleX = 1;
				startMenu.creditsButton.transform.scaleY = 1;
				startMenu.tutorialButton.transform.scaleX = 1.1;
				startMenu.tutorialButton.transform.scaleY = 1.1;				
			}
			if(indexStart <= -1)
			{
				indexStart = 4;
			}
		} 
	//Using Left & Right Arrow and A & D
		if((e.which == 65 || e.which == 37 || e.which == 68 || e.which == 39) && startMenu.creditsButton.isEnabled)
		{
			if(indexStart == 0)
			{
				indexStart++;
				startMenu.newButton.transform.scaleX = 1;
				startMenu.newButton.transform.scaleY = 1;
				startMenu.loadButton.transform.scaleX = 1.1;
				startMenu.loadButton.transform.scaleY = 1.1;
			}
			else if(indexStart == 1)
			{
				indexStart--;
				startMenu.loadButton.transform.scaleX = 1;
				startMenu.loadButton.transform.scaleY = 1;
				startMenu.newButton.transform.scaleX = 1.1;
				startMenu.newButton.transform.scaleY = 1.1;				
			}
			else if(indexStart == 2)
			{				
				indexStart++;
				startMenu.tutorialButton.transform.scaleX = 1;
				startMenu.tutorialButton.transform.scaleY = 1;
				startMenu.optionsButton.transform.scaleX = 1.1;
				startMenu.optionsButton.transform.scaleY = 1.1;
			}
			else if(indexStart == 3)
			{
				indexStart--;
				startMenu.optionsButton.transform.scaleX = 1;
				startMenu.optionsButton.transform.scaleY = 1;
				startMenu.tutorialButton.transform.scaleX = 1.1;
				startMenu.tutorialButton.transform.scaleY = 1.1;
			}
		} 
		if((e.which == 65 || e.which == 37) && startMenu.creditsButton.isEnabled)
		{
			if(indexStart == 4)
			{
				indexStart = indexStart - 2;
				startMenu.creditsButton.transform.scaleX = 1;
				startMenu.creditsButton.transform.scaleY = 1;
				startMenu.tutorialButton.transform.scaleX = 1.1;
				startMenu.tutorialButton.transform.scaleY = 1.1;
			}
		}
		if((e.which == 68 || e.which == 39) && startMenu.creditsButton.isEnabled)
		{
			if(indexStart == 4)
			{
				indexStart--;
				startMenu.creditsButton.transform.scaleX = 1;
				startMenu.creditsButton.transform.scaleY = 1;
				startMenu.optionsButton.transform.scaleX = 1.1;
				startMenu.optionsButton.transform.scaleY = 1.1;
			}
		}
	//Using Down Arrow and S
		if((e.which == 83 || e.which == 40) && startMenu.creditsButton.isEnabled)
		{
			if(indexStart == 0)
			{
				indexStart = indexStart + 2;
				startMenu.newButton.transform.scaleX = 1;
				startMenu.newButton.transform.scaleY = 1;
				startMenu.tutorialButton.transform.scaleX = 1.1;
				startMenu.tutorialButton.transform.scaleY = 1.1;
			}
			else if(indexStart == 1)
			{				
				indexStart = indexStart + 2;
				startMenu.loadButton.transform.scaleX = 1;
				startMenu.loadButton.transform.scaleY = 1;
				startMenu.optionsButton.transform.scaleX = 1.1;
				startMenu.optionsButton.transform.scaleY = 1.1;
			}
			else if(indexStart == 2)
			{				
				indexStart = indexStart + 2;
				startMenu.tutorialButton.transform.scaleX = 1;
				startMenu.tutorialButton.transform.scaleY = 1;
				startMenu.creditsButton.transform.scaleX = 1.1;
				startMenu.creditsButton.transform.scaleY = 1.1;
			}
			else if(indexStart == 3)
			{
				indexStart++;
				startMenu.optionsButton.transform.scaleX = 1;
				startMenu.optionsButton.transform.scaleY = 1;
				startMenu.creditsButton.transform.scaleX = 1.1;
				startMenu.creditsButton.transform.scaleY = 1.1;
			}
			else if(indexStart == 4)
			{
				indexStart++;
				startMenu.creditsButton.transform.scaleX = 1;
				startMenu.creditsButton.transform.scaleY = 1;
				startMenu.newButton.transform.scaleX = 1.1;
				startMenu.newButton.transform.scaleY = 1.1;				
			}
			if(indexStart >= 5)
			{
				indexStart = 0;
			}
		}
	});
	//PressingEnter
	window.addEventListener("keyup", function(e){    
		if(e.which == 13 && startMenu.creditsButton.isEnabled)
		{
			if(indexStart == 0)
			{
				setTimeout(function(){
					self.close();
					startMenu.newGameMenu.open();
				},10);
			}
			else if(indexStart == 1)
			{
				setTimeout(function(){
					self.close();
					startMenu.loadGameMenu.open();	
				},10);								
			}
			else if(indexStart == 2)
			{
				setTimeout(function(){
					self.close();
					tutorialMenu.open();
				},10);					
			}
			else if(indexStart == 3)
			{
				setTimeout(function(){
				self.close();
				startMenu.newOptions.open();
				},10);
			}
			else if(indexStart == 4)
			{
				setTimeout(function(){
					self.close();
					startMenu.loadCredits.open();
				},10);	
			}
		}
    });
	
}

StartMenu.prototype.close = function()
{
	this.signPic.setActive(false);
    this.newButton.setActive(false);
    this.loadButton.setActive(false);
    this.tutorialButton.setActive(false);
    this.optionsButton.setActive(false);
    this.creditsButton.setActive(false);
};

StartMenu.prototype.open = function()
{    
	this.signPic.setActive(true);
    this.newButton.setActive(true);
    this.loadButton.setActive(true);
    this.tutorialButton.setActive(true);
    this.optionsButton.setActive(true);
    this.creditsButton.setActive(true);
};