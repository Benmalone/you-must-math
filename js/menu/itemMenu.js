function ItemMenu(game)
{
    this.game = game;
    
    this.foundItemBox;
    this.switchItemBox;
    this.keepButton;
    this.switchButton;
    this.foundItemImage;
    this.currentItemImage;
    
    this.openedChest;
    this.foundItem;
    
    this._createFoundItemBox();
    this._createSwitchItemBox();
    this._createFoundItem();
    this._setupKeys();
    
    this.canBeOpened = true;
    
    this.close();
}

ItemMenu.prototype._createFoundItemBox = function()
{
    this.foundItemBox = this.game.create.image(0, 0, 400, 300, NEW_ITEM_PANEL_SRC);
    this.foundItemBox.transform.centerX = 480;
    this.foundItemBox.transform.centerY = 270;
	
};

ItemMenu.prototype._createFoundItem = function()
{
    this.foundItemImage = this.game.create.image(0, 0, Item.POTION_WIDTH, Item.POTION_HEIGHT, RED_POTION_SRC);
    this.foundItemImage.transform.z = this.foundItemBox.transform.z - 1;
};

ItemMenu.prototype._createSwitchItemBox = function()
{
    this.switchItemBox = this.game.create.image(0, 0, 550, 350, SWITCH_ITEM_PANEL_SRC);
    this.switchItemBox.transform.centerX = 480;
    this.switchItemBox.transform.centerY = 270;
    
    this._createKeepButton();
    this._createSwitchButton();
    this._createcurrentItemImage();
};

ItemMenu.prototype._createKeepButton = function()
{
    this.keepButton = this.game.create.image(0, 0, 220, 60, KEEP_ITEM_BUTTON_SRC);
    this.keepButton.transform.bottom = this.switchItemBox.transform.bottom - 40;
    this.keepButton.transform.left = this.switchItemBox.transform.left + 40;
    this.keepButton.transform.z = this.switchItemBox.transform.z - 1;
    
    var self = this;
    
    this.keepButton.events.out.push(function() {
        this.imageComponent.setImage(KEEP_ITEM_BUTTON_SRC);
    });
    
    this.keepButton.events.over.push(function() {
        this.imageComponent.setImage(KEEP_ITEM_BUTTON_SRC);
        itemMenu.switchButton.imageComponent.setImage(SWITCH_OUT_BUTTON_DARK_SRC);
    });
	
    this.keepButton.events.down.push(function() {
        this.imageComponent.setImage(KEEP_ITEM_BUTTON_INVERT_SRC);
        itemMenu.switchButton.imageComponent.setImage(SWITCH_OUT_BUTTON_DARK_SRC);
    });
    
    this.keepButton.events.up.push(function() {
        this.imageComponent.setImage(KEEP_ITEM_BUTTON_SRC);
    });
	
    this.keepButton.events.click.push(function()
    {
        self.openedChest.store(self.foundItem);
        self.close();
        
        setTimeout(function()
        {
            self.canBeOpened = true;
        }, 150);
    });
};

ItemMenu.prototype._createSwitchButton = function()
{    
    this.switchButton = this.game.create.image(0, 0, 220, 60, SWITCH_OUT_BUTTON_DARK_SRC);
    this.switchButton.transform.bottom = this.switchItemBox.transform.bottom - 40;
    this.switchButton.transform.right = this.switchItemBox.transform.right - 40;
    this.switchButton.transform.z = this.switchItemBox.transform.z - 1;
    
    var self = this;
    
    this.switchButton.events.out.push(function() {
        this.imageComponent.setImage(SWITCH_OUT_BUTTON_SRC);
    });
    
    this.switchButton.events.over.push(function() {
        this.imageComponent.setImage(SWITCH_OUT_BUTTON_SRC);
        itemMenu.keepButton.imageComponent.setImage(KEEP_ITEM_BUTTON_DARK_SRC);
    });
	
    this.switchButton.events.down.push(function() {
        this.imageComponent.setImage(SWITCH_OUT_BUTTON_INVERT_SRC);
        itemMenu.keepButton.imageComponent.setImage(KEEP_ITEM_BUTTON_DARK_SRC);
    });
    
    this.switchButton.events.up.push(function() {
        this.imageComponent.setImage(SWITCH_OUT_BUTTON_SRC);
    });
	
    this.switchButton.events.click.push(function()
    {
        self.openedChest.store(player.item);
        player.item = self.foundItem;
        self.close();
        
        setTimeout(function()
        {
            self.canBeOpened = true;
        }, 150);
    });
};
ItemMenu.prototype._setupKeys = function()
{  
	var indexItems = 0;
	var self = this;
	
	window.addEventListener("keydown", function(e){   
		if((e.which == 37 || e.which == 65 || e.which == 39 || e.which == 68) && itemMenu.switchButton.isEnabled)
		{
			indexItems++;
			if(indexItems >= 2)
			{
				indexItems = 0;
			}
			if(indexItems == 0)
			{			
				itemMenu.keepButton.imageComponent.setImage(KEEP_ITEM_BUTTON_SRC);
				itemMenu.switchButton.imageComponent.setImage(SWITCH_OUT_BUTTON_DARK_SRC);
			}
			else if(indexItems == 1)
			{
				itemMenu.keepButton.imageComponent.setImage(KEEP_ITEM_BUTTON_DARK_SRC);
				itemMenu.switchButton.imageComponent.setImage(SWITCH_OUT_BUTTON_SRC);
			}
		}
	});
	window.addEventListener("keyup", function(e){ 
		if(e.which == 13 && itemMenu.switchButton.isEnabled && navMenu.menuButton.isEnabled)
		{
			if(indexItems == 0)
			{
				self.openedChest.store(self.foundItem);
				self.close();
				
				setTimeout(function()
				{
					self.canBeOpened = true;
				}, 150);		
			}
			else if(indexItems == 1)
			{
				indexItems = 0;
				itemMenu.keepButton.imageComponent.setImage(KEEP_ITEM_BUTTON_SRC);
				itemMenu.switchButton.imageComponent.setImage(SWITCH_OUT_BUTTON_DARK_SRC);
				self.openedChest.store(player.item);
				player.item = self.foundItem;
				self.close();
				
				setTimeout(function()
				{
					self.canBeOpened = true;
				}, 150);
			}
		}
	});
}

ItemMenu.prototype._createcurrentItemImage = function()
{
    this.currentItemImage = this.game.create.image(0, 0, Item.POTION_WIDTH, Item.POTION_HEIGHT, RED_POTION_SRC);
    this.currentItemImage.transform.centerY = this.switchItemBox.transform.centerY;
    this.currentItemImage.transform.centerX = this.keepButton.transform.centerX;
    this.currentItemImage.transform.z = this.switchItemBox.transform.z - 1;
};

ItemMenu.prototype.close = function()
{
    this.foundItemBox.setActive(false);
    this.switchItemBox.setActive(false);
    this.keepButton.setActive(false);
    this.switchButton.setActive(false);
    this.foundItemImage.setActive(false);
    this.currentItemImage.setActive(false);
    
    
	this.foundItem = null;
};

ItemMenu.prototype.open = function(chest)
{
    this.canBeOpened = false;
    
    this.openedChest = chest;
    this.foundItem = this.openedChest.item;
    
    if (player.item == null)
    {
        this._openFoundItemBox();
    }
    else
    {
        this._openSwitchItemBox();
    }
};

ItemMenu.prototype._openFoundItemBox = function()
{
    this.foundItemBox.setActive(true);
    
    this.foundItemImage.setActive(true);
    this.foundItemImage.imageComponent.setImage(this.foundItem.src);
    this.foundItemImage.transform.centerX = this.foundItemBox.transform.centerX;
    this.foundItemImage.transform.centerY = this.foundItemBox.transform.centerY;
    
    player.item = this.foundItem;
    
    var self = this;
    
    setTimeout(function()
    {
        self.canBeOpened = true;
        self.close();
    }, 1000);
};

ItemMenu.prototype._openSwitchItemBox = function()
{
    this.switchItemBox.setActive(true);
    this.switchButton.setActive(true);
    this.keepButton.setActive(true);
    
    this.foundItemImage.setActive(true);
    this.foundItemImage.imageComponent.setImage(this.foundItem.src);
    this.foundItemImage.transform.centerX = this.switchButton.transform.centerX;
    this.foundItemImage.transform.centerY = this.switchItemBox.transform.centerY;
    
    this.currentItemImage.setActive(true);
    this.currentItemImage.imageComponent.setImage(player.item.src);
};