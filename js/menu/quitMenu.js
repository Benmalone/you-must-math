function QuitMenu(game) 
{
    this.game = game;
    this._createQuit();
    this.close();
}

QuitMenu.prototype._createQuit = function()
{
	var self = this;
	
    var indexQuit = 0;
	this.quitPanel = this.game.create.image(game.width / 4.5, game.height / 3.5, 400, 240, QUIT_PANEL_SRC);
	this.quitPanel.transform.z = -10;
    
	this.yesButton = this.game.create.image(game.width / 2.5, game.height / 2.3, 134, 72, YES_BUTTON_DARK_SRC);
	this.yesButton.transform.z = -11;
    
	this.noButton = this.game.create.image(game.width / 3.8, game.height / 2.3, 134, 72, NO_BUTTON_SRC);   
	this.noButton.transform.z = -11;
	
	this.yesButton.events.up.push(function()
    {
        this.imageComponent.setImage(YES_BUTTON_SRC);
    });
    
	this.yesButton.events.down.push(function()
    {
		indexQuit = 1;
        this.imageComponent.setImage(YES_BUTTON_INVERT_SRC);
        quitMenu.noButton.imageComponent.setImage(NO_BUTTON_DARK_SRC);
    });
    
    this.yesButton.events.click.push(function()
    {
		restart();
	});
	
	this.noButton.events.up.push(function()
    {
        this.imageComponent.setImage(NO_BUTTON_SRC);
    });
    
	this.noButton.events.down.push(function()
    {
		indexQuit = 0;
        this.imageComponent.setImage(NO_BUTTON_INVERT_SRC);
        quitMenu.yesButton.imageComponent.setImage(YES_BUTTON_DARK_SRC);
    });
    
    this.noButton.events.click.push(function()
    {
        if(navMenu.timeLabel.isEnabled)
		{
			navMenu.indexOptions = 0;
			quitMenu.noButton.imageComponent.setImage(NO_BUTTON_INVERT_SRC);
			quitMenu.yesButton.imageComponent.setImage(YES_BUTTON_DARK_SRC);
			navMenu.menuButton.setActive(true);
			navMenu.forwardButton.setActive(true);
			navMenu.leftButton.setActive(true);
			navMenu.rightButton.setActive(true);
            navMenu.optionsButton.setActive(true);
		} 
        else if (battleMenu.itemPanel.isEnabled)
		{
			navMenu.indexOptions = 0;
			quitMenu.noButton.setActive(true);
			quitMenu.yesButton.setActive(true);
			battleMenu.menuButtonB.setActive(true);
		}
        
        self.close();
	});

	window.addEventListener("keydown", function(e){    
		if((e.which == 68 || e.which == 39 || e.which == 65 || e.which == 37) && self.noButton.isEnabled)
		{
			indexQuit++;
			if(indexQuit >= 2)
			{
				indexQuit = 0;
			}
			if(indexQuit <= -1)
			{
				indexQuit = 1;
			}		
			if(indexQuit == 0)
			{
				self.yesButton.imageComponent.setImage(YES_BUTTON_DARK_SRC);
				self.noButton.imageComponent.setImage(NO_BUTTON_SRC);
			}
			if(indexQuit == 1)
			{
				self.yesButton.imageComponent.setImage(YES_BUTTON_SRC);
				self.noButton.imageComponent.setImage(NO_BUTTON_DARK_SRC);
			}	
		}			
	});
	window.addEventListener("keyup", function(e){   
		if(((e.which == 13 && indexQuit == 0) || e.which == 27 )&& quitMenu.quitPanel.isEnabled)
		{
			if(navMenu.timeLabel.isEnabled)
			{
				navMenu.menuButton.setActive(true);
				navMenu.forwardButton.setActive(true);
				navMenu.leftButton.setActive(true);
				navMenu.rightButton.setActive(true);
                navMenu.optionsButton.setActive(true);
			}
			if(battleMenu.itemPanel.isEnabled)
			{
				battleMenu.menuButtonB.setActive(true);
			}
            self.close();
		}
		if(e.which == 13 && quitMenu.quitPanel.isEnabled && indexQuit == 1)
		{
			restart();
			indexQuit = 0;
			self.yesButton.imageComponent.setImage(YES_BUTTON_DARK_SRC);
			self.noButton.imageComponent.setImage(NO_BUTTON_SRC);
		} 
	});
}

QuitMenu.prototype.close = function()
{
    this.quitPanel.setActive(false);
	this.yesButton.setActive(false);
	this.noButton.setActive(false);
};

QuitMenu.prototype.open = function()
{    
    this.quitPanel.setActive(true);
	this.yesButton.setActive(true);
	this.noButton.setActive(true);
};