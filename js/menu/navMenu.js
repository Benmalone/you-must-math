function NavMenu(game) 
{
    this.game = game;
    this.player;
        
    this._createPanels();
    this._createButtons();
    this._createTimeLabel();
	this._setupKeys();
    
    this.startTime = 0;
    
    this.disable();
}

NavMenu.prototype._createPanels = function()
{
    this.wordPanel = this.game.create.image(0, this.game.height - 180, 960, 180, WORD_PANEL_SRC);
    this.wordPanel.transform.z = 10;
    
    this.rightPanel = this.game.create.image(this.game.width - 320, this.game.height - 330, 320, 330, BOTTOM_RIGHT_PANEL_SRC);
    this.rightPanel.transform.z = 10;
    
	this.keyPanel = this.game.create.image(this.game.width - 320, 0, 320, 390, MAP_SRC);
    this.keyPanel.transform.z = 10;
};

NavMenu.prototype._createButtons = function()
{
    this._setupForwardArrowButton();
    this._setupLeftArrowButton();
    this._setupRightArrowButton();
    this._createMenuButtons();
};

NavMenu.prototype._createTimeLabel = function()
{
    this.timeLabel = this.game.create.label(this.game.width - 280, this.game.height - 290, "Time:", "sans-serif", 42, "white");
};

NavMenu.prototype._setupForwardArrowButton = function()
{
    var self = this;
    
    this.forwardButton = this.game.create.image(0, 0, 118, 88, FORWARD_ARROW_SRC);
    this.forwardButton.transform.x = (this.wordPanel.transform.width - this.forwardButton.transform.width) / 2;
    this.forwardButton.transform.y = this.wordPanel.transform.y + this.forwardButton.transform.height / 2;
    
	this.forwardButton.events.up.push(function() {
        this.imageComponent.setImage(FORWARD_ARROW_SRC);
    });
	this.forwardButton.events.down.push(function() {
        this.imageComponent.setImage(FORWARD_ARROW_INVERT_SRC);
    });
    
    this.forwardButton.events.click.push(function() {
        if (itemMenu.switchItemBox.isEnabled)
        {
            return;
        }
        
        var room = self.dungeon.getRoomFromPoint(self.player.x, self.player.y);
        
        var directionVector = Direction.getVector(self.player.direction);
        var nextPlayerX = self.player.x + directionVector.x;
        var nextPlayerY = self.player.y + directionVector.y;
        
        if (self.dungeon.canProgress(self.player.x, self.player.y, self.player.direction))
        {
            self.player.x += directionVector.x;
            self.player.y += directionVector.y;
            self.dungeon.setActiveRooms(self.player.x, self.player.y, self.player.direction);
            self.miniMap.setArrowDirection(self.player.direction);
            self.miniMap.setArrowPosition(self.player.x, self.player.y);
            self.dungeon.show();
            
            room = self.dungeon.getRoomFromPoint(self.player.x, self.player.y);
        }
        else if (room.chest != null && player.direction == room.chest.direction)
        {
            room.chest.loot();
        }
        else if (room == navMenu.dungeon.endRoom && player.direction == navMenu.dungeon.endRoom.door.direction)
        {
            var boss = EnemyFactory.createRandomBoss(this.game, player.level);
            this.game.remove(boss.dungeonSprite);
            self.disable();
            battleMenu.open(boss, self.player);
        }
        
        if (room.enemy != null)
        {
            self.disable();
            battleMenu.open(room.enemy, self.player);
            self.game.remove(room.enemy.dungeonSprite);
            room.enemy = null;
        }
        
    });
};

NavMenu.prototype._setupLeftArrowButton = function()
{
    var self = this;
    
    this.leftButton = this.game.create.image(0, 0, 151, 95, LEFT_ARROW_SRC);
    this.leftButton.transform.right = this.forwardButton.transform.left - 44;
    this.leftButton.transform.y = this.wordPanel.transform.y + this.leftButton.transform.height / 2;
    
	this.leftButton.events.up.push(function() {
        this.imageComponent.setImage(LEFT_ARROW_SRC);
    });
    
	this.leftButton.events.down.push(function() {
        this.imageComponent.setImage(LEFT_ARROW_INVERT_SRC);
    });
    
    this.leftButton.events.click.push(function() {
        if (itemMenu.switchItemBox.isEnabled)
        {
            return;
        }
        
        self._turnLeft.call(self);
        self.dungeon.setActiveRooms(self.player.x, self.player.y, self.player.direction);
        self.miniMap.setArrowDirection(self.player.direction);
        self.miniMap.setArrowPosition(self.player.x, self.player.y);
        self.dungeon.show();
    });
};

NavMenu.prototype._setupRightArrowButton = function()
{
    var self = this;
    
    this.rightButton = this.game.create.image(0, 0, 151, 95, RIGHT_ARROW_SRC);
    this.rightButton.transform.left = this.forwardButton.transform.right + 44;
    this.rightButton.transform.y = this.wordPanel.transform.y + this.rightButton.transform.height / 2;
    
	this.rightButton.events.up.push(function() {
        this.imageComponent.setImage(RIGHT_ARROW_SRC);
    });
    
	this.rightButton.events.down.push(function() {
        this.imageComponent.setImage(RIGHT_ARROW_INVERT_SRC);
    });
    
    this.rightButton.events.click.push(function() {
        if (itemMenu.switchItemBox.isEnabled)
        {
            return;
        }
        
        self._turnRight.call(self);
        self.dungeon.setActiveRooms(self.player.x, self.player.y, self.player.direction);
        self.miniMap.setArrowDirection(self.player.direction);
        self.miniMap.setArrowPosition(self.player.x, self.player.y);
        self.dungeon.show();
    });
};

NavMenu.prototype._turnLeft = function()
{
    switch(this.player.direction)
    {
		case Direction.UP:
            this.player.direction = Direction.LEFT;
			break;
		case Direction.RIGHT:
            this.player.direction = Direction.UP;
			break;
		case Direction.DOWN:
            this.player.direction = Direction.RIGHT;
			break;
		case Direction.LEFT:
            this.player.direction = Direction.DOWN;
			break;
	}
};

NavMenu.prototype._turnRight = function()
{
    switch(this.player.direction)
    {
		case Direction.UP:
            this.player.direction = Direction.RIGHT;
			break;
		case Direction.RIGHT:
            this.player.direction = Direction.DOWN;
			break;
		case Direction.DOWN:
            this.player.direction = Direction.LEFT;
			break;
		case Direction.LEFT:
            this.player.direction = Direction.UP;
			break;
	}
};

NavMenu.prototype._createMenuButtons = function() 
{	
	this._createQuitButton();
    this._createOptionsButton();
};

NavMenu.prototype._createQuitButton = function()
{
    var self = this;
    
	this.menuButton = this.game.create.image(0, 0, 175, 100, MENU_BUTTON_SRC);
	this.menuButton.transform.scaleX = 0.9;
    this.menuButton.transform.scaleY = 0.8;
    this.menuButton.transform.pivotX = 0.5;
    this.menuButton.transform.pivotY = 0.5;
    this.menuButton.transform.centerX = this.rightPanel.transform.centerX;
    this.menuButton.transform.bottom = this.rightPanel.transform.bottom - 30;
    
    this.menuButton.events.over.push(function()
    {
        self.menuButton.transform.scaleX = 1;
        self.menuButton.transform.scaleY = 0.9;
    });
    
    this.menuButton.events.out.push(function()
    {
        self.menuButton.transform.scaleX = 0.9;
        self.menuButton.transform.scaleY = 0.8;
    });
	
	this.menuButton.events.up.push(function()
    {
        this.imageComponent.setImage(MENU_BUTTON_SRC);
    });
    
	this.menuButton.events.down.push(function()
    {
        this.imageComponent.setImage(MENU_INVERT_BUTTON_SRC);
    });
	
	this.menuButton.events.click.push(function()
	{
		if (itemMenu.switchItemBox.isEnabled)
        {
            return;
        }
        
        self.menuButton.setActive(false);
		self.forwardButton.setActive(false);
		self.leftButton.setActive(false);
		self.rightButton.setActive(false);
        self.optionsButton.setActive(false);
		quitMenu.open();
	});
};
	
NavMenu.prototype._createOptionsButton = function() 
{	
	var self = this;
    
	this.optionsButton = this.game.create.image(0, 0, 175, 100, OPTIONS_BUTTON_SMALL_SRC);
    this.optionsButton.transform.pivotX = 0.5;
    this.optionsButton.transform.pivotY = 0.5;
	this.optionsButton.transform.scaleX = .9;
    this.optionsButton.transform.scaleY = .8;
    this.optionsButton.transform.centerX = this.menuButton.transform.centerX;
    this.optionsButton.transform.bottom = this.menuButton.transform.top - 40;
    
    this.optionsButton.events.over.push(function()
    {
        self.optionsButton.transform.scaleX = 1;
        self.optionsButton.transform.scaleY = 0.9;
    });
    
    this.optionsButton.events.out.push(function()
    {
        self.optionsButton.transform.scaleX = 0.9;
        self.optionsButton.transform.scaleY = 0.8;
    });
	
	this.optionsButton.events.up.push(function()
    {
        self.optionsButton.imageComponent.setImage(OPTIONS_BUTTON_SMALL_SRC);
    });
    
	this.optionsButton.events.down.push(function()
    {
        self.optionsButton.imageComponent.setImage(OPTIONS_BUTTON_SMALL_INVERT_SRC);
    });
	
	this.optionsButton.events.click.push(function()
	{
        if (itemMenu.switchItemBox.isEnabled)
        {
            return;
        }
        
        self.disable();
        newOptions.open(self);
	});
	window.addEventListener("keydown", function(e)
	{
		if(e.which == 79 && navMenu.optionsButton.isEnabled)
		{
			if (itemMenu.switchItemBox.isEnabled)
            {
                return;
            }
            
            self.disable();
			newOptions.open(self);
		}
	});
    
};
	
NavMenu.prototype._setupKeys = function() 
{
	var self = this;
	var indexOptions = 0;
	
	//Up Arrow or W	
	window.addEventListener("keyup", function(e){
		if((e.which == 38 || e.which == 87) && self.forwardButton.isEnabled && indexOptions == 0)
		{
			self.forwardButton.imageComponent.setImage(FORWARD_ARROW_SRC);
		}
	});
	window.addEventListener("keydown", function(e){
		if((e.which == 38 || e.which == 87) && self.forwardButton.isEnabled && indexOptions == 0 && !itemMenu.switchItemBox.isEnabled)
		{
            self.forwardButton.imageComponent.setImage(FORWARD_ARROW_INVERT_SRC);
			var room = self.dungeon.getRoomFromPoint(self.player.x, self.player.y);
		
			var directionVector = Direction.getVector(self.player.direction);
			var nextPlayerX = self.player.x + directionVector.x;
			var nextPlayerY = self.player.y + directionVector.y;
			
			if (self.dungeon.canProgress(self.player.x, self.player.y, self.player.direction))
			{
				self.player.x += directionVector.x;
				self.player.y += directionVector.y;
				self.dungeon.setActiveRooms(self.player.x, self.player.y, self.player.direction);
				self.miniMap.setArrowDirection(self.player.direction);
				self.miniMap.setArrowPosition(self.player.x, self.player.y);
				self.dungeon.show();
				
				room = self.dungeon.getRoomFromPoint(self.player.x, self.player.y);
			}
			else if (room.chest != null && player.direction == room.chest.direction)
			{
				room.chest.loot();
			}
			else if (room == navMenu.dungeon.endRoom && player.direction == navMenu.dungeon.endRoom.door.direction)
			{
				var boss = EnemyFactory.createRandomBoss(this.game, player.level);
				this.game.remove(boss.dungeonSprite);
				self.disable();
				battleMenu.open(boss, self.player);
			}
			
			if (room.enemy != null)
			{
				self.disable();
				battleMenu.open(room.enemy, self.player);
				self.game.remove(room.enemy.dungeonSprite);
				room.enemy = null;
			}
		}
	});
	//Left Arrow and A	
	window.addEventListener("keydown", function(e){		 
	if((e.which == 37 || e.which == 65) && self.leftButton.isEnabled && indexOptions == 0 && !itemMenu.switchItemBox.isEnabled)
		{
			self.leftButton.imageComponent.setImage(LEFT_ARROW_INVERT_SRC);
			self._turnLeft.call(self);
			self.dungeon.setActiveRooms(self.player.x, self.player.y, self.player.direction);
			self.miniMap.setArrowDirection(self.player.direction);
			self.miniMap.setArrowPosition(self.player.x, self.player.y);
			self.dungeon.show();
		}
	});
	window.addEventListener("keyup", function(e){		 
		if((e.which == 37 || e.which == 65) && self.leftButton.isEnabled && indexOptions == 0)
		{
			self.leftButton.imageComponent.setImage(LEFT_ARROW_SRC);
		}
	});
	//Right Arrow and D	
	window.addEventListener("keydown", function(e){
		if((e.which == 39 || e.which == 68) && self.rightButton.isEnabled && indexOptions == 0 && !itemMenu.switchItemBox.isEnabled)
		{
			self.rightButton.imageComponent.setImage(RIGHT_ARROW_INVERT_SRC);
			self._turnRight.call(self);
			self.dungeon.setActiveRooms(self.player.x, self.player.y, self.player.direction);
			self.miniMap.setArrowDirection(self.player.direction);
			self.miniMap.setArrowPosition(self.player.x, self.player.y);
			self.dungeon.show();
		}
	});
	window.addEventListener("keyup", function(e){			 
			if((e.which == 39 || e.which == 68) && self.rightButton.isEnabled && indexOptions == 0)
			{
				self.rightButton.imageComponent.setImage(RIGHT_ARROW_SRC);
			}
	});
	
	
	
	
	window.addEventListener("keyup", function(e)
	{
		if (itemMenu.switchItemBox.isEnabled)
        {
                return;
        }
        
        if(e.which == 27 && navMenu.menuButton.isEnabled)
		{ 
			setTimeout(function(){  
				if(indexOptions == 0)
				{
					indexOptions++;
					navMenu.menuButton.imageComponent.setImage(MENU_INVERT_BUTTON_SRC);
				}
				else if(indexOptions == 1 || indexOptions == 2)
				{
					indexOptions = 0;
					navMenu.menuButton.imageComponent.setImage(MENU_BUTTON_SRC);
					navMenu.optionsButton.imageComponent.setImage(OPTIONS_BUTTON_SMALL_SRC);
				}
			}, 15);
		}
		if(e.which == 13 && navMenu.menuButton.isEnabled)
		{
			setTimeout(function(){
				if(indexOptions == 1)
				{
					navMenu.menuButton.setActive(false);
					navMenu.forwardButton.setActive(false);
					navMenu.leftButton.setActive(false);
					navMenu.rightButton.setActive(false);
					navMenu.optionsButton.setActive(false);
					quitMenu.open();					
				}
				if(indexOptions == 2)
				{
					self.disable();
					newOptions.open(self);
				}
			}, 10);
		}
	});
	window.addEventListener("keydown", function(e)
	{
		if((e.which == 87 || e.which == 38 || e.which == 83 || e.which == 40) && navMenu.menuButton.isEnabled && indexOptions >= 1)
		{
			indexOptions++;
			if(indexOptions <= -1)
			{
				indexOptions = 2;
			}
			if(indexOptions >= 3)
			{
				indexOptions = 1;
			}
			if(indexOptions == 1)
			{
				navMenu.menuButton.imageComponent.setImage(MENU_INVERT_BUTTON_SRC);
				navMenu.optionsButton.imageComponent.setImage(OPTIONS_BUTTON_SMALL_SRC);
			}
			if(indexOptions == 2)
			{
				navMenu.menuButton.imageComponent.setImage(MENU_BUTTON_SRC);
				navMenu.optionsButton.imageComponent.setImage(OPTIONS_BUTTON_SMALL_INVERT_SRC);
			}
		}
	});
};

NavMenu.prototype.disable = function()
{
    for (var property in this)
    {
        if (this.hasOwnProperty(property))
        {

            if (this[property] instanceof Entity)
            {
                this[property].setActive(false);
            }
            else if (this[property] instanceof Array)
            {
                for (var i = 0; i < this[property].length; i++)
                {
                    if (this[property][i] instanceof Entity)
                    {
                        this[property][i].setActive(false);
                    }
                }
            }
        }
    }
    
    if (this.dungeon) 
    {
        this.dungeon.hide();
    }
    
    if (this.miniMap) 
    {
        this.miniMap.hide();
    }
};

NavMenu.prototype.open = function()
{
    background.setActive(false);
    
    if (this.dungeon == null) {
        this._createDungeon();
    }
    
    this.wordPanel.setActive(true);
    this.rightPanel.setActive(true); 
	this.keyPanel.setActive(true);
    this.forwardButton.setActive(true);
    this.leftButton.setActive(true);
    this.rightButton.setActive(true);
    this.timeLabel.setActive(true);
    this.menuButton.setActive(true);
    this.optionsButton.setActive(true);
    
    soundOpening.pause();
    if (soundOpening.currentTime) 
    {
        soundOpening.currentTime = 0;  
    }
    soundNavigation.loop = true;
	soundNavigation.play();
    
    if (this.startTime == 0)
    {
        this.startTime = this.game.elapsedTimeInSeconds;
    }
    
    this.dungeon.show();
    this.miniMap.show();
};

NavMenu.prototype._createDungeon = function()
{
    // Accessing global player reference in youMustMath.js
    this.player = player;
    
    this.dungeon = new Dungeon(this.game, this.player);
    this.dungeon.generate();
    
    this.player.direction = this.dungeon.startRoom.getRandomPassageDirection();
    this.player.x = this.dungeon.startRoom.x;
    this.player.y = this.dungeon.startRoom.y;
    
    this.miniMap = new MiniMap(game, this.dungeon);
    this.miniMap.setArrowDirection(this.player.direction);
    
    this.dungeon.setActiveRooms(this.player.x, this.player.y, this.player.direction);
}