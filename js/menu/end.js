var score, healthBonus, enemyBonus, completionBonus, timePenalty, rank, highestRank, highestRankTitle, index;
var prevHighRank = 0;

function EndGame(game) 
{
    this.game = game;
    
    this.mainMenuButton;
    this.restartButton;
    this.startOverButton;
    this.continueButton;
    
    this.levelInfo;
    this.healthBonus;
    this.healthLabel;
    this.enemyBonus;
    this.enemyLabel;
    this.timePenalty;
    this.timeLabel;
    this.completionBonus;
    this.completionLabel;
    this.scoreDivider;
    this.finalScore;
    this.finalScoreLabel;
    this.result;
    this.beginnerLabel;
    this.wizardLabel;
    this.continueWarning;
    
    this.hasContinueWarningDisplayed = false;
    
    this.rankingBar;
    this.rankingSlider;
    
    this.endCutscene;
    this.endScreen;
    
    this._init();
    this._setupKeys();
    this.close();
}

EndGame.prototype._init = function() 
{
    this._createButtons();
    this._createResultText();
    this._createRankingBar();
    this._createEndScreen();
};

EndGame.prototype._createButtons = function() 
{
    this._createStartOverButton();
    this._createRestartButton();
    this._createMainMenuButton();
    this._createContinueButton();
};

EndGame.prototype._createStartOverButton = function() 
{
    this.startOverButton = this.game.create.image(0, 0, 264, 80, START_OVER_BUTTON_SRC);
    this.startOverButton.transform.pivotX = 0.5;
    this.startOverButton.transform.pivotY = 0.5;
    this.startOverButton.transform.centerX = this.game.width / 2;
    this.startOverButton.transform.bottom = this.game.height - this.startOverButton.transform.height * 0.15;
    
    var self = this;
    
    this.startOverButton.events.over.push(function()
    {
        self.startOverButton.transform.scaleX = 1.1;
        self.startOverButton.transform.scaleY = 1.1;
    });
    
    this.startOverButton.events.out.push(function()
    {
        self.startOverButton.transform.scaleX = 1;
        self.startOverButton.transform.scaleY = 1;
    });
    
    this.startOverButton.events.down.push(function()
    {
        self.startOverButton.imageComponent.setImage(START_OVER_BUTTON_INVERT_SRC);
    });
    
    this.startOverButton.events.up.push(function()
    {
        self.startOverButton.imageComponent.setImage(START_OVER_BUTTON_SRC);
    });
    
    this.startOverButton.events.click.push(function()
    {
        self.close();
        
        var stats = SaveData.getPlayerStat(player.saveIndex);
        var index = player.saveIndex;
        restart();
        startMenu.close();
        
        player = new Player(self.game, stats.name, stats.difficulty, stats.operators, 1, stats.levelsCompleted, stats.score, stats.rank, stats.itemType, index);
        player.save();
        
        navMenu.open();
        background.setActive(false);
    });
};

EndGame.prototype._createRestartButton = function() 
{
    this.restartButton = this.game.create.image(0, 0, 264, 80, TRY_AGAIN_BUTTON_SRC);
    this.restartButton.transform.pivotX = 0.5;
    this.restartButton.transform.pivotY = 0.5;
    this.restartButton.transform.left = (this.game.width / 2) + this.restartButton.transform.width / (2 * 1.15);
    this.restartButton.transform.bottom = this.game.height - this.restartButton.transform.height * 0.15;
    
    var self = this;
    
    this.restartButton.events.over.push(function()
    {
        self.restartButton.transform.scaleX = 1.1;
        self.restartButton.transform.scaleY = 1.1;
    });
    
    this.restartButton.events.out.push(function()
    {
        self.restartButton.transform.scaleX = 1;
        self.restartButton.transform.scaleY = 1;
    });
    
    this.restartButton.events.down.push(function()
    {
        self.restartButton.imageComponent.setImage(TRY_AGAIN_BUTTON_INVERT_SRC);
    });
    
    this.restartButton.events.up.push(function()
    {
        self.restartButton.imageComponent.setImage(TRY_AGAIN_BUTTON_SRC);
    });
    
    this.restartButton.events.click.push(function()
    {        
        self.close();
        
        var stats = SaveData.getPlayerStat(player.saveIndex);
        var index = player.saveIndex;
        restart();
        startMenu.close();
        player = new Player(self.game, stats.name, stats.difficulty, stats.operators, stats.level, stats.levelsCompleted, stats.score, stats.rank, stats.itemType, index);
        navMenu.open();
        background.setActive(false);
    });
};

EndGame.prototype._createMainMenuButton = function() 
{
    this.mainMenuButton = this.game.create.image(0, 0, 264, 80, MAIN_MENU_BUTTON_SRC);
    this.mainMenuButton.transform.pivotX = 0.5;
    this.mainMenuButton.transform.pivotY = 0.5;
    this.mainMenuButton.transform.centerX = this.game.width / 2;
    this.mainMenuButton.transform.bottom = this.game.height - this.mainMenuButton.transform.height * 0.15;
    
    var self = this;
    
    this.mainMenuButton.events.over.push(function()
    {
        self.mainMenuButton.transform.scaleX = 1.1;
        self.mainMenuButton.transform.scaleY = 1.1;
    });
    
    this.mainMenuButton.events.out.push(function()
    {
        self.mainMenuButton.transform.scaleX = 1;
        self.mainMenuButton.transform.scaleY = 1;
    });
    
    this.mainMenuButton.events.down.push(function()
    {
        self.mainMenuButton.imageComponent.setImage(MAIN_MENU_BUTTON_INVERT_SRC);
    });
    
    this.mainMenuButton.events.up.push(function()
    {
        self.mainMenuButton.imageComponent.setImage(MAIN_MENU_BUTTON_SRC);
    });
    
    this.mainMenuButton.events.click.push(function()
    {
        if (!player.isDead())
        {
            player.levelUp();
            player.save();
        }
        
        restart();
    });
};

EndGame.prototype._createContinueButton = function() 
{
    this.continueButton = this.game.create.image(0, 0, 264, 80, CONTINUE_BUTTON_SRC);
    this.continueButton.transform.pivotX = 0.5;
    this.continueButton.transform.pivotY = 0.5;
    this.continueButton.transform.centerX = this.game.width / 2;
    this.continueButton.transform.bottom = this.game.height - this.continueButton.transform.height * 0.15;
    
    var self = this;
    
    this.continueButton.events.over.push(function()
    {
        self.continueButton.transform.scaleX = 1.1;
        self.continueButton.transform.scaleY = 1.1;
    });
    
    this.continueButton.events.out.push(function()
    {
        self.continueButton.transform.scaleX = 1;
        self.continueButton.transform.scaleY = 1;
    });
    
    this.continueButton.events.down.push(function()
    {
        self.continueButton.imageComponent.setImage(CONTINUE_BUTTON_INVERT_SRC);
    });
    
    this.continueButton.events.up.push(function()
    {
        self.continueButton.imageComponent.setImage(CONTINUE_BUTTON_SRC);
    });
    
    this.continueButton.events.click.push(function()
    {
        if (player.level < 3)
        {
            player.levelUp();
            player.save();
            
            self.close();
        
            var stats = SaveData.getPlayerStat(player.saveIndex);
            var index = player.saveIndex;
            restart();
            startMenu.close();
            player = new Player(self.game, stats.name, stats.difficulty, stats.operators, stats.level, stats.levelsCompleted, stats.score, stats.rank, stats.itemType, index);
            navMenu.open();
            background.setActive(false);
        }
        else
        {
            if (player.difficulty == "Hard")
            {
                player.levelUp();
                player.save();
                
                var stats = SaveData.getPlayerStat(player.saveIndex);
                var index = player.saveIndex;
                restart();
                startMenu.close();

                player = new Player(self.game, stats.name, stats.difficulty, stats.operators, 1, stats.levelsCompleted, stats.score, stats.rank, stats.itemType, index);
                player.save();

                navMenu.open();
                background.setActive(false);
            }
            else if (self.hasContinueWarningDisplayed)
            {                
                if (player.difficulty == "Easy")
                {
                    player.difficulty = "Normal";
                }
                else if (player.difficulty == "Normal")
                {
                    player.difficulty = "Hard";
                }
                player.save();
                
                self.close();
        
                var stats = SaveData.getPlayerStat(player.saveIndex);
                var index = player.saveIndex;
                restart();
                startMenu.close();

                player = new Player(self.game, stats.name, stats.difficulty, stats.operators, 1, 0, stats.score, "None", stats.itemType, index);
                player.save();

                navMenu.open();
                background.setActive(false);
            }
            else
            {
                self.continueWarning.setActive(true);
                self.continueWarning.textScrollComponent.create("Continuing will increase the difficulty!", 1, 0, function()
                {
                    self.hasContinueWarningDisplayed = true;
                });
            }
        }
    });
};

EndGame.prototype._createResultText = function() 
{
    this.levelInfo = this.game.create.hint(this.game.width - 1200, this.game.height - 700, "sans-serif", 42, "white");
    
    this.healthBonus = this.game.create.hint(this.game.width - 1150, this.game.height - 650, "sans-serif", 42, "white");
    this.healthLabel = this.game.create.hint(0, this.game.height - 650, "sans-serif", 42, "white");
    
    this.enemyBonus = this.game.create.hint(this.game.width - 1150, this.game.height - 600, "sans-serif", 42, "white");
    this.enemyLabel = this.game.create.hint(0, this.game.height - 600, "sans-serif", 42, "white");
    
    this.timePenalty = this.game.create.hint(this.game.width - 1150, this.game.height - 550, "sans-serif", 42, "white");
    this.timeLabel = this.game.create.hint(0, this.game.height - 550, "sans-serif", 42, "white");
    
    this.completionBonus = this.game.create.hint(this.game.width - 1150, this.game.height - 500, "sans-serif", 42, "white");
    this.completionLabel = this.game.create.hint(0, this.game.height - 500, "sans-serif", 42, "white");
    
    this.scoreDivider = this.game.create.image(this.game.width - 1150, this.game.height - 450, 280, 6, SCORE_DIVIDER_SRC);
    this.scoreDivider.transform.width = 500;
    
    this.finalScore = this.game.create.hint(this.game.width - 1150, this.game.height - 444, "sans-serif", 42, "white");
    this.finalScoreLabel = this.game.create.hint(0, this.game.height - 444, "sans-serif", 42, "white");
    
    this.result = this.game.create.hint(this.game.width / 2 - 100, this.game.height / 2, "sans-serif", 42, "white");
    this.continueWarning = this.game.create.hint((this.game.width / 2 - 350), (this.game.height - 150), "sans-serif", 42, "white");
};

EndGame.prototype._createRankingBar = function() 
{
    this.rankingBar = this.game.create.image(0, 0, 500, 50, RANKING_BAR_SRC);
    this.rankingBar.transform.left = this.scoreDivider.transform.left;
    this.rankingBar.transform.centerY = this.finalScore.transform.bottom + 200;
    
    this.rankingSlider = this.game.create.image(0, 0, 60, 50, RANKING_SLIDER_SRC);
    this.rankingSlider.transform.centerX = this.rankingBar.transform.left;
    this.rankingSlider.transform.top = this.rankingBar.transform.top;
    this.rankingSlider.transform.z = this.rankingBar.transform.z - 1;
    
    this.beginnerLabel = this.game.create.label(0, 0, "Beginner", "sans-serif", 24, "white");
    this.beginnerLabel.transform.centerX = this.rankingBar.transform.left;
    this.beginnerLabel.transform.top = this.rankingBar.transform.bottom;
    
    this.wizardLabel = this.game.create.label(0, 0, "Wizard", "sans-serif", 24, "white");
    this.wizardLabel.transform.centerX = this.rankingBar.transform.right;
    this.wizardLabel.transform.top = this.rankingBar.transform.bottom;
};

EndGame.prototype._createEndScreen = function() 
{    
    this.endScreen = this.game.create.image(0,0, 1280, 720, GAME_OVER_SCREEN_SRC);
    this.endScreen.transform.z = 100;
    
    // Accessing globcal endCutscene reference
    this.endCutscene = endCutscene;
    
    var self = this;
    
    this.endCutscene.addEventListener("play", function()
    {
        self.endScreen.setActive(true);
        self.endScreen.imageComponent.image = this;
    });
    
    this.endCutscene.addEventListener("ended", function()
    {
        self.endScreen.imageComponent.setImage(window["END_SCREEN_" + player.level + "_SRC"]);
        self._displayResults();
    });
};

EndGame.prototype.close = function()
{
    this.mainMenuButton.setActive(false);
    this.restartButton.setActive(false);
    this.startOverButton.setActive(false);
    this.continueButton.setActive(false);
    
    this.levelInfo.setActive(false);
    this.healthBonus.setActive(false);
    this.enemyBonus.setActive(false);
    this.timePenalty.setActive(false);
    this.completionBonus.setActive(false);
    this.scoreDivider.setActive(false);
    this.finalScore.setActive(false);
    this.result.setActive(false);
    this.beginnerLabel.setActive(false);
    this.wizardLabel.setActive(false);
    this.continueWarning.setActive(false);
    this.healthLabel.setActive(false);
    this.enemyLabel.setActive(false);
    this.timeLabel.setActive(false);
    this.completionLabel.setActive(false);
    this.finalScoreLabel.setActive(false);
    
    this.levelInfo.textComponent.text = "";
    this.healthBonus.textComponent.text = "";
    this.enemyBonus.textComponent.text = "";
    this.timePenalty.textComponent.text = "";
    this.completionBonus.textComponent.text = "";
    this.finalScore.textComponent.text = "";
    this.result.textComponent.text = "";
    this.continueWarning.textComponent.text = "";
    
    this.healthLabel.textComponent.text = "";
    this.enemyLabel.textComponent.text = "";
    this.timeLabel.textComponent.text = "";
    this.completionLabel.textComponent.text = "";
    this.finalScoreLabel.textComponent.text = "";
    
    this.rankingBar.setActive(false);
    this.rankingSlider.setActive(false);
    
    this.rankingSlider.transform.centerX = this.rankingBar.transform.left;
    this.rankingSlider.transform.bottom = this.rankingBar.transform.top;
    
    this.endScreen.setActive(false);
};

EndGame.prototype.open = function()
{
	if (player.isDead())
    {
        this._openGameOver();
    }
    else if (player.level < 3)
    {
        this._openRanking();
    }
    else if (player.level == 3)
    {
        this._openFinale();
    }
};

EndGame.prototype._openGameOver = function()
{    
    this.endScreen.imageComponent.setImage(GAME_OVER_SCREEN_SRC);
    this.endScreen.setActive(true);
    
    this.restartButton.setActive(true);
    
    this.mainMenuButton.setActive(true);
    this.mainMenuButton.transform.right = (this.game.width / 2) - this.mainMenuButton.transform.width / (2 * 1.15);
    this.mainMenuButton.transform.bottom = this.game.height - this.mainMenuButton.transform.height * 0.15;
    
    this.result.setActive(true);
    this.result.textScrollComponent.create("GAME OVER!", 1, 0, null);
    this.result.transform.x = this.game.width / 2 - 130;
    this.result.transform.y = this.game.height - 185;
};

EndGame.prototype._openRanking = function()
{    
    this.mainMenuButton.transform.right = (this.game.width / 2) - this.mainMenuButton.transform.width / (2 * 1.15);
    this.mainMenuButton.transform.bottom = this.game.height - this.mainMenuButton.transform.height * 0.15;
    
    this.continueButton.transform.left = (this.game.width / 2) + this.continueButton.transform.width / (2 * 1.15);
    this.continueButton.transform.bottom = this.game.height - this.continueButton.transform.height * 0.15;
    
    this.endScreen.imageComponent.setImage(window["END_SCREEN_" + player.level + "_SRC"]);
    this.endScreen.setActive(true);
    
    this._calculateScore();
    this._displayResults();
};

EndGame.prototype._openFinale = function()
{    
    this.hasContinueWarningDisplayed = false;
    
    this.mainMenuButton.transform.right = this.startOverButton.transform.left - this.mainMenuButton.transform.width * 0.15;
    this.mainMenuButton.transform.bottom = this.game.height - this.mainMenuButton.transform.height * 0.15;
    
    this.continueButton.transform.left = this.startOverButton.transform.right + this.continueButton.transform.width * 0.15;
    this.continueButton.transform.bottom = this.game.height - this.continueButton.transform.height * 0.15;
    
    this._calculateScore();
    this.endCutscene.play();
};

EndGame.prototype._displayResults = function()
{    
    this.levelInfo.setActive(true);
    this.levelInfo.textScrollComponent.create("Level " + player.level + " Results", 2, 0, this._displayHealthBonus.bind(this));
};

EndGame.prototype._displayHealthBonus = function()
{        
    this.healthBonus.setActive(true);
    this.healthBonus.textScrollComponent.create("Health Bonus:", 2, 0, this._displayHealthLabel.bind(this));
};

EndGame.prototype._displayHealthLabel = function()
{        
    this.healthLabel.setActive(true);
    this.healthLabel.textScrollComponent.create("" + healthBonus, 2, 0, this._displayEnemyBonus.bind(this));
    this.healthLabel.transform.right = this.scoreDivider.transform.right;
};

EndGame.prototype._displayEnemyBonus = function()
{        
    this.enemyBonus.setActive(true);
    this.enemyBonus.textScrollComponent.create("Enemy Bonus:", 2, 0, this._displayEnemyLabel.bind(this));
};

EndGame.prototype._displayEnemyLabel = function()
{        
    this.enemyLabel.setActive(true);
    this.enemyLabel.textScrollComponent.create("" + enemyBonus, 2, 0, this._displayTimePenalty.bind(this));
    this.enemyLabel.transform.right = this.scoreDivider.transform.right;
};

EndGame.prototype._displayTimePenalty = function()
{    
    this.timePenalty.setActive(true);
    this.timePenalty.textScrollComponent.create("Time Penalty:", 2, 0, this._displayTimeLabel.bind(this));
};

EndGame.prototype._displayTimeLabel = function()
{        
    this.timeLabel.setActive(true);
    this.timeLabel.textScrollComponent.create("-" + timePenalty, 2, 0, this._displayCompletionBonus.bind(this));
    this.timeLabel.transform.right = this.scoreDivider.transform.right;
};

EndGame.prototype._displayCompletionBonus = function()
{    
    this.completionBonus.setActive(true);
    this.completionBonus.textScrollComponent.create("Completion Bonus:", 2, 0, this._displayCompletionLabel.bind(this));
};

EndGame.prototype._displayCompletionLabel = function()
{        
    this.completionLabel.setActive(true);
    this.completionLabel.textScrollComponent.create("" + completionBonus, 2, 0, this._displayFinalScore.bind(this));
    this.completionLabel.transform.right = this.scoreDivider.transform.right;
};

EndGame.prototype._displayFinalScore = function()
{    
    this.scoreDivider.setActive(true);
    this.finalScore.setActive(true);
    this.finalScore.textScrollComponent.create("Final Score:", 2, 0, this._displayFinalLabel.bind(this));
};

EndGame.prototype._displayFinalLabel = function()
{    
    this.finalScoreLabel.setActive(true);
    this.finalScoreLabel.textScrollComponent.create("" + score, 2, 0, this._positionRankingSlider.bind(this));
    this.finalScoreLabel.transform.right = this.scoreDivider.transform.right;
};

EndGame.prototype._positionRankingSlider = function()
{    
    this.rankingBar.setActive(true);
    this.rankingSlider.setActive(true);
    this.beginnerLabel.setActive(true);
    this.wizardLabel.setActive(true);
    
    var maxScore = 0;
    var minScore = 0;
    
    if (player.level == 1)
    {
        minScore = 500;
        maxScore = 4500;
    }
    else if (player.level == 2)
    {
        minScore = 6500;
        maxScore = 10500;
    }
    else if (player.level == 3)
    {
        minScore = 10500;
        maxScore = 14500;
    }
    
    var sliderPercentage = (score - minScore) / (maxScore - minScore);
    
    if (sliderPercentage > 1)
    {
        sliderPercentage = 1;
    }
    else if (sliderPercentage < 0)
    {
        sliderPercentage = 0;
    }
    
    var sliderEndX = this.rankingSlider.transform.x + (this.rankingBar.transform.width * sliderPercentage);
    
    this.rankingSlider.tweenComponent.move(this.rankingSlider.transform.x,
                                           this.rankingSlider.transform.y,
                                           sliderEndX, 
                                           this.rankingSlider.transform.y, 
                                           120,
                                           this._displayRank.bind(this));

};

EndGame.prototype._displayRank = function()
{    
    this.result.setActive(true);
    this.result.textScrollComponent.create(rank, 1, 0, this._enableButtons.bind(this));
    this.result.transform.centerX = this.rankingBar.transform.centerX;
    this.result.transform.top = this.rankingBar.transform.top - (this.result.transform.height * 3);
};
                                                                              
EndGame.prototype._enableButtons = function()
{        
    if (player.level < 3) 
    {
        this.mainMenuButton.setActive(true);
        this.continueButton.setActive(true);
    }
    else
    {
        this.continueButton.setActive(true);
        this.startOverButton.setActive(true);
        this.mainMenuButton.setActive(true);
    }
};

EndGame.prototype._calculateScore = function()
{
	//player's rank
	rankings = ["Math Beginner", "Math Peasant", "Math Goat", "Math Apprentice", "Math Commoner", "Math Squire", "Math Noble", "Math Knight", "Math Hero", "Math Wizard"];
	
	score = 0;
	healthBonus = 0;
	enemyBonus = 0;
	timePenalty = 0;
	completionBonus = 0;
	
	healthBonus = health * 5;
	score += healthBonus;
	enemyBonus = enemiesDefeated * 1000;
	score += enemyBonus;
	timePenalty = time * 10;
	score -= timePenalty;
	
	enemiesDefeated = 0;
	
	if(score < 0)
	{
		score = 0;
	}
	
	if (player.level == 1)
    {
		if (player.difficulty == "Normal")
		{
			completionBonus+=250;
		}
		else if (player.difficulty == "Hard")
		{
			completionBonus+=500;
		}

		score += completionBonus;
		
      	if(score >= 0 && score<500)
		{
			index = 0;
			rank = rankings[0];
		}
		else if(score >= 500 && score<1000)
		{
			index = 1;
			rank = rankings[1];
		}
		else if(score >= 1000 && score<1500)
		{
			index = 2;
			rank = rankings[2];
		}
		else if(score >= 1500 && score<2000)
		{
			index = 3;
			rank = rankings[3];
		}
		else if(score >= 2000 && score<2500)
		{
			index = 4;
			rank = rankings[4];
		}
		else if(score >= 2500 && score<3000)
		{
			index = 5;
			rank = rankings[5];
		}
		else if(score >= 3000 && score<3500)
		{
			index = 6;
			rank = rankings[6];
		}
		else if(score >= 3500 && score<4000)
		{
			index = 7;
			rank = rankings[7];
		}
		else if(score >= 4000 && score<4500)
		{
			index = 8;
			rank = rankings[8];
		}
		else if(score >= 4500)
		{
			index = 9;
			rank = rankings[9];
		}
    }
    else if (player.level == 2)
    {
		if (player.difficulty == "Normal")
		{
			completionBonus+=250;
		}
		else if (player.difficulty == "Hard")
		{
			completionBonus+=500;
		}
		
		completionBonus = 4000;
		score += completionBonus;
		
		if(score >= 0 && score<6500)
		{
			index = 0;
			rank = rankings[0];
		}
		else if(score >= 6500 && score<7000)
		{
			index = 1;
			rank = rankings[1];
		}
		else if(score >= 7000 && score<7500)
		{
			index = 2;
			rank = rankings[2];
		}
		else if(score >= 7500 && score<8000)
		{
			index = 3;
			rank = rankings[3];
		}
		else if(score >= 8000 && score<8500)
		{
			index = 4;
			rank = rankings[4];
		}
		else if(score >= 8500 && score<9000)
		{
			index = 5;
			rank = rankings[5];
		}
		else if(score >= 9000 && score<9500)
		{
			index = 6;
			rank = rankings[6];
		}
		else if(score >= 9500 && score<10000)
		{
			index = 7;
			rank = rankings[7];
		}
		else if(score >= 10000 && score<10500)
		{
			index = 8;
			rank = rankings[8];
		}
		else if(score >= 10500)
		{
			index = 9;
			rank = rankings[9];
		}
    }
    else if (player.level == 3)
    {
		completionBonus = 6000;
		
		if (player.difficulty == "Normal")
		{
			completionBonus+=250;
		}
		else if (player.difficulty == "Hard")
		{
			completionBonus+=500;
		}
		
		score += completionBonus;
		
		if(score >= 0 && score<10500)
		{
			index = 0;
			rank = rankings[0];
		}
		else if(score >= 10500 && score<11000)
		{
			index = 1;
			rank = rankings[1];
		}
		else if(score >= 11000 && score<11500)
		{
			index = 2;
			rank = rankings[2];
		}
		else if(score >= 11500 && score<12000)
		{
			index = 3;
			rank = rankings[3];
		}
		else if(score >= 12000 && score<12500)
		{
			index = 4;
			rank = rankings[4];
		}
		else if(score >= 12500 && score<13000)
		{
			index = 5;
			rank = rankings[5];
		}
		else if(score >= 13000 && score<13500)
		{
			index = 6;
			rank = rankings[6];
		}
		else if(score >= 13500 && score<14000)
		{
			index = 7;
			rank = rankings[7];
		}
		else if(score >= 14000 && score<14500)
		{
			index = 8;
			rank = rankings[8];
		}
		else if(score >= 14500)
		{
			index = 9;
			rank = rankings[9];
		}
    }
    
    var currentRankIndex = rankings.indexOf(player.rank);
    
    if (index > currentRankIndex)
    {
        player.rank = rank;
    }
    player.save();
};

EndGame.prototype._setupKeys = function()
{
	var self = this;
	var indexEndButtons = -1;
	
	window.addEventListener("keydown", function(e){  
	//Left arrow & Right Arrow and  A & D*/
		if((e.which == 37 || e.which == 65 || e.which == 39 || e.which == 68) && endGame.restartButton.isEnabled)
		{
			indexEndButtons++;
			if(indexEndButtons <= -1)
			{
				indexEndButtons = 1;
			}
			if(indexEndButtons >= 2)
			{
				indexEndButtons = 0;
			}
			if(indexEndButtons == 0)
			{				
				endGame.mainMenuButton.imageComponent.setImage(MAIN_MENU_BUTTON_INVERT_SRC);
				endGame.restartButton.imageComponent.setImage(TRY_AGAIN_BUTTON_SRC);
			}
			else if(indexEndButtons == 1)
			{
				endGame.mainMenuButton.imageComponent.setImage(MAIN_MENU_BUTTON_SRC);
				endGame.restartButton.imageComponent.setImage(TRY_AGAIN_BUTTON_INVERT_SRC);
			}
		}
		if((e.which == 37 || e.which == 65 || e.which == 39 || e.which == 68) && endGame.continueButton.isEnabled && !endGame.startOverButton.isEnabled)
		{
			indexEndButtons++;
			if(indexEndButtons <= -1)
			{
				indexEndButtons = 2;
			}
			if(indexEndButtons == 1)
			{
				indexEndButtons = 2;
			}
			if(indexEndButtons >= 3)
			{
				indexEndButtons = 0;
			}
			if(indexEndButtons == 0)
			{				
				endGame.mainMenuButton.imageComponent.setImage(MAIN_MENU_BUTTON_INVERT_SRC);
				endGame.continueButton.imageComponent.setImage(CONTINUE_BUTTON_SRC);
			}
			else if(indexEndButtons == 2)
			{
				endGame.mainMenuButton.imageComponent.setImage(MAIN_MENU_BUTTON_SRC);
				endGame.continueButton.imageComponent.setImage(CONTINUE_BUTTON_INVERT_SRC);
			}
		}
		//left main menu -> startover ->continue
		if((e.which == 37 || e.which == 65) && endGame.continueButton.isEnabled && endGame.startOverButton.isEnabled)
		{
			if(indexEndButtons == -1)
			{
				indexEndButtons = 2;
			}
			else if(indexEndButtons == 3)//startover
			{
				indexEndButtons = 0;//continue
			}
			else if(indexEndButtons == 2)
			{
				indexEndButtons = 3;//mainMenu
			}
			else if(indexEndButtons == 0)
			{
				indexEndButtons = 2;//mainMenu
			}
			if(indexEndButtons == 0)//mainMenu
			{				
				endGame.mainMenuButton.imageComponent.setImage(MAIN_MENU_BUTTON_INVERT_SRC);
				endGame.continueButton.imageComponent.setImage(CONTINUE_BUTTON_SRC);
				endGame.startOverButton.imageComponent.setImage(START_OVER_BUTTON_SRC);
			}
			else if(indexEndButtons == 2)//continue
			{
				endGame.mainMenuButton.imageComponent.setImage(MAIN_MENU_BUTTON_SRC);
				endGame.continueButton.imageComponent.setImage(CONTINUE_BUTTON_INVERT_SRC);
				endGame.startOverButton.imageComponent.setImage(START_OVER_BUTTON_SRC);
			}
			else if(indexEndButtons == 3)//startover
			{
				endGame.mainMenuButton.imageComponent.setImage(MAIN_MENU_BUTTON_SRC);
				endGame.continueButton.imageComponent.setImage(CONTINUE_BUTTON_SRC);
				endGame.startOverButton.imageComponent.setImage(START_OVER_BUTTON_INVERT_SRC);
			}				
		}
		//right
		if((e.which == 39 || e.which == 68) && endGame.continueButton.isEnabled && endGame.startOverButton.isEnabled)
		{
			if(indexEndButtons == -1)
			{
				indexEndButtons = 0;
			}
			else if(indexEndButtons == 3)//startover
			{
				indexEndButtons = 2;//continue
			}
			else if(indexEndButtons == 2)
			{
				indexEndButtons = 0;//mainMenu
			}
			else if(indexEndButtons == 0)
			{
				indexEndButtons = 3;//mainMenu
			}
			if(indexEndButtons == 0)//mainMenu
			{				
				endGame.mainMenuButton.imageComponent.setImage(MAIN_MENU_BUTTON_INVERT_SRC);
				endGame.continueButton.imageComponent.setImage(CONTINUE_BUTTON_SRC);
				endGame.startOverButton.imageComponent.setImage(START_OVER_BUTTON_SRC);
			}
			else if(indexEndButtons == 2)//continue
			{
				endGame.mainMenuButton.imageComponent.setImage(MAIN_MENU_BUTTON_SRC);
				endGame.continueButton.imageComponent.setImage(CONTINUE_BUTTON_INVERT_SRC);
				endGame.startOverButton.imageComponent.setImage(START_OVER_BUTTON_SRC);
			}
			else if(indexEndButtons == 3)//startover
			{
				endGame.mainMenuButton.imageComponent.setImage(MAIN_MENU_BUTTON_SRC);
				endGame.continueButton.imageComponent.setImage(CONTINUE_BUTTON_SRC);
				endGame.startOverButton.imageComponent.setImage(START_OVER_BUTTON_INVERT_SRC);
			}			
		}
	});
	window.addEventListener("keyup", function(e){  
		if(e.which == 13 && endGame.mainMenuButton.isEnabled)
		{
			if(indexEndButtons == 0)
			{
				indexEndButtons = -1;
				endGame.mainMenuButton.imageComponent.setImage(MAIN_MENU_BUTTON_SRC);
				endGame.continueButton.imageComponent.setImage(CONTINUE_BUTTON_SRC);
				endGame.startOverButton.imageComponent.setImage(START_OVER_BUTTON_SRC);
				if (!player.isDead())
				{
					player.levelUp();
					player.save();
				}
				
                restart();
			}
			else if(indexEndButtons == 1)
			{
				indexEndButtons = -1;
				endGame.mainMenuButton.imageComponent.setImage(MAIN_MENU_BUTTON_SRC);
				endGame.continueButton.imageComponent.setImage(CONTINUE_BUTTON_SRC);
				endGame.startOverButton.imageComponent.setImage(START_OVER_BUTTON_SRC);
				self.close();
        
				var stats = SaveData.getPlayerStat(player.saveIndex);
				var index = player.saveIndex;
				restart();
				startMenu.close();
				player = new Player(self.game, stats.name, stats.difficulty, stats.operators, stats.level, stats.score, stats.itemType, index);
				navMenu.open();
				background.setActive(false);
			}
			else if(indexEndButtons == 2)
			{
				if (player.level < 3)
				{
					indexEndButtons = -1;
					endGame.mainMenuButton.imageComponent.setImage(MAIN_MENU_BUTTON_SRC);
					endGame.continueButton.imageComponent.setImage(CONTINUE_BUTTON_SRC);
					endGame.startOverButton.imageComponent.setImage(START_OVER_BUTTON_SRC);
					player.levelUp();
					player.save();
					
					self.close();
				
					var stats = SaveData.getPlayerStat(player.saveIndex);
					var index = player.saveIndex;
					restart();
					startMenu.close();
					player = new Player(self.game, stats.name, stats.difficulty, stats.operators, stats.level, stats.levelsCompleted, stats.score, stats.rank, stats.itemType, index);
					navMenu.open();
					background.setActive(false);
				}
				else
				{
					if (player.difficulty == "Hard")
					{
						player.levelUp();
                        player.save();                        
                        
                        var stats = SaveData.getPlayerStat(player.saveIndex);
						var index = player.saveIndex;
						restart();
						startMenu.close();

						player = new Player(self.game, stats.name, stats.difficulty, stats.operators, 1, stats.levelsCompleted, stats.score, stats.rank, stats.itemType, index);
						player.save();

						navMenu.open();
						background.setActive(false);
					}
					else if (self.hasContinueWarningDisplayed)
					{
						if (player.difficulty == "Easy")
						{
							player.difficulty = "Normal";
						}
						else if (player.difficulty == "Normal")
						{
							player.difficulty = "Hard";
						}
						player.save();
						
						self.close();
				
						var stats = SaveData.getPlayerStat(player.saveIndex);
						var index = player.saveIndex;
						restart();
						startMenu.close();

						player = new Player(self.game, stats.name, stats.difficulty, stats.operators, 1, 0, stats.score, "None", stats.itemType, index);
						player.save();

						navMenu.open();
						background.setActive(false);
					}
					else
					{
						self.continueWarning.setActive(true);
						self.continueWarning.textScrollComponent.create("Continuing will increase the difficulty!", 1, 0, function()
						{
							self.hasContinueWarningDisplayed = true;
						});
					}
				}
			}
			else if(indexEndButtons == 3)
			{
				indexEndButtons = -1;
				endGame.mainMenuButton.imageComponent.setImage(MAIN_MENU_BUTTON_SRC);
				endGame.continueButton.imageComponent.setImage(CONTINUE_BUTTON_SRC);
				endGame.startOverButton.imageComponent.setImage(START_OVER_BUTTON_SRC);
				self.close();
                
                player.levelUp();
                player.save();
				
				var stats = SaveData.getPlayerStat(player.saveIndex);
				var index = player.saveIndex;
				restart();
				startMenu.close();
				
				player = new Player(self.game, stats.name, stats.difficulty, stats.operators, 1, stats.levelsCompleted, stats.score, stats.rank, stats.itemType, index);
				player.save();
				
				navMenu.open();
				background.setActive(false);
			}
		}
	});
    
    window.onbeforeunload = function(e)
    {
        if (self.endScreen.isEnabled && !player.isDead())
        {
            player.levelUp();
            player.save();
        }
    }
};