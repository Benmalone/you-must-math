function TutorialMenu(game) 
{
    this.game = game;
    
    this.centerImage;
    
    this.backButton;
    this.previousButton;
    this.nextButton;
    
    this.currentImageIndex = 1;
    
    this._createImages();
    this._createButtons();
    
    this.close();
}

TutorialMenu.prototype._createImages = function()
{
    this.centerImage = this.game.create.image(this.game.width / 2, (this.game.height / 2) - 40, 1254, 705, TUTORIAL1_SRC);
    this.centerImage.transform.pivotX = 0.5;
    this.centerImage.transform.pivotY = 0.5;
    this.centerImage.transform.scaleX = 0.8;
    this.centerImage.transform.scaleY = 0.8;
};

TutorialMenu.prototype._createButtons = function()
{
    this._createBackButton();
    this._createNextButton();
    this._createPreviousButton();
};

TutorialMenu.prototype._createBackButton = function()
{
    this.backButton = this.game.create.image(0, 0, 200, 80, BACK_BUTTON_INVERT_SRC);
    this.backButton.transform.pivotX = 0.5;
    this.backButton.transform.pivotY = 0.5;
    this.backButton.transform.centerX = this.game.width / 2;
    this.backButton.transform.bottom = this.game.height - 10;
    
    var self = this;
    
    this.backButton.events.over.push(function()
    {
        self.backButton.transform.scaleX = 1.1;
        self.backButton.transform.scaleY = 1.1;
    });
    
    this.backButton.events.out.push(function()
    {
        self.backButton.transform.scaleX = 1;
        self.backButton.transform.scaleY = 1;
    });
    
    this.backButton.events.down.push(function()
    {
        self.backButton.imageComponent.setImage(BACK_BUTTON_INVERT_SRC);
    });
    
    this.backButton.events.up.push(function()
    {
        self.backButton.imageComponent.setImage(BACK_BUTTON_SRC);
    });
    
    this.backButton.events.click.push(function()
    {
        self.close();
        startMenu.open();
    });
	window.addEventListener("keyup", function(e){    
		if((e.which == 27 || e.which == 13) && tutorialMenu.backButton.isEnabled)
		{			
			self.close();
			startMenu.open();
		}
    });
};

TutorialMenu.prototype._createNextButton = function()
{
    this.nextButton = this.game.create.image(0, 0, 64, 86, TUTORIAL_FORWARD_SRC);
    this.nextButton.transform.pivotX = 0.5;
    this.nextButton.transform.pivotY = 0.5;
    this.nextButton.transform.z = -10;
    this.nextButton.transform.left = this.centerImage.transform.right + this.nextButton.transform.width / 4;
    this.nextButton.transform.centerY = this.centerImage.transform.centerY;
    
    var self = this;
    
    this.nextButton.events.over.push(function()
    {
        self.nextButton.transform.scaleX = 1.1;
        self.nextButton.transform.scaleY = 1.1;
    });
    
    this.nextButton.events.out.push(function()
    {
        self.nextButton.transform.scaleX = 1;
        self.nextButton.transform.scaleY = 1;
    });
    
    this.nextButton.events.down.push(function()
    {
        self.nextButton.imageComponent.setImage(TUTORIAL_FORWARD_INVERT_SRC);
    });
    
    this.nextButton.events.up.push(function()
    {
        self.nextButton.imageComponent.setImage(TUTORIAL_FORWARD_SRC);
    });
    
    this.nextButton.events.click.push(function()
    {
        self.currentImageIndex++;
        
        if (self.currentImageIndex >= 25) 
        {
            self.currentImageIndex = 25;
            self.nextButton.setActive(false);
        }
        
        if (self.currentImageIndex > 1 && !self.previousButton.isEnabled)
        {
            self.previousButton.setActive(true);
        }
        
        self.centerImage.imageComponent.setImage(window["TUTORIAL" + self.currentImageIndex + "_SRC"]);
    });
	window.addEventListener("keydown", function(e){    
		if((e.which == 39 || e.which == 68) && self.nextButton.isEnabled)
		{
			self.currentImageIndex++;
			
			if (self.currentImageIndex >= 25) 
			{
				self.currentImageIndex = 25;
				self.nextButton.setActive(false);
			}
			
			if (self.currentImageIndex > 1 && !self.previousButton.isEnabled)
			{
				self.previousButton.setActive(true);
			}
			
			self.centerImage.imageComponent.setImage(window["TUTORIAL" + self.currentImageIndex + "_SRC"]);
		}
	});
};

TutorialMenu.prototype._createPreviousButton = function()
{
    this.previousButton = this.game.create.image(0, 0, 64, 86, TUTORIAL_BACK_SRC);
    this.previousButton.transform.pivotX = 0.5;
    this.previousButton.transform.pivotY = 0.5;
    this.previousButton.transform.z = -10;
    this.previousButton.transform.right = this.centerImage.transform.left - this.previousButton.transform.width / 4;
    this.previousButton.transform.centerY = this.centerImage.transform.centerY;
    
    var self = this;
    
    this.previousButton.events.over.push(function()
    {
        self.previousButton.transform.scaleX = 1.1;
        self.previousButton.transform.scaleY = 1.1;
    });
    
    this.previousButton.events.out.push(function()
    {
        self.previousButton.transform.scaleX = 1;
        self.previousButton.transform.scaleY = 1;
    });
    
    this.previousButton.events.down.push(function()
    {
        self.previousButton.imageComponent.setImage(TUTORIAL_BACK_INVERT_SRC);
    });
    
    this.previousButton.events.up.push(function()
    {
        self.previousButton.imageComponent.setImage(TUTORIAL_BACK_SRC);
    });
    
    this.previousButton.events.click.push(function()
    {
        self.currentImageIndex--;
        
        if (self.currentImageIndex <= 1) 
        {
            self.currentImageIndex = 1;
            self.previousButton.setActive(false);
        }
        
        if (self.currentImageIndex < 25 && !self.nextButton.isEnabled)
        {
            self.nextButton.setActive(true);
        }
        
        self.centerImage.imageComponent.setImage(window["TUTORIAL" + self.currentImageIndex + "_SRC"]);
    });
	
	window.addEventListener("keydown", function(e){ 
	
		if((e.which == 37 || e.which == 65) && self.previousButton.isEnabled)
		{
			self.currentImageIndex--;
        
			if (self.currentImageIndex <= 1) 
			{
				self.currentImageIndex = 1;
				self.previousButton.setActive(false);
			}
			
			if (self.currentImageIndex < 25 && !self.nextButton.isEnabled)
			{
				self.nextButton.setActive(true);
			}
			
			self.centerImage.imageComponent.setImage(window["TUTORIAL" + self.currentImageIndex + "_SRC"]);
		}
	});
};

TutorialMenu.prototype.open = function()
{
    this.backButton.setActive(true);
    this.nextButton.setActive(true);
    this.centerImage.setActive(true);
    
    this.currentImageIndex = 1;
    
    this.centerImage.imageComponent.setImage(window["TUTORIAL" + this.currentImageIndex + "_SRC"]);
};

TutorialMenu.prototype.close = function()
{
    this.backButton.setActive(false);
    this.nextButton.setActive(false);
    this.previousButton.setActive(false);
    this.centerImage.setActive(false);
};
