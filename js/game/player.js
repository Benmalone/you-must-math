//Player Class. Makes the player and sets initial values. Also controls inventory
var enemiesDefeated;

function Player(game, name, difficulty, operators, level, levelsCompleted, score, rank, itemType, saveIndex)
{
	this.game = game;
    
    this.name = name;
    this.difficulty = difficulty;
    this.operators = operators;
    this.level = Number(level);
    this.levelsCompleted = Number(levelsCompleted);
    this.score = score;
    this.rank = rank;
	this.item = ItemFactory.createType(itemType);
    this.saveIndex = saveIndex;
    
    this.x;
    this.y;
    
    this.direction;
    
	this.health = Player.MAX_HEALTH;
	this.damage = Player.NORMAL_DAMAGE;
    this._speed = Player.NORMAL_SPEED;
}

Player.MAX_HEALTH = 248;
Player.NORMAL_SPEED = 240;
Player.NORMAL_DAMAGE = 22;

Player.prototype.generateScore = function(dungeonTime, enemiesDefeated)
{	
	this.score += this.health * 5;
	this.score += enemiesDefeated * 5000;
	this.score -= dungeonTime * 10;
	
	if(this.score < 0)
	{
		this.score = 0;
	}
}

Player.prototype.useItem = function()
{
    if (this.item != null)
    {
        this.item.use(this);
        this.item = null;
    }
};

Player.prototype.levelUp = function()
{
    this.level++;
    this.levelsCompleted++;
    
    if (this.level > 3)
    {
        this.level = 3;
    }
};

Player.prototype.hit = function(damage)
{
    this.health -= damage;
    
    if (this.health == 204 || this.health == 153 || this.health == 102 || this.health == 51 || this.health == 7)
    {
        this.health -= 7;
    }
	if (this.health == 182)
    {
        this.health = 175;
    }
    if (this.health == 131)
    {
        this.health = 124;
    }
    if (this.health == 80)
    {
        this.health = 73;
    }
    if (this.health == 29)
    {
        this.health = 22;
    }
	
    if (this.health < 0)
    {
        this.health = 0;
    }
};

Player.prototype.heal = function(health)
{
    this.health += health;
    
    if (this.health > Player.MAX_HEALTH)
    {
        this.health = Player.MAX_HEALTH;
    }
};

Player.prototype.damageUp = function(damage)
{
    this.damage *= damage;
};

Player.prototype.speedUp = function(speed)
{
    this._speed *= speed;
};

Player.prototype.resetStats = function()
{
	this.damage = Player.NORMAL_DAMAGE;
    this._speed = Player.NORMAL_SPEED;
};

Player.prototype.isDead = function()
{
    return this.health <= 0;
};

Player.prototype.kill = function()
{    
    this.name = "";
    this.difficulty = "";
    this.level = 0;
    this.score = 0;
    this.saveIndex = -1;
    
    if (this.item != null)
    {
        this.item.game.remove(this.item.image);
        this.item = null;
    }
    
    this.x = 0;
    this.y = 0;
    
    this.direction = null;
    
	this.health = Player.MAX_HEALTH;
	this.damage = Player.NORMAL_DAMAGE;
    this._speed = Player.NORMAL_SPEED;
};

Player.prototype.save = function()
{    
    if (this.saveIndex != -1)
    {
        var type;

        if (this.item != null)
        {
            type = this.item.type;
        }
        else
        {
            type = "none";
        }

        var stats =
        {
            name: this.name,
            difficulty: this.difficulty,
            operators: this.operators,
            level: this.level,
            levelsCompleted: this.levelsCompleted,
            score: this.score,
            rank: this.rank,
            itemType: type
        };
        
        SaveData.setPlayerStat(this.saveIndex, stats);
        SaveData.saveStatsToLocalStorage();
    }
};

Object.defineProperty(Player.prototype, "speed", 
{
    get: function()
    {
        if (this.difficulty == "Easy")
        {
            return this._speed * 1.6;
        }

        if (this.difficulty == "Normal")
        {
            return this._speed * 1.3;
        }

        if (this.difficulty == "Hard")
        {
            return this._speed * 1.6;
        }
    },
    
    set: function(value)
    {
        this._speed = value;
    }
});