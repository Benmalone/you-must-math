SaveData = 
{
    // {name: [string], difficulty: [string], level: [number] score: [number], itemType: [string]}
    playerStats: [null, null, null],
    
    saveWarningWasDisplayed: false,
	
    loadStatsFromLocalStorage: function()
    {
        var playerStats = this.playerStats;
        
        try
        {
            playerStats = JSON.parse(localStorage.getItem("playerStats"));
            
            if (playerStats == null)
            {
                playerStats = this.playerStats;
            }
        } 
        catch (e)
        {
            if (!this.saveWarningWasDisplayed)
            {
                alert("This game uses cookies to save. You can keep playing if they are disabled but saves will not work properly.");
                this.saveWarningWasDisplayed = true;
            }
        }        
        
        this.playerStats = playerStats;
    },
    
    saveStatsToLocalStorage: function()
    {
        var playerStatsJSON = JSON.stringify(this.playerStats);
        
        try
        {
            localStorage.setItem("playerStats", playerStatsJSON);
        } 
        catch (e)
        {
            if (!this.saveWarningWasDisplayed)
            {
                alert("This game uses cookies to save. You can keep playing if they are disabled but saves will not work properly.");
                this.saveWarningWasDisplayed = true;
            }
        }
    },
    
    setPlayerStat: function(slot, stat)
    {
        this.playerStats[slot] = stat;
    },
    
    getPlayerStat: function(slot)
    {
        return this.playerStats[slot];
    },
    
    getPlayerStats: function()
    {
        return this.playerStats;
    },
    
    isSlotEmpty: function(slot)
    {
        return this.playerStats[slot] == null;
    }
};