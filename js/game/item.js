function Item(type, health, damage, speed, src)
{
    this.type = type;
    
    this.health = health;
    this.damage = damage;
    this.speed = speed;
    
    this.src = src;
}
                                        
Item.POTION_WIDTH = 100;
Item.POTION_HEIGHT = 132;

Item.prototype.use = function(target) 
{
    target.heal(this.health);
    target.speedUp(this.speed);
    target.damageUp(this.damage);
};
