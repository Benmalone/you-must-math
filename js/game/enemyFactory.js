EnemyFactory =
{
    sprites:
    [
        SPRITE_BRUISER_SRC,
        SPRITE_BAT_SRC,
        SPRITE_SENTRY_SRC,
        SPRITE_GHOST_SRC,
        SPRITE_BEE_SRC,
        SPRITE_SKELETON_SRC,
        SPRITE_SPIDER_SRC,
        SPRITE_MAGE_SRC,
        SPRITE_THIEF_SRC,
        SPRITE_DRAGON_SRC,
		SPRITE_GORGON_SRC,
		SPRITE_MINOTAUR_SRC,
		SPRITE_WIZARD_SRC
    ],
    
    attack:
    [
        "Punch",
        "Bite",
        "Slash",
        "Punch",
        "Slash",
        "Slash",
        "Bite",
        "Fireball",
        "Slash",
        "Fireball",
		"Bite",
		"Punch",
		"Fireball"
    ],
    
    spriteSizes:
    [
        {width: 128, height: 128},
        {width: 128, height: 128},
        {width: 128, height: 128},
        {width: 128, height: 128},
        {width: 128, height: 128},
        {width: 128, height: 128},
        {width: 128, height: 192},
        {width: 128, height: 128},
        {width: 128, height: 128},
        {width: 256, height: 256},
		{width: 128, height: 128},
		{width: 256, height: 256},
		{width: 256, height: 256}
    ],
    
    spritePositions:
    [
        {x: 352, y: 142},
        {x: 352, y: 142},
        {x: 352, y: 142},
        {x: 335, y: 130},
        {x: 352, y: 142},
        {x: 352, y: 142},
        {x: 352, y: 0},
        {x: 352, y: 130},
        {x: 342, y: 100},
        {x: 245, y: 0},
        {x: 335, y: 110},
        {x: 245, y: 0},
        {x: 235, y: 20}
    ],
    
    spriteScales:
    [
        {scaleX: 2, scaleY: 2},
        {scaleX: 2, scaleY: 2},
        {scaleX: 2, scaleY: 2},
        {scaleX: 2.4, scaleY: 2.4},
        {scaleX: 2, scaleY: 2},
        {scaleX: 2, scaleY: 2},
        {scaleX: 2, scaleY: 2},
        {scaleX: 2.3, scaleY: 2.3},
        {scaleX: 2.3, scaleY: 2.3},
        {scaleX: 2.1, scaleY: 2.1},
        {scaleX: 2.7, scaleY: 2.7},
        {scaleX: 2.1, scaleY: 2.1},
        {scaleX: 2, scaleY: 2}
    ],
    
    spriteAnimations:
    [
        {name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
        {name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
        {name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
        {name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
        {name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
        {name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
        {name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
        {name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
        {name: "idle", frames: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], frameRate: 100, loop: true},
        {name: "idle", frames: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13], frameRate: 100, loop: true},
		{name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
		{name: "idle", frames: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], frameRate: 100, loop: true},
		{name: "idle", frames: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], frameRate: 100, loop: true}
    ],
    
    dungeonSprites:
    [
        SPRITE_ENEMY_SRC,
        SPRITE_ENEMY_SRC,
        SPRITE_ENEMY_SRC,
        SPRITE_ENEMY_SRC,
        SPRITE_ENEMY_SRC,
        SPRITE_ENEMY_SRC,
        SPRITE_ENEMY_SRC,
        SPRITE_ENEMY_SRC,
        SPRITE_ENEMY_SRC,
        SPRITE_ENEMY_SRC,
		SPRITE_ENEMY_SRC,
		SPRITE_ENEMY_SRC,
		SPRITE_ENEMY_SRC
    ],
    
    dungeonSpriteSizes:
    [
        {width: 128, height: 128},
        {width: 128, height: 128},
        {width: 128, height: 128},
        {width: 128, height: 128},
        {width: 128, height: 128},
        {width: 128, height: 128},
        {width: 128, height: 128},
        {width: 128, height: 128},
        {width: 128, height: 128},
        {width: 128, height: 128},
		{width: 128, height: 128},
		{width: 128, height: 128},
		{width: 128, height: 128}
    ],
    
    dungeonSpriteAnimations:
    [
        {name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
        {name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
        {name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
        {name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
        {name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
        {name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
        {name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
        {name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
        {name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
        {name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
		{name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
		{name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true},
		{name: "idle", frames: [0, 1, 2, 3], frameRate: 300, loop: true}
    ],
    
	names: 
    [
        "Timely Tim",
        "Reduce Wayne",
        "Sentry of Subtraction",
        "Spooky Subrina",
        "Bisection Buzz",
        "Segmented Steve",
        "Addem the Arachnid",
        "Demonica the Mage",
        "Thieving Theo",
        "Denominator Drake",
		"Multiple Medusa",
		"Minustaur",
		"The Wizard of Ops"
    ],
    
	puns:
    [
        "I'll multiply your pain!",
        "I'll divide your parts!",
        "You are sub-par to me!",
        "I'll subtract your soul!",
        "You can't reduce me!",
        "Wait till you get a piece of me!",
        "I'll add you to to my web!",
        "I'll multiply your misery!",
        "You'll be added to my collection!",
        "My powers are limitless!",
		"I'll reduce you to stone!",
		"You won't add up to me!",
		"You shall not math!"
    ],
    
	add: [0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0],
    
	sub: [0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0],
    
	mult:[1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1],
    
	div: [0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1],
    
    isBoss:
    [
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        true,
		false,
		true,
		true
    ],
    
    health:
    [
        124,
        73,
        95,
        95,
        73,
        95,
        95,
        95,
        95,
        248,
		146,
		146,
		197
    ],
    
    damage:
    [
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        44,
		22,
		44,
		44
    ],
    
    levels:
    [
        [false, true, true],
        [false, true, false],
        [true, false, false],
        [false, false, true],
        [false, true, true],
        [true, false, true],
        [true, false, true],
        [false, true, false],
        [true, false, false],
        [false, false, true],
        [false, false, true],
        [true, false, false],
        [false, true, false]
    ],
    
    createRandomEnemy: function(game, level)
    {
        var i = this.getRandomEnemyIndex(level);
        return this.createEnemy(game, i);
    },
    
    createRandomBoss: function(game, level)
    {
        var i = this.getRandomBossIndex(level);
        return this.createEnemy(game, i);
    },
    
    createEnemy: function(game, i)
    {
        var sprite = game.create.sprite(0, 0, this.spriteSizes[i].width, this.spriteSizes[i].height, this.sprites[i]);
        sprite.transform.x = this.spritePositions[i].x;
        sprite.transform.y = this.spritePositions[i].y;
        sprite.transform.scaleX = this.spriteScales[i].scaleX;
        sprite.transform.scaleY = this.spriteScales[i].scaleY;
        
        sprite.animations.add(this.spriteAnimations[i].name,
                              this.spriteAnimations[i].frames,
                              this.spriteAnimations[i].frameRate,
                              this.spriteAnimations[i].loop);
        
        sprite.animations.play(this.spriteAnimations[i].name);
        
        
        var dungeonSprite = game.create.sprite(0, 0, this.dungeonSpriteSizes[i].width, this.dungeonSpriteSizes[i].height, this.dungeonSprites[i]);
        
        dungeonSprite.animations.add(this.dungeonSpriteAnimations[i].name,
                                     this.dungeonSpriteAnimations[i].frames,
                                     this.dungeonSpriteAnimations[i].frameRate,
                                     this.dungeonSpriteAnimations[i].loop);
        
        dungeonSprite.animations.play(this.dungeonSpriteAnimations[i].name);
        
        var enemy = new Enemy(game,
                             this.names[i],
                             this.puns[i],
                             this.add[i],
                             this.sub[i],
                             this.mult[i],
                             this.div[i],
                             this.health[i],
                             this.damage[i],
                             sprite,
                             dungeonSprite,
                             this.isBoss[i],
                             this.attack[i]);
        
        this.convertEnemyAttacksBasedOnPlayerSelection(enemy, player);
        
        return enemy;
    },
    
    convertEnemyAttacksBasedOnPlayerSelection: function(enemy, player)
    {
        if (player.operators == "Add/Sub")
        {
            if (enemy.mult == 1)
            {
                enemy.add = 1;
            }
            
            if (enemy.div == 1)
            {
                enemy.sub = 1;
            }
            
            enemy.mult = 0;
            enemy.div = 0;
        }
        else if (player.operators == "Multi/Div")
        {
            if (enemy.add == 1)
            {
                enemy.mult = 1;
            }
            
            if (enemy.sub == 1)
            {
                enemy.div = 1;
            }
            
            enemy.add = 0;
            enemy.sub = 0;
        }
    },
    
    getRandomEnemyIndex: function(level)
    {
        var enemyIndices = [];
        
        for (var i = 0; i < this.names.length; i++)
        {
            if (!this.isBoss[i] && this.levels[i][level - 1])
            {
                enemyIndices.push(i);
            }
        }
        
        return enemyIndices[Random.intRange(0, enemyIndices.length - 1)];
    },
    
    getRandomBossIndex: function(level)
    {
        var enemyIndices = [];
        
        for (var i = 0; i < this.names.length; i++)
        {
            if (this.isBoss[i] && this.levels[i][level - 1])
            {
                enemyIndices.push(i);
            }
        }
        
        return enemyIndices[Random.intRange(0, enemyIndices.length - 1)];
    }
};