//Enemy class. To create, use new Enemy(name, add(0/1), sub(0/1), mult(0/1), div(0/1), health, damage(1 or 2), sprite) 1 is true, 0 is false
//Ex: sentryOfSub = new Enemy("Sentry of Subtraction", false, true, false, false, 3, 1, SENTRY_OF_SUB);

function Enemy(game, name, pun, add, sub, mult, div, health, attackPower, sprite, dungeonSprite, isBoss, attack)
{
	this.game = game;
    
    // Grid Position
    this.x;
    this.y;
    
    this.assignedRoom = null;
    
    this.health = health;
	this.damage = attackPower;
    
    this.isBoss = isBoss;
    
    this.dungeonSprite = dungeonSprite;
    this.sprite = sprite;
    
    this.name = name;
	this.pun = pun;
	
	this.add = add;
	this.sub = sub;
	this.mult = mult;
	this.div = div;
    
    this.attack = attack;
}

Enemy.prototype.hit = function(damage)
{
    this.health -= damage;
    
    if (this.health == 204 || this.health == 153 || this.health == 102 || this.health == 58 || this.health == 51 || this.health == 7)
    {
        this.health -= 7;
    }
    if (this.health == 182)
    {
        this.health = 175;
    }
    if (this.health == 131)
    {
        this.health = 124;
    }
    if (this.health == 80)
    {
        this.health = 73;
    }
    if (this.health == 29)
    {
        this.health = 22;
    }
    
    if (this.health < 0)
    {
        this.health = 0;
    }
};

Enemy.prototype.move = function(room)
{
    if (room.enemy == null)
    {
        this.assignedRoom.enemy = null;
        
        this.x = room.x;
        this.y = room.y;
        this.assignedRoom = room;
        room.enemy = this;
    }
};

Enemy.prototype.removeFromDungeon = function()
{
    this.assignedRoom.enemy = null;
    this.assignedRoom = null;
    this.game.remove(this.dungeonSprite);
};

Enemy.prototype.isDead = function()
{
    return this.health <= 0;
};

Enemy.prototype.kill = function()
{
    this.game.remove(this.sprite);
};

Enemy.prototype.destroy = function()
{
    if (this.dungeonSprite != null)
    {
        this.game.remove(this.dungeonSprite);
    }
    
    if (this.sprite != null)
    {
        this.game.remove(this.sprite);
    }
    
    this.sprite = null;
    this.dungeonSprite = null;
    this.assignedRoom = null;
};