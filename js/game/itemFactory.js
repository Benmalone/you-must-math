ItemFactory =
{
    type:
    [
        "health",
        "speed",
        "damage"
    ],
    
    sprites:
    [
        RED_POTION_SRC,
        YELLOW_POTION_SRC,
        BLUE_POTION_SRC
    ],
    
	health: 
    [
        248,
        0,
        0
    ],
    
	speed:
    [
        1,
        2,
        1
    ],
    
    damage:
    [
        1,
        1,
        2
    ],
    
    createRandom: function()
    {
        var index = Random.intRange(0, this.sprites.length - 1);
        return new Item(this.type[index], this.health[index], this.damage[index], this.speed[index], this.sprites[index]);
    },
    
    createType: function(type)
    {
        var index = this.type.indexOf(type);
        if (index != -1)
        {
            return new Item(this.type[index], this.health[index], this.damage[index], this.speed[index], this.sprites[index]);
        }
        else
        {
            return null;
        }
    }
};