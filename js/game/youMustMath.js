// Backgrounds
HTML_BACKGROUND_SRC = "assets/images/HTMLbackgroundSmaller.png";
START_SCREEN_SRC = "assets/images/startscreen.png";
CREDITS_PAGE_SRC = "assets/images/creditsPage2.png";
TITLE_SRC = "assets/images/title.png";
LOADING_SCREEN_SRC = "assets/images/LoadingScreen.png";
SIGN_SRC = "assets/images/Sign.png";

// Start Menu Buttons
NEW_BUTTON_SRC = "assets/images/newButton.png";
NEW_BUTTON_INVERT_SRC = "assets/images/newButtonInvert.png";

LOAD_BUTTON_SRC = "assets/images/loadButton.png";
LOAD_BUTTON_INVERT_SRC = "assets/images/loadButtonInvert.png";

TUTORIAL_BUTTON_SRC = "assets/images/tutorialButton.png";
TUTORIAL_BUTTON_INVERT_SRC = "assets/images/tutorialButtonInvert.png";

OPTIONS_BUTTON_SRC = "assets/images/optionsButton.png";
OPTIONS_BUTTON_INVERT_SRC = "assets/images/optionsButtonInvert.png";

CREDITS_BUTTON_SRC = "assets/images/creditsButton.png";
CREDITS_BUTTON_INVERT_SRC = "assets/images/creditsButtonInvert.png";

BACK_BUTTON_SRC = "assets/images/backButton.png";
BACK_BUTTON_INVERT_SRC = "assets/images/backButtonInvert.png";

// Create Menu
CONTINUE_BUTTON_SRC = "assets/images/continueButton.png";
CONTINUE_BUTTON_INVERT_SRC = "assets/images/continueButtonInvert.png";
EASY_BUTTON_SRC = "assets/images/easyButton.png";
EASY_BUTTON_INVERT_SRC = "assets/images/easyButtonInvert.png";
EASY_BUTTON_SELECTED_SRC = "assets/images/easyButtonSelected.png";
NORMAL_BUTTON_SRC = "assets/images/normalButton.png";
NORMAL_BUTTON_INVERT_SRC = "assets/images/normalButtonInvert.png";
NORMAL_BUTTON_SELECTED_SRC = "assets/images/normalButtonSelected.png";
HARD_BUTTON_SRC = "assets/images/hardButton.png";
HARD_BUTTON_INVERT_SRC = "assets/images/hardButtonInvert.png";
HARD_BUTTON_SELECTED_SRC = "assets/images/hardButtonSelected.png";
ADD_SUB_BUTTON_SRC = "assets/images/add_sub_button.png";
ADD_SUB_BUTTON_INVERT_SRC = "assets/images/add_sub_invert.png";
ADD_SUB_BUTTON_SELECTED_SRC = "assets/images/add_sub_invert2.png";
MULT_DIV_BUTTON_SRC = "assets/images/mult_div_button.png";
MULT_DIV_BUTTON_INVERT_SRC = "assets/images/mult_div_invert.png";
MULT_DIV_BUTTON_SELECTED_SRC = "assets/images/mult_div_invert2.png";
ALL_OPERATORS_BUTTON_SRC = "assets/images/all_operators_button.png";
ALL_OPERATORS_BUTTON_INVERT_SRC = "assets/images/all_operators_invert.png";
ALL_OPERATORS_BUTTON_SELECTED_SRC = "assets/images/all_operators_invert2.png";
CHARACTER_CREATE_BOX_SRC = "assets/images/characterCreate.png";

// Selection Menu
SLOT_SRC = "assets/images/slot.png";
SLOT_INVERT_SRC = "assets/images/slotInvert.png";
SLOT_SELECTED_SRC = "assets/images/slotSelected.png";
EMPTY_SLOT_TEXT_SRC = "assets/images/emptySlotText.png";

// Panels
WORD_PANEL_SRC = "assets/images/wordpanel.png";
BOTTOM_RIGHT_PANEL_SRC = "assets/images/bottomrightpanel.png";

// Keypad Buttons
ZERO_BUTTON_SRC = "assets/images/0.png";
ZERO_INVERT_BUTTON_SRC = "assets/images/0invert.png";
ZERO_LIGHT_BUTTON_SRC = "assets/images/0light.png";
ONE_BUTTON_SRC = "assets/images/1.png";
ONE_INVERT_BUTTON_SRC = "assets/images/1invert.png";
ONE_LIGHT_BUTTON_SRC = "assets/images/1light.png";
TWO_BUTTON_SRC = "assets/images/2.png";
TWO_INVERT_BUTTON_SRC = "assets/images/2invert.png";
TWO_LIGHT_BUTTON_SRC = "assets/images/2light.png";
THREE_BUTTON_SRC = "assets/images/3.png";
THREE_INVERT_BUTTON_SRC = "assets/images/3invert.png";
THREE_LIGHT_BUTTON_SRC = "assets/images/3light.png";
FOUR_BUTTON_SRC = "assets/images/4.png";
FOUR_INVERT_BUTTON_SRC = "assets/images/4invert.png";
FOUR_LIGHT_BUTTON_SRC = "assets/images/4light.png";
FIVE_BUTTON_SRC = "assets/images/5.png";
FIVE_INVERT_BUTTON_SRC = "assets/images/5invert.png";
FIVE_LIGHT_BUTTON_SRC = "assets/images/5light.png";
SIX_BUTTON_SRC = "assets/images/6.png";
SIX_INVERT_BUTTON_SRC = "assets/images/6invert.png";
SIX_LIGHT_BUTTON_SRC = "assets/images/6light.png";
SEVEN_BUTTON_SRC = "assets/images/7.png";
SEVEN_INVERT_BUTTON_SRC = "assets/images/7invert.png";
SEVEN_LIGHT_BUTTON_SRC = "assets/images/7light.png";
EIGHT_BUTTON_SRC = "assets/images/8.png";
EIGHT_INVERT_BUTTON_SRC = "assets/images/8invert.png";
EIGHT_LIGHT_BUTTON_SRC = "assets/images/8light.png";
NINE_BUTTON_SRC = "assets/images/9.png";
NINE_INVERT_BUTTON_SRC = "assets/images/9invert.png";
NINE_LIGHT_BUTTON_SRC = "assets/images/9light.png";

CLEAR_BUTTON_SRC = "assets/images/CL.png";
CLEAR_INVERT_BUTTON_SRC = "assets/images/CLinvert.png";
CLEAR_LIGHT_BUTTON_SRC = "assets/images/CLlight.png";
ENTER_BUTTON_SRC = "assets/images/ENT.png";
ENTER_INVERT_BUTTON_SRC = "assets/images/ENTinvert.png";
ENTER_LIGHT_BUTTON_SRC = "assets/images/ENTlight.png";

// Battle Buttons
ATTACK_BUTTON_SRC = "assets/images/Attack.png";
ATTACK_BUTTON_INVERT_SRC = "assets/images/Attackinvert.png";
DEFEND_BUTTON_SRC = "assets/images/defend.png";
DEFEND_BUTTON_INVERT_SRC = "assets/images/defendinvert.png";

ADD_BUTTON_SRC = "assets/images/add.png";
ADD_BUTTON_INVERT_SRC = "assets/images/addinvert.png";
ADD_BUTTON_DARK_SRC = "assets/images/add_dark.png";
MINUS_BUTTON_SRC = "assets/images/minus.png";
MINUS_BUTTON_INVERT_SRC = "assets/images/minusinvert.png";
MINUS_BUTTON_DARK_SRC = "assets/images/sub_dark.png";
MULTIPLY_BUTTON_SRC = "assets/images/multiply.png";
MULTIPLY_BUTTON_INVERT_SRC = "assets/images/multiplyinvert.png";
MULTIPLY_BUTTON_DARK_SRC = "assets/images/mult_dark.png";
DIVIDE_BUTTON_SRC = "assets/images/divide.png";
DIVIDE_BUTTON_INVERT_SRC = "assets/images/divideinvert.png";
DIVIDE_BUTTON_DARK_SRC = "assets/images/div_dark.png";

// HUD
HEARTS_SRC = "assets/images/hearts.png";
HEARTS_DAMAGED_SRC = "assets/images/heartsDamaged.png";

MUSIC_BUTTON_SRC = "assets/images/music.png";
MUTE_BUTTON_SRC = "assets/images/mute.png";

MAP_SRC = "assets/images/map.png";
MINIMAP_ARROW_SRC = "assets/images/minimap_arrow.png";
MINIMAP_CHEST_SRC = "assets/images/minimap_chest.png";
MINIMAP_GRID_SRC = "assets/images/minimap_grid_sheet.png";

// Sprites
SPRITE_ENEMY_SRC = "assets/sprites/enemy.png"
SPRITE_BRUISER_SRC = "assets/sprites/bruiser.png"
SPRITE_BEE_SRC = "assets/sprites/bee.png"
SPRITE_SPIDER_SRC = "assets/sprites/spider.png"
SPRITE_THIEF_SRC = "assets/sprites/thief.png"
SPRITE_GHOST_SRC = "assets/sprites/ghost.png"
SPRITE_GORGON_SRC = "assets/sprites/gorgon.png"
SPRITE_BAT_SRC = "assets/sprites/bat.png"
SPRITE_SENTRY_SRC = "assets/sprites/ponyknight.png"
SPRITE_SKELETON_SRC = "assets/sprites/skeleton.png"
SPRITE_MAGE_SRC = "assets/sprites/mage.png"
SPRITE_DRAGON_SRC = "assets/sprites/dragon.png"
SPRITE_MINOTAUR_SRC = "assets/sprites/minotaur.png"
SPRITE_WIZARD_SRC = "assets/sprites/wizard.png"

// Effects
ADD_ATTACK_SRC = "assets/sprites/addAttack.png";
SUB_ATTACK_SRC = "assets/sprites/subAttack.png";
MULTIPLY_ATTACK_SRC = "assets/sprites/multAttack.png";
DIVIDE_ATTACK_SRC = "assets/sprites/divideAttack.png";
ENEMY_BITE_SRC = "assets/sprites/enemy_bite.png";
ENEMY_SLASH_SRC = "assets/sprites/enemy_slash.png";
ENEMY_PUNCH_SRC = "assets/sprites/enemy_punch.png";
ENEMY_FIREBALL_SRC = "assets/sprites/enemy_fireball.png";

// Dungeon
HALLWAY_STRAIGHT_BLUE_SRC = "assets/images/Straight1.png";
HALLWAY_LEFT_BLUE_SRC = "assets/images/LeftTurn1.png";
HALLWAY_RIGHT_BLUE_SRC = "assets/images/RightTurn1.png";
HALLWAY_INTERSECTION_BLUE_SRC = "assets/images/Intersection1.png";
HALLWAY_STRAIGHT_BROWN_SRC = "assets/images/Straight2.png";
HALLWAY_LEFT_BROWN_SRC = "assets/images/LeftTurn2.png";
HALLWAY_RIGHT_BROWN_SRC = "assets/images/RightTurn2.png";
HALLWAY_INTERSECTION_BROWN_SRC = "assets/images/Intersection2.png";
HALLWAY_STRAIGHT_GREEN_SRC = "assets/images/Straight3.png";
HALLWAY_LEFT_GREEN_SRC = "assets/images/LeftTurn3.png";
HALLWAY_RIGHT_GREEN_SRC = "assets/images/RightTurn3.png";
HALLWAY_INTERSECTION_GREEN_SRC = "assets/images/Intersection3.png";

//Dungeon Room
DOOR_LEFT_BLUE_SRC = "assets/images/DoorLeft1.png";
DOOR_RIGHT_BLUE_SRC = "assets/images/DoorRight1.png";
DOOR_FORWARD_BLUE_SRC = "assets/images/DoorStraight1.png";
DOOR_LEFT_BROWN_SRC = "assets/images/DoorLeft2.png";
DOOR_RIGHT_BROWN_SRC = "assets/images/DoorRight2.png";
DOOR_FORWARD_BROWN_SRC = "assets/images/DoorStraight2.png";
DOOR_LEFT_GREEN_SRC = "assets/images/DoorLeft3.png";
DOOR_RIGHT_GREEN_SRC = "assets/images/DoorRight3.png";
DOOR_FORWARD_GREEN_SRC = "assets/images/DoorStraight3.png";
CHEST_OPEN_SRC = "assets/images/ChestOpen.png";
CHEST_CLOSED_SRC = "assets/images/ChestClosed.png";

// Navigation Buttons
LEFT_ARROW_SRC = "assets/images/leftarrow.png";
LEFT_ARROW_INVERT_SRC = "assets/images/leftarrowinvert.png";
FORWARD_ARROW_SRC = "assets/images/forwardarrow.png";
FORWARD_ARROW_INVERT_SRC = "assets/images/forwardarrowinvert.png";
DOWN_ARROW_SRC = "assets/images/downarrow.png";
DOWN_ARROW_INVERT_SRC = "assets/images/downarrowinvert.png";
RIGHT_ARROW_SRC = "assets/images/rightarrow.png";
RIGHT_ARROW_INVERT_SRC = "assets/images/rightarrowinvert.png";

MENU_BUTTON_SRC = "assets/images/menubutton.png";
MENU_INVERT_BUTTON_SRC = "assets/images/menubuttoninvert.png";
OPTIONS_BUTTON_SMALL_SRC = "assets/images/optionsButtonSmall.png";
OPTIONS_BUTTON_SMALL_INVERT_SRC = "assets/images/optionsButtonSmallInvert.png";
NO_BUTTON_SRC = "assets/images/noButton.png";
NO_BUTTON_INVERT_SRC = "assets/images/noButtonInvert.png";
NO_BUTTON_DARK_SRC = "assets/images/no_dark.png";
YES_BUTTON_SRC = "assets/images/yesButton.png";
YES_BUTTON_INVERT_SRC = "assets/images/yesButtonInvert.png";
YES_BUTTON_DARK_SRC = "assets/images/yes_dark.png";
QUIT_PANEL_SRC = "assets/images/quit.png";

// Items
RED_POTION_SRC = "assets/images/redpotion.png";
BLUE_POTION_SRC = "assets/images/bluepotion.png";
YELLOW_POTION_SRC = "assets/images/yellowpotion.png";

// Item UI
SWITCH_OUT_BUTTON_SRC = "assets/images/switchoutbutton.png";
SWITCH_OUT_BUTTON_INVERT_SRC = "assets/images/switchoutbuttoninvert.png";
SWITCH_OUT_BUTTON_DARK_SRC = "assets/images/switch_dark.png";
KEEP_ITEM_BUTTON_SRC = "assets/images/keepitembutton.png";
KEEP_ITEM_BUTTON_INVERT_SRC = "assets/images/keepitembuttoninvert.png";
KEEP_ITEM_BUTTON_DARK_SRC = "assets/images/current_dark.png";
NEW_ITEM_PANEL_SRC = "assets/images/founditembox.png";
SWITCH_ITEM_PANEL_SRC = "assets/images/itemswitchbox.png";
ITEM_BUTTON_SRC = "assets/images/itembutton.png";

// Sounds
SOUND_DOWN_BUTTON_SRC = "assets/images/volumedown.png";
SOUND_UP_BUTTON_SRC = "assets/images/volumeup.png";
SOUND_DOWN_BUTTON_INVERT_SRC = "assets/images/volumedowninvert.png";
SOUND_UP_BUTTON_INVERT_SRC = "assets/images/volumeupinvert.png";
SOUND_EFFECTS_LABEL_SRC = "assets/images/Soundeffects.png";
SOUND_MUSIC_LABEL_SRC = "assets/images/musicLabel.png";
VOLUME_BOX_SRC = "assets/images/volumebox.png";

//Tutorial
TUTORIAL1_SRC = "assets/images/tutorial/t1.png";
TUTORIAL2_SRC = "assets/images/tutorial/t2.png";
TUTORIAL3_SRC = "assets/images/tutorial/t3.png";
TUTORIAL4_SRC = "assets/images/tutorial/t4.png";
TUTORIAL5_SRC = "assets/images/tutorial/t5.png";
TUTORIAL6_SRC = "assets/images/tutorial/t6.png";
TUTORIAL7_SRC = "assets/images/tutorial/t7.png";
TUTORIAL8_SRC = "assets/images/tutorial/t8.png";
TUTORIAL9_SRC = "assets/images/tutorial/t9.png";
TUTORIAL10_SRC = "assets/images/tutorial/t10.png";
TUTORIAL11_SRC = "assets/images/tutorial/t11.png";
TUTORIAL12_SRC = "assets/images/tutorial/t12.png";
TUTORIAL13_SRC = "assets/images/tutorial/t13.png";
TUTORIAL14_SRC = "assets/images/tutorial/t14.png";
TUTORIAL15_SRC = "assets/images/tutorial/t15.png";
TUTORIAL16_SRC = "assets/images/tutorial/t16.png";
TUTORIAL17_SRC = "assets/images/tutorial/t17.png";
TUTORIAL18_SRC = "assets/images/tutorial/t18.png";
TUTORIAL19_SRC = "assets/images/tutorial/t19.png";
TUTORIAL20_SRC = "assets/images/tutorial/t20.png";
TUTORIAL21_SRC = "assets/images/tutorial/t21.png";
TUTORIAL22_SRC = "assets/images/tutorial/t22.png";
TUTORIAL23_SRC = "assets/images/tutorial/t23.png";
TUTORIAL24_SRC = "assets/images/tutorial/t24.png";
TUTORIAL25_SRC = "assets/images/tutorial/t25.png";
TUTORIAL_FORWARD_SRC = "assets/images/tutorial/forwardbutton.png";
TUTORIAL_FORWARD_INVERT_SRC = "assets/images/tutorial/forwardbuttoninvert.png";
TUTORIAL_BACK_SRC = "assets/images/tutorial/backarrowbutton.png";
TUTORIAL_BACK_INVERT_SRC = "assets/images/tutorial/backarrowbuttoninvert.png";

//END
MAIN_MENU_BUTTON_SRC = "assets/images/mainMenuButton.png";
MAIN_MENU_BUTTON_INVERT_SRC = "assets/images/mainMenuInvert.png";
RESTART_BUTTON_SRC = "assets/images/restartbutton.png";
RESTART_BUTTON_INVERT_SRC = "assets/images/restartbuttoninvert.png";
START_OVER_BUTTON_SRC = "assets/images/startOverButton.png";
START_OVER_BUTTON_INVERT_SRC = "assets/images/startOverInvert.png";
TRY_AGAIN_BUTTON_SRC = "assets/images/try_again.png";
TRY_AGAIN_BUTTON_INVERT_SRC = "assets/images/tryagain_invert.png";
SCORE_DIVIDER_SRC = "assets/images/scoreDivider.png";
RANKING_BAR_SRC = "assets/images/rankbar.png";
RANKING_SLIDER_SRC = "assets/images/marker.png"
END_SCREEN_1_SRC = "assets/images/end1.png";
END_SCREEN_2_SRC = "assets/images/end2.png";
END_SCREEN_3_SRC = "assets/images/end3.png";
GAME_OVER_SCREEN_SRC = "assets/images/gameOver2.png";


var game = new Game("game", {preload: preload, load: load, setup: setup, update: update});
game.start();

var player,
    background,
    startMenu,
    newGameMenu,
    loadGameMenu,
    loadCredits,
	newOptions,
    createMenu,
	battleMenu,
    navMenu,
	endGame,
    endCutscene,
    itemMenu,
    quitMenu,
    tutorialMenu,
	time,
	musicButton,
	noMusicButton,
	soundNavigation,
	soundBattle,
	sndButton,
	sndPotion,
	sndDeath,
	sndDamage;

function preload()
{
    OnResizeCalled();
    game.setScale();
    
    game.assetManager.queueDownload(HEARTS_SRC);
    game.assetManager.queueDownload(HEARTS_DAMAGED_SRC);
    game.assetManager.queueDownload(LOADING_SCREEN_SRC);
    
    game.preloader.outlineSRC = HEARTS_DAMAGED_SRC;
    game.preloader.progressSRC = HEARTS_SRC;
    game.preloader.titleSRC = LOADING_SCREEN_SRC;
}

function load()
{
	game.assetManager.queueDownload(HTML_BACKGROUND_SRC);
	game.assetManager.queueDownload(SIGN_SRC);
    game.assetManager.queueDownload(WORD_PANEL_SRC);
    game.assetManager.queueDownload(HALLWAY_STRAIGHT_BLUE_SRC);
    game.assetManager.queueDownload(HALLWAY_LEFT_BLUE_SRC);
    game.assetManager.queueDownload(HALLWAY_RIGHT_BLUE_SRC);
    game.assetManager.queueDownload(HALLWAY_INTERSECTION_BLUE_SRC);
    game.assetManager.queueDownload(HALLWAY_STRAIGHT_BROWN_SRC);
    game.assetManager.queueDownload(HALLWAY_LEFT_BROWN_SRC);
    game.assetManager.queueDownload(HALLWAY_RIGHT_BROWN_SRC);
    game.assetManager.queueDownload(HALLWAY_INTERSECTION_BROWN_SRC);
    game.assetManager.queueDownload(HALLWAY_STRAIGHT_GREEN_SRC);
    game.assetManager.queueDownload(HALLWAY_LEFT_GREEN_SRC);
    game.assetManager.queueDownload(HALLWAY_RIGHT_GREEN_SRC);
    game.assetManager.queueDownload(HALLWAY_INTERSECTION_GREEN_SRC);
	
    game.assetManager.queueDownload(DOOR_LEFT_BLUE_SRC);
    game.assetManager.queueDownload(DOOR_RIGHT_BLUE_SRC);
    game.assetManager.queueDownload(DOOR_FORWARD_BLUE_SRC);
    game.assetManager.queueDownload(DOOR_LEFT_BROWN_SRC);
    game.assetManager.queueDownload(DOOR_RIGHT_BROWN_SRC);
    game.assetManager.queueDownload(DOOR_FORWARD_BROWN_SRC);
    game.assetManager.queueDownload(DOOR_LEFT_GREEN_SRC);
    game.assetManager.queueDownload(DOOR_RIGHT_GREEN_SRC);
    game.assetManager.queueDownload(DOOR_FORWARD_GREEN_SRC);
	
    game.assetManager.queueDownload(CHEST_OPEN_SRC);
    game.assetManager.queueDownload(CHEST_CLOSED_SRC);
    game.assetManager.queueDownload(BOTTOM_RIGHT_PANEL_SRC);
    game.assetManager.queueDownload(MENU_BUTTON_SRC);
    game.assetManager.queueDownload(MENU_INVERT_BUTTON_SRC);
    game.assetManager.queueDownload(NO_BUTTON_SRC);
    game.assetManager.queueDownload(NO_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(NO_BUTTON_DARK_SRC);
    game.assetManager.queueDownload(YES_BUTTON_SRC);
    game.assetManager.queueDownload(YES_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(YES_BUTTON_DARK_SRC);
    game.assetManager.queueDownload(QUIT_PANEL_SRC);
	
    game.assetManager.queueDownload(FORWARD_ARROW_SRC);
    game.assetManager.queueDownload(FORWARD_ARROW_INVERT_SRC);
    game.assetManager.queueDownload(DOWN_ARROW_SRC);
    game.assetManager.queueDownload(DOWN_ARROW_INVERT_SRC);
    game.assetManager.queueDownload(LEFT_ARROW_SRC);
    game.assetManager.queueDownload(LEFT_ARROW_INVERT_SRC);
    game.assetManager.queueDownload(RIGHT_ARROW_SRC);
    game.assetManager.queueDownload(RIGHT_ARROW_INVERT_SRC);
	game.assetManager.queueDownload(SPRITE_ENEMY_SRC);
	game.assetManager.queueDownload(SPRITE_BRUISER_SRC);
	game.assetManager.queueDownload(SPRITE_GORGON_SRC);
	game.assetManager.queueDownload(SPRITE_BAT_SRC);
	game.assetManager.queueDownload(SPRITE_SPIDER_SRC);
	game.assetManager.queueDownload(SPRITE_BEE_SRC);
	game.assetManager.queueDownload(SPRITE_SENTRY_SRC);
	game.assetManager.queueDownload(SPRITE_THIEF_SRC);
	game.assetManager.queueDownload(SPRITE_SKELETON_SRC);
	game.assetManager.queueDownload(SPRITE_GHOST_SRC);
	game.assetManager.queueDownload(SPRITE_MAGE_SRC);
	game.assetManager.queueDownload(SPRITE_DRAGON_SRC);
	game.assetManager.queueDownload(SPRITE_MINOTAUR_SRC);
	game.assetManager.queueDownload(SPRITE_WIZARD_SRC);
    game.assetManager.queueDownload(TITLE_SRC);
    
    game.assetManager.queueDownload(MAP_SRC);
    game.assetManager.queueDownload(CREDITS_PAGE_SRC);
    game.assetManager.queueDownload(CREDITS_BUTTON_SRC);
    game.assetManager.queueDownload(CREDITS_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(OPTIONS_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(OPTIONS_BUTTON_SRC);
    game.assetManager.queueDownload(TUTORIAL_BUTTON_SRC);
    game.assetManager.queueDownload(TUTORIAL_BUTTON_INVERT_SRC);
	
    game.assetManager.queueDownload(SWITCH_OUT_BUTTON_SRC);
    game.assetManager.queueDownload(SWITCH_OUT_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(SWITCH_OUT_BUTTON_DARK_SRC);
    game.assetManager.queueDownload(KEEP_ITEM_BUTTON_SRC);
    game.assetManager.queueDownload(KEEP_ITEM_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(KEEP_ITEM_BUTTON_DARK_SRC);
    game.assetManager.queueDownload(NEW_ITEM_PANEL_SRC);
    game.assetManager.queueDownload(SWITCH_ITEM_PANEL_SRC);
    game.assetManager.queueDownload(ITEM_BUTTON_SRC);
	
    game.assetManager.queueDownload(ATTACK_BUTTON_SRC);
    game.assetManager.queueDownload(ATTACK_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(DEFEND_BUTTON_SRC);
    game.assetManager.queueDownload(DEFEND_BUTTON_INVERT_SRC);
	game.assetManager.queueDownload(RED_POTION_SRC);
	game.assetManager.queueDownload(BLUE_POTION_SRC);
	game.assetManager.queueDownload(YELLOW_POTION_SRC);
    game.assetManager.queueDownload(ADD_BUTTON_SRC);
    game.assetManager.queueDownload(ADD_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(ADD_BUTTON_DARK_SRC);
	game.assetManager.queueDownload(MINUS_BUTTON_SRC);
	game.assetManager.queueDownload(MINUS_BUTTON_INVERT_SRC);
	game.assetManager.queueDownload(MINUS_BUTTON_DARK_SRC);
    game.assetManager.queueDownload(MULTIPLY_BUTTON_SRC);
    game.assetManager.queueDownload(MULTIPLY_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(MULTIPLY_BUTTON_DARK_SRC);
    game.assetManager.queueDownload(DIVIDE_BUTTON_SRC);
    game.assetManager.queueDownload(DIVIDE_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(DIVIDE_BUTTON_DARK_SRC);
    game.assetManager.queueDownload(ZERO_BUTTON_SRC);
    game.assetManager.queueDownload(ZERO_INVERT_BUTTON_SRC);
    game.assetManager.queueDownload(ZERO_LIGHT_BUTTON_SRC);
    game.assetManager.queueDownload(ONE_BUTTON_SRC);
    game.assetManager.queueDownload(ONE_INVERT_BUTTON_SRC);
    game.assetManager.queueDownload(ONE_LIGHT_BUTTON_SRC);
    game.assetManager.queueDownload(TWO_BUTTON_SRC);
    game.assetManager.queueDownload(TWO_INVERT_BUTTON_SRC);
    game.assetManager.queueDownload(TWO_LIGHT_BUTTON_SRC);
    game.assetManager.queueDownload(THREE_BUTTON_SRC);
    game.assetManager.queueDownload(THREE_INVERT_BUTTON_SRC);
    game.assetManager.queueDownload(THREE_LIGHT_BUTTON_SRC);
    game.assetManager.queueDownload(FOUR_BUTTON_SRC);
    game.assetManager.queueDownload(FOUR_INVERT_BUTTON_SRC);
    game.assetManager.queueDownload(FOUR_LIGHT_BUTTON_SRC);
    game.assetManager.queueDownload(FIVE_BUTTON_SRC);
    game.assetManager.queueDownload(FIVE_INVERT_BUTTON_SRC);
    game.assetManager.queueDownload(FIVE_LIGHT_BUTTON_SRC);
    game.assetManager.queueDownload(SIX_BUTTON_SRC);
    game.assetManager.queueDownload(SIX_INVERT_BUTTON_SRC);
    game.assetManager.queueDownload(SIX_LIGHT_BUTTON_SRC);
    game.assetManager.queueDownload(SEVEN_BUTTON_SRC);
    game.assetManager.queueDownload(SEVEN_INVERT_BUTTON_SRC);
    game.assetManager.queueDownload(SEVEN_LIGHT_BUTTON_SRC);
    game.assetManager.queueDownload(EIGHT_BUTTON_SRC);
    game.assetManager.queueDownload(EIGHT_INVERT_BUTTON_SRC);
    game.assetManager.queueDownload(EIGHT_LIGHT_BUTTON_SRC);
    game.assetManager.queueDownload(NINE_BUTTON_SRC);
    game.assetManager.queueDownload(NINE_INVERT_BUTTON_SRC);
    game.assetManager.queueDownload(NINE_LIGHT_BUTTON_SRC);
    game.assetManager.queueDownload(CLEAR_BUTTON_SRC);
    game.assetManager.queueDownload(CLEAR_INVERT_BUTTON_SRC);
    game.assetManager.queueDownload(CLEAR_LIGHT_BUTTON_SRC);
    game.assetManager.queueDownload(ENTER_BUTTON_SRC);
    game.assetManager.queueDownload(ENTER_INVERT_BUTTON_SRC);
    game.assetManager.queueDownload(ENTER_LIGHT_BUTTON_SRC);
    game.assetManager.queueDownload(START_SCREEN_SRC);
    game.assetManager.queueDownload(ADD_ATTACK_SRC);
    game.assetManager.queueDownload(SUB_ATTACK_SRC);
    game.assetManager.queueDownload(MULTIPLY_ATTACK_SRC);
    game.assetManager.queueDownload(DIVIDE_ATTACK_SRC);
    game.assetManager.queueDownload(ENEMY_BITE_SRC);
    game.assetManager.queueDownload(ENEMY_SLASH_SRC);
    game.assetManager.queueDownload(ENEMY_PUNCH_SRC);
    game.assetManager.queueDownload(ENEMY_FIREBALL_SRC);
    game.assetManager.queueDownload(MINIMAP_ARROW_SRC);
    game.assetManager.queueDownload(MINIMAP_CHEST_SRC);
    game.assetManager.queueDownload(MINIMAP_GRID_SRC);
    game.assetManager.queueDownload(MUSIC_BUTTON_SRC);
    game.assetManager.queueDownload(MUTE_BUTTON_SRC);
    
    game.assetManager.queueDownload(NEW_BUTTON_SRC);
    game.assetManager.queueDownload(NEW_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(LOAD_BUTTON_SRC);
    game.assetManager.queueDownload(LOAD_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(TUTORIAL_BUTTON_SRC);
    game.assetManager.queueDownload(TUTORIAL_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(OPTIONS_BUTTON_SRC);
    game.assetManager.queueDownload(OPTIONS_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(CREDITS_BUTTON_SRC);
    game.assetManager.queueDownload(CREDITS_BUTTON_INVERT_SRC);
    
    game.assetManager.queueDownload(SLOT_SRC);
    game.assetManager.queueDownload(SLOT_INVERT_SRC);
    game.assetManager.queueDownload(SLOT_SELECTED_SRC);
    
    game.assetManager.queueDownload(BACK_BUTTON_SRC);
    game.assetManager.queueDownload(BACK_BUTTON_INVERT_SRC);
    
    game.assetManager.queueDownload(CHARACTER_CREATE_BOX_SRC);
    game.assetManager.queueDownload(EASY_BUTTON_SRC);
    game.assetManager.queueDownload(EASY_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(EASY_BUTTON_SELECTED_SRC);
    game.assetManager.queueDownload(NORMAL_BUTTON_SRC);
    game.assetManager.queueDownload(NORMAL_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(NORMAL_BUTTON_SELECTED_SRC);
    game.assetManager.queueDownload(HARD_BUTTON_SRC);
    game.assetManager.queueDownload(HARD_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(HARD_BUTTON_SELECTED_SRC);
    game.assetManager.queueDownload(ADD_SUB_BUTTON_SRC);
    game.assetManager.queueDownload(ADD_SUB_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(ADD_SUB_BUTTON_SELECTED_SRC);
    game.assetManager.queueDownload(MULT_DIV_BUTTON_SRC);
    game.assetManager.queueDownload(MULT_DIV_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(MULT_DIV_BUTTON_SELECTED_SRC);
    game.assetManager.queueDownload(ALL_OPERATORS_BUTTON_SRC);
    game.assetManager.queueDownload(ALL_OPERATORS_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(ALL_OPERATORS_BUTTON_SELECTED_SRC);
    game.assetManager.queueDownload(CONTINUE_BUTTON_SRC);
    game.assetManager.queueDownload(CONTINUE_BUTTON_INVERT_SRC);
	
    game.assetManager.queueDownload(SOUND_DOWN_BUTTON_SRC);
    game.assetManager.queueDownload(SOUND_UP_BUTTON_SRC);
    game.assetManager.queueDownload(SOUND_DOWN_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(SOUND_UP_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(SOUND_EFFECTS_LABEL_SRC);
    game.assetManager.queueDownload(SOUND_MUSIC_LABEL_SRC);
    game.assetManager.queueDownload(VOLUME_BOX_SRC);
    
    game.assetManager.queueDownload(TUTORIAL1_SRC);
    game.assetManager.queueDownload(TUTORIAL2_SRC);
    game.assetManager.queueDownload(TUTORIAL3_SRC);
    game.assetManager.queueDownload(TUTORIAL4_SRC);
    game.assetManager.queueDownload(TUTORIAL5_SRC);
    game.assetManager.queueDownload(TUTORIAL6_SRC);
    game.assetManager.queueDownload(TUTORIAL7_SRC);
    game.assetManager.queueDownload(TUTORIAL8_SRC);
    game.assetManager.queueDownload(TUTORIAL9_SRC);
    game.assetManager.queueDownload(TUTORIAL10_SRC);
    game.assetManager.queueDownload(TUTORIAL11_SRC);
    game.assetManager.queueDownload(TUTORIAL12_SRC);
    game.assetManager.queueDownload(TUTORIAL13_SRC);
    game.assetManager.queueDownload(TUTORIAL14_SRC);
    game.assetManager.queueDownload(TUTORIAL15_SRC);
    game.assetManager.queueDownload(TUTORIAL16_SRC);
    game.assetManager.queueDownload(TUTORIAL17_SRC);
    game.assetManager.queueDownload(TUTORIAL18_SRC);
    game.assetManager.queueDownload(TUTORIAL19_SRC);
    game.assetManager.queueDownload(TUTORIAL20_SRC);
    game.assetManager.queueDownload(TUTORIAL21_SRC);
    game.assetManager.queueDownload(TUTORIAL22_SRC);
    game.assetManager.queueDownload(TUTORIAL23_SRC);
    game.assetManager.queueDownload(TUTORIAL24_SRC);
    game.assetManager.queueDownload(TUTORIAL25_SRC);
    game.assetManager.queueDownload(TUTORIAL_FORWARD_SRC);
    game.assetManager.queueDownload(TUTORIAL_FORWARD_INVERT_SRC);
    game.assetManager.queueDownload(TUTORIAL_BACK_SRC);
    game.assetManager.queueDownload(TUTORIAL_BACK_INVERT_SRC);
    
    game.assetManager.queueDownload(MAIN_MENU_BUTTON_SRC);
    game.assetManager.queueDownload(MAIN_MENU_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(START_OVER_BUTTON_SRC);
    game.assetManager.queueDownload(START_OVER_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(TRY_AGAIN_BUTTON_SRC);
    game.assetManager.queueDownload(TRY_AGAIN_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(SCORE_DIVIDER_SRC);
    game.assetManager.queueDownload(RANKING_BAR_SRC);
    game.assetManager.queueDownload(RANKING_SLIDER_SRC);
    game.assetManager.queueDownload(GAME_OVER_SCREEN_SRC);
    game.assetManager.queueDownload(RESTART_BUTTON_SRC);
    game.assetManager.queueDownload(RESTART_BUTTON_INVERT_SRC);
    game.assetManager.queueDownload(END_SCREEN_1_SRC);
    game.assetManager.queueDownload(END_SCREEN_2_SRC);
    game.assetManager.queueDownload(END_SCREEN_3_SRC);
    
    game.assetManager.queueDownload(OPTIONS_BUTTON_SMALL_SRC);
    game.assetManager.queueDownload(OPTIONS_BUTTON_SMALL_INVERT_SRC);
}

function setup()
{
    OnResizeCalled();
    game.setScale();
	
	soundBattle = document.getElementById("Battle_src");
	soundNavigation = document.getElementById("Void_src");
	soundOpening = document.getElementById("Northside_src");
	sndPotion = document.getElementById("Slurp_src");
	sndDamage = document.getElementById("Hurt_src");
	sndDeath = document.getElementById("Death_src");
	sndEnemyDeath = document.getElementById("EnemyDead_src");
	sndButton = document.getElementById("Button_src");
    
    endCutscene = document.createElement("video");
    endCutscene.src = "assets/videos/final_cutscene.mp4";
	
    background = game.create.image(0, 0, 1280, 720, START_SCREEN_SRC);
    background.transform.z = 100;

    startMenu = new StartMenu(game);
    navMenu = new NavMenu(game, player);
    newGameMenu = new NewGameMenu(game, startMenu);
    loadGameMenu = new LoadGameMenu(game, startMenu, navMenu);
    createMenu = new CreateMenu(game, newGameMenu, navMenu);
    loadCredits = new LoadCredits(game, startMenu);
    newOptions = new NewOptions(game, startMenu);
	quitMenu = new QuitMenu(game);
    itemMenu = new ItemMenu(game);
    tutorialMenu = new TutorialMenu(game);
    
    newGameMenu.createMenu = createMenu;
    
    startMenu.newGameMenu = newGameMenu;
    startMenu.loadGameMenu = loadGameMenu;
    startMenu.loadCredits = loadCredits;
    startMenu.newOptions = newOptions;
	
	battleMenu = new BattleMenu(game, player);
    battleMenu.disable();
	
	endGame = new EndGame(game);
	
    startT = navMenu.startTime;
	
	setupKeyboard();

	musicButton = game.create.image(game.width - 1240, game.height - 100, 64, 64, MUSIC_BUTTON_SRC);
    musicButton.transform.z = 1;
	muteButton = game.create.image(game.width - 1240, game.height - 100, 64, 64, MUTE_BUTTON_SRC);
    muteButton.transform.z = 1;
	muteButton.setActive(false);

	musicButton.events.click.push(function()
    {		
		muteButton.setActive(true);
		soundNavigation.muted = true;
		soundBattle.muted = true;
		soundOpening.muted = true;
		sndPotion.muted = true;
		sndButton.muted = true;
		sndDeath.muted = true;
		sndEnemyDeath.muted = true;
		sndDamage.muted = true;
        endCutscene.muted = true;
		musicButton.setActive(false);
	});
	muteButton.events.click.push(function()
    {		
		musicButton.setActive(true);
		soundNavigation.muted = false;
		soundBattle.muted = false;
		soundOpening.muted = false;
		sndPotion.muted = false;
		sndButton.muted = false;
		sndDeath.muted = false;
		sndEnemyDeath.muted = false;
		sndDamage.muted = false;
        endCutscene.muted = false;
		muteButton.setActive(false);
	});
	
    SaveData.loadStatsFromLocalStorage();
}

function setupKeyboard()
{	
	var body = document.querySelector("body");
	
	window.addEventListener("keypress", function(e){
        
		if(e.which >= 48 && e.which <= 57 && battleMenu.answerField.isEnabled)
		{
			battleMenu.answerField.textComponent.text += String.fromCharCode(e.which);
		}        
		if(e.which == 13 && battleMenu.answerField.isEnabled)
		{
			battleMenu.buttonENT.events.click[0]();
		}
        
	});
    
    window.addEventListener("keydown", function(e)
    {
		if(e.which == 8)
        {
            if (createMenu.nameInputElement != document.activeElement) {
                e.preventDefault();
            }
            
            if(battleMenu.answerField.isEnabled)
            {
                var text = battleMenu.answerField.textComponent.text.slice(0,-1);
                battleMenu.answerField.textComponent.text = text;
            }
        }
	});
    
	window.addEventListener("keydown", function(e){   
		if(e.which == 77 && musicButton.isEnabled)
		{
			musicButton.events.click[0]();
		}        
		else if(e.which == 77 && muteButton.isEnabled)
		{
            muteButton.events.click[0]();
		}        
	});
    
    window.addEventListener("blur", function(e)
    {
        musicButton.events.click[0]();
    });
    
    window.addEventListener("focus", function(e)
    {
        muteButton.events.click[0]();
    });
}

function update() 
{
    if (navMenu)
    {
		time = game.elapsedTimeInSeconds - navMenu.startTime;
        
        var timeMinutes = Math.floor(time / 60);
        var timeSeconds = time % 60;
        
		if(timeSeconds < 10)
		{
			navMenu.timeLabel.textComponent.text = "Time:  " + timeMinutes + ":0" + timeSeconds;
		}
        else
        {
            navMenu.timeLabel.textComponent.text = "Time:  " + timeMinutes + ":" + timeSeconds;
        }
    }
    createMenu.positionNameElement();
}

function restart()
{
    soundOpening.loop = true;
    soundOpening.play();
    
    soundNavigation.pause();
    if (soundNavigation.currentTime)
    {
        soundNavigation.currentTime = 0;
    }
    
    soundBattle.pause();
    if (soundBattle.currentTime) 
    {
        soundBattle.currentTime = 0;  
    }  
	
	player = null;
    
    navMenu.startTime = 0;
    
    navMenu.dungeon.destroy();
    navMenu.dungeon = null;
    
    navMenu.miniMap.destroy();
    navMenu.miniMap = null;
    
    createMenu.close();
    itemMenu.close();
    quitMenu.close();
    
    battleMenu.disable();
    navMenu.disable();
	endGame.close();
    
    background.setActive(true);
    startMenu.open();
}
