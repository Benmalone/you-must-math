$(function() {
    var $navLinks = $("nav > a");
    var $pages = $("div.header");
    
    $pages.hide();
    $pages.eq(0).show();
    
    $navLinks.click(function(){
        var $this = $(this);
        var index = $this.index();
        
        $pages.hide();
        $pages.eq(index).fadeIn(500);
    });
});