function Door(game, doorColor)
{
	this.game = game;
    
    // Grid Position
    this.x;
    this.y;
    
    // Direction player has to be facing to see chest
    this.direction;
    this.assignedRoom = null;
    
    this.color = doorColor.toUpperCase();
    
    this.sprite = this.game.create.sprite(0, 0, 960, 540, window["DOOR_FORWARD_" + this.color + "_SRC"]);
}

Door.prototype.destroy = function()
{
    if (this.sprite != null)
    {
        this.game.remove(this.sprite);
    }
    this.sprite = null;
    this.assignedRoom = null;
};
