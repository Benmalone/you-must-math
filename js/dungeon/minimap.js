function MiniMap(game, dungeon)
{
    this.game = game;
    this.cells;
    
    this._cellScaleX = 1;
    this._cellScaleY = 1;
    
    this.dungeon = dungeon;
    
    this.generate();
}

MiniMap.CELL_SIZE = 52;
MiniMap.MAX_WIDTH = 220;
MiniMap.MAX_HEIGHT = 300;
MiniMap.TOP_OFFSET = 48;
MiniMap.LEFT_OFFSET = 1012;

MiniMap.prototype.generate = function()
{
    this._initializeMapGrid();
    this._setupMapCells();
    this._setupArrow();
};

MiniMap.prototype._initializeMapGrid = function()
{
    this.cells = [];
    for (var x = 0; x < this.dungeon.width; x++)
    {
        this.cells[x] = [];
        for (var y = 0; y < this.dungeon.height; y++)
        {
            this.cells[x][y] = null;
        }
    }
};

MiniMap.prototype._setupMapCells = function()
{
    this._setCellScale();
    
    for (var x = 0; x < this.dungeon.width; x++)
    {
        for (var y = 0; y < this.dungeon.height; y++)
        {
            var room = this.dungeon.getRoomFromPoint(x, y);
            
            if (room != null)
            {
                this._createMapCell(x, y, room);
            }
        }
    }
};

MiniMap.prototype._setCellScale = function()
{
    var totalUnscaledMapWidth = this.dungeon.width * MiniMap.CELL_SIZE;
    var totalUnscaledMapHeight = this.dungeon.height * MiniMap.CELL_SIZE;
    
    this._cellScaleX = MiniMap.MAX_WIDTH / totalUnscaledMapWidth;
    this._cellScaleY = MiniMap.MAX_HEIGHT / totalUnscaledMapHeight;
};

MiniMap.prototype._createMapCell = function(x, y, room)
{
    var cellSprite = this._createMapSprite(x, y, room);
    var cellChestSprite = this._createMapChestSprite(x, y, room);
    
    this.cells[x][y] = new MapCell(x, y, cellSprite, cellChestSprite);
};

MiniMap.prototype._createMapSprite = function(x, y, room)
{
    var cellSprite = this.game.create.sprite(0, 0, MiniMap.CELL_SIZE, MiniMap.CELL_SIZE, MINIMAP_GRID_SRC);
    
    cellSprite.transform.pivotX = 0.5;
    cellSprite.transform.pivotY = 0.5;
    cellSprite.transform.scaleX = this._cellScaleX;
    cellSprite.transform.scaleY = this._cellScaleY;
    cellSprite.transform.left = MiniMap.LEFT_OFFSET + (cellSprite.transform.width * x);
    cellSprite.transform.top = MiniMap.TOP_OFFSET + (cellSprite.transform.height * y);
    
    cellSprite.setActive(false);
    
    var cellGridFrame = this._determineMapCellSprite(room);
    cellSprite.setFrame(cellGridFrame);
    
    return cellSprite;
};

MiniMap.prototype._createMapChestSprite = function(x, y, room)
{
    var cellChestSprite = null;
    
    if (room.chest != null)
    {
        cellChestSprite = this.game.create.sprite(0, 0, MiniMap.CELL_SIZE, MiniMap.CELL_SIZE, MINIMAP_CHEST_SRC);
    
        cellChestSprite.transform.pivotX = 0.5;
        cellChestSprite.transform.pivotY = 0.5;
        cellChestSprite.transform.scaleX = this._cellScaleX;
        cellChestSprite.transform.scaleY = this._cellScaleY;
        cellChestSprite.transform.left = MiniMap.LEFT_OFFSET + (cellChestSprite.transform.width * x);
        cellChestSprite.transform.top = MiniMap.TOP_OFFSET + (cellChestSprite.transform.height * y);

        cellChestSprite.setActive(false);
    }
    
    return cellChestSprite;
};
    
MiniMap.prototype._determineMapCellSprite = function(room)
{
    var spriteFrame = 0;
    var wall = room.walls.state;
    
    if (!wall.up && !wall.right && !wall.down && !wall.left)
        spriteFrame = 0;
    else if (!wall.up && !wall.right && !wall.down && wall.left)
        spriteFrame = 1;
    else if (!wall.up && wall.right && !wall.down && !wall.left)
        spriteFrame = 2;
    else if (!wall.up && !wall.right && wall.down && !wall.left)
        spriteFrame = 3;
    else if (wall.up && !wall.right && !wall.down && !wall.left)
        spriteFrame = 4;
    else if (!wall.up && wall.right && !wall.down && wall.left)
        spriteFrame = 5;
    else if (wall.up && !wall.right && wall.down && !wall.left)
        spriteFrame = 6;
    else if (wall.up && !wall.right && !wall.down && wall.left)
        spriteFrame = 7;
    else if (wall.up && wall.right && !wall.down && !wall.left)
        spriteFrame = 8;
    else if (!wall.up && wall.right && wall.down && !wall.left)
        spriteFrame = 9;
    else if (!wall.up && !wall.right && wall.down && wall.left)
        spriteFrame = 10;
    else if (!wall.up && wall.right && wall.down && wall.left)
        spriteFrame = 11;
    else if (wall.up && !wall.right && wall.down && wall.left)
        spriteFrame = 12;
    else if (wall.up && wall.right && !wall.down && wall.left)
        spriteFrame = 13;
    else if (wall.up && wall.right && wall.down && !wall.left)
        spriteFrame = 14;
    else if (wall.up && wall.right && wall.down && wall.left)
        spriteFrame = 15;
    
    return spriteFrame;
};

MiniMap.prototype._setupArrow = function()
{
    this.arrow = this.game.create.image(0, 0, MiniMap.CELL_SIZE, MiniMap.CELL_SIZE, MINIMAP_ARROW_SRC);
    this.arrow.transform.pivotX = 0.5;
    this.arrow.transform.pivotY = 0.5;
    this.arrow.transform.scaleX = this._cellScaleX;
    this.arrow.transform.scaleY = this._cellScaleY;
    this.setArrowPosition(this.dungeon.startRoom.x, this.dungeon.startRoom.y);
};

MiniMap.prototype.setArrowPosition = function(x, y)
{
    var cell = this.cells[x][y];
    
    if (cell != null)
    {
        if (!cell.wasVisited)
        {
            cell.wasVisited = true;
            cell.sprite.setActive(true);
            
            if (cell.chestSprite != null)
            {
                cell.chestSprite.setActive(true);
            }
        }
        
        this.arrow.transform.x = cell.sprite.transform.x;
        this.arrow.transform.y = cell.sprite.transform.y;
    }
};

MiniMap.prototype.setArrowDirection = function(direction)
{
    this.arrow.transform.rotation = Direction.getAngle(direction);
};

MiniMap.prototype.setCellChestState = function(x, y, state)
{
    var cell = this.cells[x][y];
    
    if (cell.chestSprite != null)
    {
        cell.chestSprite.setActive(state);
        cell.isChestFull = state;
    }
};

MiniMap.prototype.hide = function()
{
    this.arrow.setActive(false);
    
    for (var x = 0; x < this.cells.length; x++)
    {
        for (var y = 0; y < this.cells[x].length; y++)
        {
            if (this.cells[x][y] != null && this.cells[x][y].sprite.isEnabled)
            {
                this.cells[x][y].sprite.setActive(false);
                
                if (this.cells[x][y].chestSprite != null && this.cells[x][y].chestSprite.isEnabled)
                {
                    this.cells[x][y].chestSprite.setActive(false);
                }
            }
        }
    }
};

MiniMap.prototype.show = function()
{
    this.arrow.setActive(true);
    
    for (var x = 0; x < this.cells.length; x++)
    {
        for (var y = 0; y < this.cells[x].length; y++)
        {
            if (this.cells[x][y] != null && this.cells[x][y].wasVisited)
            {
                this.cells[x][y].sprite.setActive(true);
                
                if (this.cells[x][y].chestSprite != null && this.cells[x][y].isChestFull)
                {
                    this.cells[x][y].chestSprite.setActive(true);
                }
            }
        }
    }
};

MiniMap.prototype.destroy = function()
{
    for (var x = 0; x < this.cells.length; x++)
    {
        for (var y = 0; y < this.cells[x].length; y++)
        {
            if (this.cells[x][y] != null)
            {
                this.cells[x][y].destroy();
            }
        }
    }
    
    if (this.arrow != null) 
    {
        this.game.remove(this.arrow);
    }
    this.arrow = null;
    this.dungeon = null;
    
    this.cells = [];
};