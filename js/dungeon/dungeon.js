Dungeon = function(game, player)
{
	this.game = game;
	
    this.player = player;
    
	this.width;
	this.height;
	
	this.rooms;
	this.activeRooms = [];
	this.startRoom;
	this.endRoom;
    
	this.highestCursorPosition = 0;
	this.roomBranchCount = 4;
    
    this.color;
    
    this.enemyCount;
    this.enemies;
    
    this.init();
};

Dungeon.prototype = {
    
    constructor: Dungeon,
    
    init: function()
    {
        if (this.player.level == 1)
        {
            this.color = "BLUE";
        }
        else if (this.player.level == 2)
        {
            this.color = "BROWN";
        }
        else if (this.player.level == 3)
        {
            this.color = "GREEN";
        }
        
        this.enemyCount = this.player.level * 3;
        
        this.width = (this.player.level * 2) + 1;
        this.height = (this.player.level * 2) + 1;
    },
	
	generate: function()
    {    
        this._initializeRoomGrid();
		
		var roomGenerationCursor = [];
		
		this._initialGenerationStep(roomGenerationCursor);
		
		while (roomGenerationCursor.length > 0)
        {
			this._nextGenerationStep(roomGenerationCursor);
		}
        
        this._placeEnemies();
        this._placeBossDoor();
	},
    
    _initializeRoomGrid: function()
    {
        this.rooms = [];
		for (var x = 0; x < this.width; x++)
        {
			this.rooms[x] = [];
            for (var y = 0; y < this.height; y++)
            {
                this.rooms[x][y] = null;
            }
		}
    },
	
	_initialGenerationStep: function(roomGenerationCursor)
    {
		var randomX = this._getRandomXWithinDungeon();
		var randomY = this._getRandomYWithinDungeon();
        
		this.startRoom = this._createRoom(randomX, randomY);
		
		roomGenerationCursor.push(this.startRoom);
	},	
	
    _createRoom: function(x, y)
    {
		var room = new Room(this.game, x, y, this.color);
		room.maxConnectedRooms = this.roomBranchCount;
		this.rooms[x][y] = room;
		return room;
	},
    
	_nextGenerationStep: function(roomGenerationCursor) {
		var cursorPosition = roomGenerationCursor.length - 1;
		var currentRoom = roomGenerationCursor[cursorPosition];
		
		if (this.highestCursorPosition <= cursorPosition)
        {
			this.highestCursorPosition = cursorPosition;
			this.endRoom = currentRoom;
		}
		
		if (!currentRoom.isFullyInitialized()) {
		
			var direction = currentRoom.getRandomUnintializedWallDirection();
			var directionVector = Direction.getVector(direction);
            
			var nextX = directionVector.x + currentRoom.x;
            var nextY = directionVector.y + currentRoom.y;
			
			if (this._pointIsWithinDungeon(nextX, nextY)) {
				
				var nextRoom = this.getRoomFromPoint(nextX, nextY);
				
				if (nextRoom == null) {
					nextRoom = this._createRoom(nextX, nextY);
                    
					this._placePassage(currentRoom, nextRoom, direction);
					roomGenerationCursor.push(nextRoom);
				} else {
					this._placeWall(currentRoom, nextRoom, direction);
				}
				
			} 
			else {
				this._placeWall(currentRoom, null, direction);
			}
		
		} else {
            if (currentRoom.isDeadEnd() && currentRoom != this.startRoom) 
            {
                this._placeChest(currentRoom);
            }
            
			roomGenerationCursor.splice(cursorPosition, 1);
		}
	},	
	
	_placePassage: function(currentRoom, nextRoom, direction)
    {
		currentRoom.setWall(direction, false);
		currentRoom.connectedRoomsCount++;
		
		nextRoom.setOppositeWall(direction, false);
		nextRoom.connectedRoomsCount++;
	},
	
	_placeWall: function(currentRoom, nextRoom, direction)
    {
		currentRoom.setWall(direction, true);
		
		if (nextRoom != null)
        {
			nextRoom.setOppositeWall(direction, true);
			nextRoom.connectedRoomsCount++;
			
			currentRoom.connectedRoomsCount++;
		}
	},
    
    _placeEnemies: function()
    {
        this.enemies = [];
        
        var currentEnemyCount = 0;
        while (currentEnemyCount < this.enemyCount)
        {
            var randomX = this._getRandomXWithinDungeon();
            var randomY = this._getRandomYWithinDungeon();
            var randomRoom = this.getRoomFromPoint(randomX, randomY);

            if (randomRoom.enemy == null && randomRoom != this.startRoom)
            {
                var enemy = EnemyFactory.createRandomEnemy(this.game, player.level);
                enemy.x = randomRoom.x;
                enemy.y = randomRoom.y;
                enemy.assignedRoom = randomRoom;
                randomRoom.enemy = enemy;

                enemy.dungeonSprite.transform.pivotX = 0.5;
                enemy.dungeonSprite.transform.pivotY = 0.5;
                enemy.dungeonSprite.setActive(false);
                enemy.sprite.setActive(false);
                    
                this.enemies.push(enemy);
                
                currentEnemyCount++;
            }
        }
    },
    
    _placeChest: function(room)
    {
        var chest = new Chest(this.game);
        chest.x = room.x;
        chest.y = room.y;
        chest.assignedRoom = room;
        room.chest = chest;
        
        chest.direction = Direction.getOppositeDirection(room.getRandomPassageDirection());
        chest.sprite.transform.pivotX = 0.5;
        chest.sprite.transform.pivotY = 0.5;
        chest.sprite.setActive(false);
    },
	
    _placeBossDoor: function()
    {
        if (this.endRoom.chest != null)
        {
            this.endRoom.chest.removeFromDungeon();
        }
        
        var door = new Door(this.game, this.color);
        door.x = this.endRoom.x;
        door.y = this.endRoom.y;
        door.assignedRoom = this.endRoom;
        this.endRoom.door = door;
        
        door.direction = Direction.getOppositeDirection(this.endRoom.getRandomPassageDirection());
        door.sprite.transform.pivotX = 0.5;
        door.sprite.transform.pivotY = 0.5;
        door.sprite.setActive(false);
    },
    
	_pointIsWithinDungeon: function(x, y)
    {
		return (x >= 0 && x < this.width) && (y >= 0 && y < this.height);
	},
	
	_getRandomXWithinDungeon: function()
    {
		return Random.intRange(0, this.width - 1);
	},
    
    _getRandomYWithinDungeon: function()
    {
		return Random.intRange(0, this.height - 1);
	},
	
	getRoomFromPoint: function(x, y)
    {
		if (this._pointIsWithinDungeon(x, y))
        {
            return this.rooms[x][y];
        }
        else
        {
            return null;
        }
	},
    
    setActiveRooms: function(x, y, direction)
    {
        this.hide();
        this._traverseActiveRoomsUntilWallIsReached(x, y, direction);
        this._scaleActiveRooms();
    },
    
    hide: function()
    {
        for (var i = 0; i < this.activeRooms.length; i++)
        {
            this.activeRooms[i].image.setActive(false);
            
            if (this.activeRooms[i].enemy != null)
            {
                this.activeRooms[i].enemy.dungeonSprite.setActive(false);
            }
            
            if (this.activeRooms[i].chest != null)
            {
                this.activeRooms[i].chest.sprite.setActive(false);
            }
            
            if (this.activeRooms[i].door != null)
            {
                this.activeRooms[i].door.sprite.setActive(false);
            }
        }
    },
    
    show: function()
    {
        for (var i = 0; i < this.activeRooms.length; i++)
        {
            this.activeRooms[i].image.setActive(true);
            
            if (this.activeRooms[i].enemy != null)
            {
                this.activeRooms[i].enemy.dungeonSprite.setActive(true);
            }
            
            if (this.activeRooms[i].chest != null && this.activeRooms[i].chest.direction == player.direction)
            {
                this.activeRooms[i].chest.sprite.setActive(true);
            } 
            
            if (this.activeRooms[i].door != null && this.activeRooms[i].door.direction == player.direction)
            {
                this.activeRooms[i].door.sprite.setActive(true);
            } 
        }
    },
    
    _traverseActiveRoomsUntilWallIsReached: function(x, y, direction)
    {
        var wallReached = false;
        var directionVector = Direction.getVector(direction);
        
        var room = this.getRoomFromPoint(x, y);
        
        this.activeRooms = [];
        this.activeRooms.push(room);
        
        while (!wallReached) 
        {
            room.setRenderedWallFromDirection(direction);
            
            x += directionVector.x;
            y += directionVector.y;
            
            room = this.getRoomFromPoint(x, y);
            
            if (room == null || room.getOppositeWallState(direction))
            {
               wallReached = true;
            }
            else
            {
                this.activeRooms.push(room);
            }
        }
    },
    
    _scaleActiveRooms: function()
    {        
        this._setPropertiesForFirstRoomAndItsContents();
        
        for (var i = 1; i < this.activeRooms.length; i++)
        {
            var room = this.activeRooms[i];
            var previousRoom = this.activeRooms[i - 1];
            
            this._scaleRoomAccordingToPreviousRoom(room, previousRoom);
            
            this._scaleEnemy(room.enemy, i);
            this._scaleChest(room.chest, i);
            this._scaleDoor(room.door, i);
        }
    },
    
    _setPropertiesForFirstRoomAndItsContents: function()
    {
        var room = this.activeRooms[0];
        
        room.image.imageRenderer.mask = 1200;
        
        room.image.transform.x = 480;
        room.image.transform.y = 270;
        room.image.transform.scaleX = 1.5;
        room.image.transform.scaleY = 1.5;
        room.image.transform.z = 50;
        
        this._scaleEnemy(room.enemy, 0);
        this._scaleChest(room.chest, 0);
        this._scaleDoor(room.door, 0);
    },
    
    _scaleEnemy: function(enemy, roomIndex)
    {
        if (enemy != null)
        {
            var room = enemy.assignedRoom;
            enemy.dungeonSprite.transform.scaleX = room.image.transform.scaleX * 1.5;
            enemy.dungeonSprite.transform.scaleY = room.image.transform.scaleY * 1.5;
            enemy.dungeonSprite.transform.x = room.image.transform.x;
            enemy.dungeonSprite.transform.y = room.image.transform.y;
            enemy.dungeonSprite.transform.z = this.activeRooms.length + roomIndex - 1;
        }
    },
    
    _scaleChest: function(chest, roomIndex)
    {
        if (chest != null)
        {
            var room = chest.assignedRoom;
            chest.sprite.transform.scaleX = room.image.transform.scaleX * 0.75;
            chest.sprite.transform.scaleY = room.image.transform.scaleY * 0.75;
            chest.sprite.transform.x = room.image.transform.x;
            chest.sprite.transform.y = room.image.transform.y;
            chest.sprite.transform.z = this.activeRooms.length + roomIndex;
        }
    },
    
    _scaleDoor: function(door, roomIndex)
    {
        if (door != null)
        {
            var room = door.assignedRoom;
            door.sprite.transform.scaleX = room.image.transform.scaleX;
            door.sprite.transform.scaleY = room.image.transform.scaleY;
            door.sprite.transform.x = room.image.transform.x;
            door.sprite.transform.y = room.image.transform.y;
            door.sprite.transform.z = this.activeRooms.length + roomIndex;
        }
    },
    
    _scaleRoomAccordingToPreviousRoom: function(room, previousRoom)
    {
        room.image.transform.x = previousRoom.image.transform.x;
        room.image.transform.y = previousRoom.image.transform.y;

        room.image.transform.scaleX = previousRoom.image.transform.scaleX * 0.5;
        room.image.transform.scaleY = previousRoom.image.transform.scaleY * 0.5;
        room.image.transform.z = previousRoom.image.transform.z - 1;
        
        this._stretchRoomImageToNearestPixel(room.image);
    },

    _stretchRoomImageToNearestPixel: function(image) 
    {    
        if (this.isFloat(image.transform.left))
        {
            image.transform.left = Math.floor(image.transform.left);
            image.transform.width++;
        }

        if (this.isFloat(image.transform.top))
        {
            image.transform.top = Math.floor(image.transform.top);
            image.transform.height++;
        }
    },

    isFloat: function(number) 
    {
        return number % 1 != 0;
    },
    
    canProgress: function(currentX, currentY, desiredDirection)
    {
        var currentRoom = this.getRoomFromPoint(currentX, currentY);
        var directionName = Direction.getName(desiredDirection);
        
        return currentRoom != null && !currentRoom.walls.state[directionName];
    },
    
    moveEnemies: function()
    {
		for (var i = 0; i < this.enemies.length; i++)
        {
            var randomDirectionVector = Direction.getVector(this.enemies[i].assignedRoom.getRandomPassageDirection());
            var nextX = this.enemies[i].x + randomDirectionVector.x;
            var nextY = this.enemies[i].y + randomDirectionVector.y;
            
            var nextRoom = this.getRoomFromPoint(nextX, nextY);
            
            if (nextRoom != null && nextRoom.enemy == null)
            {
                this._moveEnemyToNextRoom(this.enemies[i], nextRoom);
            }
            
            console.log("Enemy Location: " + this.enemies[i].x + ", " + this.enemies[i].y);
		}
        
        console.log("Player Location: " + player.x + ", " + player.y);
    },
    
    _moveEnemyToNextRoom: function(enemy, room)
    {
        enemy.assignedRoom.enemy = null;
        enemy.assignedRoom = room;
        enemy.x = room.x;
        enemy.y = room.y;
        
        if (this.activeRooms.indexOf(room) != -1) 
        {
            enemy.dungeonSprite.setActive(true);
        }
        else
        {
            enemy.dungeonSprite.setActive(false);
        }
    },
    
    destroy: function()
    {
        for (var x = 0; x < this.width; x++)
        {
            for (var y = 0; y < this.height; y++)
            {
                if (this.rooms[x][y] != null)
                {
                    this.rooms[x][y].destroy();
                    this.rooms[x][y] = null;
                }
            }
		}
        
        this.enemies = [];
        this.enemyCount = 0;
        
        this.activeRooms = [];
	
        this.rooms = null;
        this.startRoom = null;
        this.endRoom = null;
    }
	
};