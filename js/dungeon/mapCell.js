MapCell = function(x, y, sprite, chestSprite)
{
    this.sprite = sprite;
    this.chestSprite = chestSprite;
    
    this.x = x;
    this.y = y;
    
    this.isChestFull = (chestSprite != null);
    
    this.wasVisited = false;
};

MapCell.prototype.destroy = function()
{
    if (this.sprite != null)
    {
        game.remove(this.sprite);
    }
    
    if (this.chestSprite != null)
    {
        game.remove(this.chestSprite);
    }
    
    this.sprite = null;
    this.chestSprite = null;
};