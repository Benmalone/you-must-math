function Chest(game)
{
	this.game = game;
    
    // Grid Position
    this.x;
    this.y;
    
    // Direction player has to be facing to see chest
    this.direction;
    this.assignedRoom = null;
    
    this.item = ItemFactory.createRandom();
    
    this.looted = false;
    
    this.sprite = this.game.create.sprite(0, 0, 960, 540, CHEST_CLOSED_SRC);
    
    var self = this;
    
    this.sprite.events.click.push(function()
    {
        if (!itemMenu.switchItemBox.isEnabled && self.assignedRoom.x == player.x && self.assignedRoom.y == player.y && itemMenu.canBeOpened)
        {
            self.loot();
        }
    });
}

Chest.prototype.loot = function()
{
    if (!this.looted)
    {
        this.looted = true;
        this.sprite.imageComponent.setImage(CHEST_OPEN_SRC, 960, 540);
        
        itemMenu.open(this);
        this.item = null;
        
        navMenu.miniMap.setCellChestState(this.x, this.y, false);
    } 
};

Chest.prototype.store = function(item)
{
    if (this.looted)
    {
        this.item = item;
        
        this.looted = false;
        this.sprite.imageComponent.setImage(CHEST_CLOSED_SRC, 960, 540);
        
        navMenu.miniMap.setCellChestState(this.x, this.y, true);
    } 
};

Chest.prototype.removeFromDungeon = function()
{
    this.assignedRoom.chest = null;
    this.assignedRoom = null;
    this.game.remove(this.sprite);
};

Chest.prototype.destroy = function()
{
    if(this.sprite != null)
    {
        this.game.remove(this.sprite);
    }
    this.sprite = null;
    this.assignedRoom = null;
};