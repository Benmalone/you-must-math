Room = function(game, x, y, wallColor)
{
	this.game = game;
    
    this.image = this.game.create.image(480, 270, 960, 540, HALLWAY_INTERSECTION_BLUE_SRC);
    this.image.transform.pivotX = 0.5;
    this.image.transform.pivotY = 0.5;
    this.image.setActive(false);
	
    // Grid position
    this.x = x;
    this.y = y;
    
	this.walls =
    {
		// Src of image to be drawn based on facing direction
		src:
        {
			up: null,
			right: null,
			down: null,
			left: null
		},
		
		// Stores if wall was set for the first time
		// used in conjuction with Dungeon to ensure
		// walls that were already set aren't again. 
		initialized:
        {
			up: false,
			right: false,
			down: false,
			left: false
		},
		
		// Maintains the current state of all walls.
		state:
        {
			up: true,
			right: true,
			down: true,
			left: true
		}
	};
    
    this.color = wallColor.toUpperCase();
    
    this.chest = null;
    this.door = null;
    this.enemy = null;
    
	this.maxConnectedRooms = 4;
	this.connectedRoomsCount = 0;
};

Room.prototype.setWall = function(direction, state) {
	switch(direction)
    {
		case Direction.UP:
			this.walls.state.up = state;
			this.walls.initialized.up = true;
			break;
		case Direction.RIGHT:
			this.walls.state.right = state;
			this.walls.initialized.right = true;
			break;
		case Direction.DOWN:
			this.walls.state.down = state;
			this.walls.initialized.down = true;
			break;
		case Direction.LEFT:
			this.walls.state.left = state;
			this.walls.initialized.left = true;
			break;
	}
	
	this._determineWallImages();
};

// Method to help set an adjacent room's walls to match the
// current rooms wall status
Room.prototype.setOppositeWall = function(direction, state) {
    switch(direction)
    {
		case Direction.UP:
			this.walls.state.down = state;
			this.walls.initialized.down = true;
			break;
		case Direction.RIGHT:
			this.walls.state.left = state;
			this.walls.initialized.left = true;
			break;
		case Direction.DOWN:
			this.walls.state.up = state;
			this.walls.initialized.up = true;
			break;
		case Direction.LEFT:
			this.walls.state.right = state;
			this.walls.initialized.right = true;
			break;
	}
	
	this._determineWallImages();
};

Room.prototype.getOppositeWallState = function(direction) {
    switch(direction)
    {
		case Direction.UP:
			return this.walls.state.down;
			break;
		case Direction.RIGHT:
			return this.walls.state.left
			break;
		case Direction.DOWN:
			return this.walls.state.up
			break;
		case Direction.LEFT:
			return this.walls.state.right
			break;
	}
};

Room.prototype._determineWallImages = function()
{
	this._determineUpImage();
    this._determineDownImage();
    this._determineLeftImage();
    this._determineRightImage();
};

Room.prototype._determineUpImage = function()
{
	var upImage;
    
    if (this.walls.state.left && this.walls.state.right)
    {
        upImage = window["HALLWAY_STRAIGHT_" + this.color + "_SRC"];
    }
    else if (this.walls.state.left) 
    {
        upImage = window["HALLWAY_RIGHT_" + this.color + "_SRC"];
    }
    else if (this.walls.state.right) 
    {
        upImage = window["HALLWAY_LEFT_" + this.color + "_SRC"];
    }
    else
    {
        upImage = window["HALLWAY_INTERSECTION_" + this.color + "_SRC"];
    }
    
    this.walls.src.up = upImage;
};

Room.prototype._determineDownImage = function()
{
	var downImage;
    
    if (this.walls.state.left && this.walls.state.right)
    {
        downImage = window["HALLWAY_STRAIGHT_" + this.color + "_SRC"];
    }
    else if (this.walls.state.left) 
    {
        downImage = window["HALLWAY_LEFT_" + this.color + "_SRC"];
    }
    else if (this.walls.state.right) 
    {
        downImage = window["HALLWAY_RIGHT_" + this.color + "_SRC"];
    }
    else
    {
        downImage = window["HALLWAY_INTERSECTION_" + this.color + "_SRC"];
    }
    
    this.walls.src.down = downImage;
};

Room.prototype._determineLeftImage = function()
{
	var leftImage;
    
    if (this.walls.state.up && this.walls.state.down)
    {
        leftImage = window["HALLWAY_STRAIGHT_" + this.color + "_SRC"];
    }
    else if (this.walls.state.up) 
    {
        leftImage = window["HALLWAY_LEFT_" + this.color + "_SRC"];
    }
    else if (this.walls.state.down) 
    {
        leftImage = window["HALLWAY_RIGHT_" + this.color + "_SRC"];
    }
    else
    {
        leftImage = window["HALLWAY_INTERSECTION_" + this.color + "_SRC"];
    }
    
    this.walls.src.left = leftImage;
};

Room.prototype._determineRightImage = function()
{
	var rightImage;
    
    if (this.walls.state.up && this.walls.state.down)
    {
        rightImage = window["HALLWAY_STRAIGHT_" + this.color + "_SRC"];
    }
    else if (this.walls.state.up) 
    {
        rightImage = window["HALLWAY_RIGHT_" + this.color + "_SRC"];
    }
    else if (this.walls.state.down) 
    {
        rightImage = window["HALLWAY_LEFT_" + this.color + "_SRC"];
    }
    else
    {
        rightImage = window["HALLWAY_INTERSECTION_" + this.color + "_SRC"];
    }
    
    this.walls.src.right = rightImage;
};

Room.prototype.getRandomUnintializedWallDirection = function()
{
	var uninitializedWalls = [];
	
	if (!this.walls.initialized.up) {
		uninitializedWalls.push(Direction.UP);
	}
	
	if (!this.walls.initialized.right) {
		uninitializedWalls.push(Direction.RIGHT);
	}
	
	if (!this.walls.initialized.down) {
		uninitializedWalls.push(Direction.DOWN);
	}
	
	if (!this.walls.initialized.left) {
		uninitializedWalls.push(Direction.LEFT);
	}
	
	return uninitializedWalls[Random.intRange(0, uninitializedWalls.length - 1)];
};

Room.prototype.getRandomPassageDirection = function()
{
	var passages = [];
	
	if (!this.walls.state.up) {
		passages.push(Direction.UP);
	}
	
	if (!this.walls.state.right) {
		passages.push(Direction.RIGHT);
	}
	
	if (!this.walls.state.down) {
		passages.push(Direction.DOWN);
	}
	
	if (!this.walls.state.left) {
		passages.push(Direction.LEFT);
	}
	
    if (passages.length <= 0)
    {
        return -1;
    }
    else
    {
        return passages[Random.intRange(0, passages.length - 1)];
    }
};

Room.prototype.isFullyInitialized = function()
{
	return 	this.connectedRoomsCount >= this.maxConnectedRooms
			|| (this.walls.initialized.up
				&& this.walls.initialized.right
				&& this.walls.initialized.down
				&& this.walls.initialized.left);			
};

Room.prototype.getWallCount = function()
{
	var count = 0;
	
	if (this.walls.state.up) {
        count++;
	}
	
	if (this.walls.state.right) {
        count++;
	}
	
	if (this.walls.state.down) {
        count++;
	}
	
	if (this.walls.state.left) {
        count++;
	}
	
    return count;
};

Room.prototype.isDeadEnd = function()
{
    return this.getWallCount() == 3;
};

Room.prototype.setRenderedWallFromDirection = function(direction)
{
    var imgSrc;
    
    switch(direction)
    {
		case Direction.UP:
            imgSrc = this.walls.src.up;
			break;
		case Direction.RIGHT:
            imgSrc = this.walls.src.right;
			break;
		case Direction.DOWN:
            imgSrc = this.walls.src.down;
			break;
		case Direction.LEFT:
            imgSrc = this.walls.src.left;
			break;
	}
    
    this.image.imageComponent.setImage(imgSrc);
};

Room.prototype.destroy = function()
{
    if (this.enemy != null)
    {
        this.enemy.destroy();
    }
    
    if (this.chest != null)
    {
        this.chest.destroy();
    }
    
    if (this.door != null)
    {
        this.door.destroy();
    }
    
    if (this.image != null)
    {
        this.game.remove(this.image);
    }
    
    this.enemy = null;
    this.chest = null;
    this.door = null;
    this.image = null;
};